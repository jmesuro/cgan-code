from datasets import datalist
from numpy.random import RandomState
from lib.vis import color_grid_vis


data = datalist.get_dataset('soja2', '/home/jmesuro/fullhd')


X_sample = data.get_unlab_batch(0, 9)
#X_sample = data.image_crop(X_sample, 64, random_state=0)
color_grid_vis(X_sample.transpose(0, 2, 3, 1), (8, 8),
               '/home/jmesuro/fullhd/real_%s_sample.png' % ('soja2'))