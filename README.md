## Pruebas


### Share
![](assets/share.png)
DCGAN para la generaci�n de imagenes en Theano. 
Se incorpora el datasets Drone Soja para realizar Tesis en Detecci�n de Malezas.


### split
Permite ahorrar pesos en la red. haciendo transformaciones en los filtros.


La idea es que el Generador y el Discriminador tienen los mismos pesos. Y en el entrenamient
en cada iteración son modificados por ambos trainers.


Params (sin contar los de BN) no sé si están los bias (b) ahí. 
Por ejemplo en la primera capa de GEN es densa, tenemos 100 features por 16384 (= 128 * 8 * 4 * 4 = 1024 * 4 * 4) es la cantidad de neuronas de la capa densa.
En la segunda 1024 filtros de de 4 * 4 y así…
Entonces vamos a compartir las capas entre DIS y GEN los que sean compatibles en tamaño, 

* w2g -- w4d
* w3g -- w3d
* w4g -- w2d

GEN
* w1g 100 16354
* w2g 1024 512 5 5
* w3g 512 256 5 5
* w4g 256 128 5 5
* w5g 128 3 5 5

DIS
* w1d 256 128 5 5
* w2d 256 128 5 5
* w3d 512 256 5 5
* w4d 1024 512 5 5
* w5d 1024 1

RESULTADOS

![](assets/share_result.png)

## Split layers

![](assets/split.png)

La idea es partir los layers de tal manera de tener menos parámetros. Por ejemplo en la imagen de abajo spliteamos la primera capa convolucional del generador G. 

Pasamos de 512 filtros de 1024x5x5 → 256 filtros de 512x5x5. 

Para hacer eso primero pasamos de 128 mapas (tamaño del batch) de 1024x4 a 256 mapas de 512x4x4. Lo que se hace en realidad es dividir los mapas por la mitad, duplicando su cantidad, adaptamos los filtros, que deben ser de la mitad de tamaño, pero también disminuimos la cantidad de filtros a la mitad (ahí está la reducción de parámetros), la salida nos queda con el doble de mapas de la mitad de tamaño y se corrige con un reshape final.


![](assets/split_result.png)


