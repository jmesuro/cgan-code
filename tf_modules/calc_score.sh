#!/bin/bash

# TMP="/state/partition1/$USER/$$"
# mkdir -p $TMP
# rm -R -f "$TMP/*"
#
# export THEANO_FLAGS="floatX=float32,device=gpu0,config.base_compiledir=\"$TMP\",nvcc.fastmath=True"

# Cambia al directorio actual
#$ -cwd

#$ -j y

# Exporta las variables de entorno:
#$ -V

##$ -o logs
##$ -e logs

# Pide la gpu, uso exclusivo:
#$ -l gpu=true

# El nombre del job
#$ -N "run"

# Selecciono la cola qmla. Usar qmla@compute-0-5 para pedir ese nodo exclusivamente.
##$ -q qmla
#$ -q qmla@compute-0-8

source activate tensorflow
export PYTHONPATH=/state/partition1/anaconda2/envs/tensorflow/lib/python2.7/site-packages

python -u calc_score.py "$@"

source deactivate
