#!/bin/bash

mkdir new_libs
cd new_libs

wget http://vault.centos.org/centos/7.0.1406/updates/x86_64/Packages/libstdc++-devel-4.8.2-16.2.el7_0.x86_64.rpm
wget http://vault.centos.org/centos/7.0.1406/updates/x86_64/Packages/libstdc++-4.8.2-16.2.el7_0.x86_64.rpm
wget http://vault.centos.org/centos/7.0.1406/updates/x86_64/Packages/glibc-devel-2.17-55.el7_0.5.x86_64.rpm
wget http://vault.centos.org/centos/7.0.1406/updates/x86_64/Packages/glibc-2.17-55.el7_0.5.x86_64.rpm

rpm2cpio glibc-devel-2.17-55.el7_0.5.x86_64.rpm | cpio -id
rpm2cpio glibc-2.17-55.el7_0.5.x86_64.rpm | cpio -id
rpm2cpio libstdc++-4.8.2-16.2.el7_0.x86_64.rpm | cpio -id
rpm2cpio libstdc++-devel-4.8.2-16.2.el7_0.x86_64.rpm | cpio -id

cd ..
cp -R new_libs /share/apps
