import inception_score.model as score
from sklearn.externals import joblib

# Ejemplo de uso
def calc_cifar_score():
    import datasets.cifar10_data as cifar
    cif = cifar.cifar10()
    mini = cif.get_train_batch(0, 50000)
    miniX = mini[0]
    miniX = miniX.transpose(0,2,3,1)
    listX = list()
    for i in range(50000):
        listX.append(miniX[i])

    return score.get_inception_score(listX)


# Ejemplo de generacion
def gen_images(gen_size=50000, params_file='models/dcgan/last/params.jl', save_file='gen_images.jl'):
    import sys
    import numpy as np
    import datasets.cifar10_data as dataset
    from src.model import GAN_model
    from src.gan import GAN_trainer
    from lib.theano_utils import floatX
    from lib.rng import np_rng

    img_size = 32
    nchannels = 3
    gen_dim = 100
    init_scale = 0.02
    model = GAN_model(img_size=(img_size, img_size), nchannels=nchannels, gen_dim=gen_dim, init_scale=init_scale)
    model.load(params_file)

    trainer = GAN_trainer(model)
    data = dataset.cifar10()

    z_sample = floatX(np_rng.uniform(-1., 1., size=(gen_size, model.gen_dim)))
    imgs = np.zeros(shape=(gen_size, 3, 32, 32))
    for i in range(gen_size/100):
        sys.stdout.write(".")
        sys.stdout.flush()
        samples = np.asarray(trainer._gen(z_sample[i * 100:(i + 1) * 100]))
        imgs[i * 100:(i + 1) * 100] = data.inv_scale_data(samples)

    joblib.dump(imgs, save_file)


def get_score(images):
    size = images.shape[0]
    images = images.transpose(0, 2, 3, 1)
    listX = list()

    for i in range(size):
        listX.append(images[i])

    return score.get_inception_score(listX)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--imgfile", type=str, default='gen_images.jl')
    parser.add_argument("--outfile", type=str, default='output.jl')
    args = parser.parse_args()

    images = joblib.load(args.imgfile)
    val = get_score(images)
    joblib.dump(val, args.outfile)
