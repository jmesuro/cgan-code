#!/usr/bin/env python2.7
import os
import argparse
import pickle
from time import time
from networks.share_w import share_layers_dis_to_gen, share_layers_gen_to_dis
import datetime
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from datasets import dicaprio
from lasagne.init import Normal
from lib.lasagne_utils import load_network_values, save_network_values
from src import wgan
import datasets.datalist as datalist
from lib import save_images
from networks.dcgan_inv_share_w import build_discriminator, build_generator
from lib.comunes import init_folders

# -----------------------------------------------------------------------------
# Entrena una inversa (solo ultima capa), tomando un discriminador ya entrenado
# Meterle n_iter = iters con que entreno el disc + las que queremos
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='dicaprio')
parser.add_argument("--datapath", type=str,
                    default='/home/jmesuro/datasets/dicaprio/all_movies_ancho35-221_alto70-256.h5')
parser.add_argument("--batch_size", type=int, default=64)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=100000)  # OJO ACA!!!
parser.add_argument("--gen_iter", type=int, default=1)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--exp_name", type=str, default='dcgan_share_w_inv_last_layer')
args = parser.parse_args()

EXPERIMENT_PATH = '/home/jmesuro/cgan-code/out/23_abril_18_56_den_dim_100_bs_128_model_width_128_n_iter_50000_dicaprio/'
# EXPERIMENT_PATH = ''
last_model_dir, best_models_dir, samples_dir, logs, errors_log, resume = \
    init_folders(args, os, datetime, time, experiment_path=EXPERIMENT_PATH)

np_rng = RandomState(args.seed)

# DATASET
nchannels = 3

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()
rmix = T.vector()

# MODELS
gen_network, WsG = build_generator(gen_dim=args.gen_dim,
                                   img_size=(args.img_size, args.img_size),
                                   nchannels=nchannels,
                                   ngf=args.model_width)

dis_network, inv_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                                        nchannels=nchannels,
                                                        ndf=args.model_width,
                                                        W_init=Normal(std=args.init_scale),
                                                        global_pool=True)

if resume:
    # Retomar una corrida interrumpida
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter  # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p" % last_model_dir, "rb"))
    args.n_iter = n_iter
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network_inv.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network_inv.npz'))
    load_network_values(inv_network, os.path.join(last_model_dir, 'inv_network_inv.npz'))
    args_load = pickle.load(open("%s/args.p" % (last_model_dir), "rb"))
else:
    with open(errors_log, 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss inv_loss')
        f.write('\n')


# -----------------------------------------------------
# X -> INV -> invX_real -> GEN -> test_invX_real
# Z -> GEN -> genX -> INV -> invX -> GEN -> test_invX
# Z -> GEN -> test_genX
# -----------------------------------------------------
genX = lasagne.layers.get_output(gen_network, Z)
invX = lasagne.layers.get_output(inv_network, genX)
test_invX = lasagne.layers.get_output(gen_network, invX, deterministic=True)

invX_real = lasagne.layers.get_output(inv_network, X)
test_invX_real = lasagne.layers.get_output(gen_network, invX_real, deterministic=True)

test_genX = lasagne.layers.get_output(gen_network, Z, deterministic=True)


print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, no_default_updates=True, on_unused_input='ignore')
test_inv = theano.function([Z], test_invX, on_unused_input='ignore')
test_inv_real = theano.function([X], test_invX_real, on_unused_input='ignore', allow_input_downcast=True)

print '%.2f seconds to compile theano functions' % (time() - t)

# DATASET
(inf_gen,) = dicaprio.load_64x64(64, data_dir=args.datapath)


def inf_train_gen():
    while True:
        for (images,) in inf_gen():
            yield images


# PRINT
X_sample = inf_train_gen().next()
Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))

save_images.save_images(X_sample.reshape((64, 3, 64, 64)),
                        os.path.join(samples_dir, 'test_groundtruth.jpg'))

samples = np.asarray(test_gen(Z_sample))
color_grid_vis(samples.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir + '/test_gen.png')

samples_inv = np.asarray(test_inv(Z_sample))
color_grid_vis(samples_inv.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir + '/test_inv.png')

samples_inv_real = np.asarray(test_inv_real(X_sample))[:64,:,:,:]
color_grid_vis(samples_inv_real.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir + '/test_inv_real.png')

print "Fin print inv\n"
