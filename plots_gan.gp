# reset
set style data lines
set xlabel "iter"


set title "Loss Function"
set ylabel "loss"
set logscale y
set yrange [0.01:100]
plot "errors.log" u 1:4 t "d_train",\
        "" u 1:5 t "g_train"
