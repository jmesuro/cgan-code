import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import lasagne
from lasagne.layers import InputLayer, DenseLayer, Conv2DLayer, Deconv2DLayer, NonlinearityLayer, GlobalPoolLayer
from src.normalization import BatchNormLayer
from lasagne.layers import ReshapeLayer, FlattenLayer, MergeLayer
from lasagne.init import Normal, GlorotUniform, Constant
from lasagne.nonlinearities import tanh, sigmoid, rectify
from lasagne.nonlinearities import leaky_rectify as lrelu

# De lasagne examples. Modificado
# ##################### Custom layer for middle of VCAE ######################
# This layer takes the mu and sigma (both DenseLayers) and combines them with
# a random vector epsilon to sample values for a multivariate Gaussian
class GaussianSampleLayer(MergeLayer):
    def __init__(self, mu, logsigma, L=10, rng=None, **kwargs):
        self.rng = rng if rng else RandomStreams(lasagne.random.get_rng().randint(1,2147462579))
        super(GaussianSampleLayer, self).__init__([mu, logsigma], **kwargs)
        self.L = L

    def get_output_shape_for(self, input_shapes):
        return (None if input_shapes[0][0] is None else input_shapes[0][0]*self.L, input_shapes[0][1])

    def get_output_for(self, inputs, deterministic=False, **kwargs):
        mu, logsigma = inputs
        shape=(mu.shape[0]*self.L, mu.shape[1])
        if deterministic:
            return T.tile(mu, (self.L, 1))
        else:
            return T.tile(mu, (self.L, 1)) + T.tile(T.exp(logsigma), (self.L,1)) * self.rng.normal(shape)

## TODO: caso continuo
def build_model(alpha=1.0, z_dim=2048, img_size=[64, 64], nchannels=3, L=2, binary=True):
    net = dict()
    net['input'] = InputLayer((None, nchannels, img_size[0], img_size[1]))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    print(lasagne.layers.get_output_shape(net['input']))

    net['enc_l1_Conv'] = Conv2DLayer(net['input'], num_filters=64, filter_size=vis_W_size, stride=vis_stride,
                                     pad='same', nonlinearity=None, name='enc_l1_Conv')
    net['enc_l1_BN'] = BatchNormLayer(net['enc_l1_Conv'], alpha=alpha, epsilon=1e-4, name='enc_l1_BN')
    net['enc_l1_lrelu'] = NonlinearityLayer(net['enc_l1_BN'], nonlinearity=lrelu, name='end_l1_lrelu')
    ishape = lasagne.layers.get_output_shape(net['enc_l1_Conv'])
    print(lasagne.layers.get_output_shape(net['enc_l1_Conv']))

    net['enc_l2_Conv'] = Conv2DLayer(net['enc_l1_lrelu'], num_filters=128, filter_size=5, stride=2,
                                     pad='same', nonlinearity=None, name='enc_l2_Conv')
    net['enc_l2_BN'] = BatchNormLayer(net['enc_l2_Conv'], alpha=alpha, epsilon=1e-4, name='enc_l2_BN')
    net['enc_l2_lrelu'] = NonlinearityLayer(net['enc_l2_BN'], nonlinearity=lrelu, name='end_l2_lrelu')
    ishape = lasagne.layers.get_output_shape(net['enc_l2_Conv'])
    print(lasagne.layers.get_output_shape(net['enc_l2_Conv']))

    net['enc_l3_Conv'] = Conv2DLayer(net['enc_l2_lrelu'], num_filters=256, filter_size=5, stride=2,
                                     pad='same', nonlinearity=None, name='enc_l3_Conv')
    net['enc_l3_BN'] = BatchNormLayer(net['enc_l3_Conv'], alpha=alpha, epsilon=1e-4, name='enc_l3_BN')
    net['enc_l3_lrelu'] = NonlinearityLayer(net['enc_l3_BN'], nonlinearity=lrelu, name='end_l3_lrelu')
    ishape = lasagne.layers.get_output_shape(net['enc_l3_Conv'])
    print(lasagne.layers.get_output_shape(net['enc_l3_Conv']))

    net['output_mu'] = DenseLayer(net['enc_l3_lrelu'], z_dim, nonlinearity=None, name='mu')
#        net['mu_BN'] = BatchNormLayer(net['mu'], alpha=alpha, epsilon=1e-4, name='mu_BN')
#        net['output_mu'] = NonlinearityLayer(net['mu_BN'], nonlinearity=lrelu, name='mu_lrelu')
    print(lasagne.layers.get_output_shape(net['output_mu']))

    net['output_logsigma'] = DenseLayer(net['enc_l3_lrelu'], z_dim, nonlinearity=None, name='logsigma')
#        net['logsigma_BN'] = BatchNormLayer(net['logsigma'], alpha=alpha, epsilon=1e-4, name='logsigma_BN')
#        net['output_logsigma'] = NonlinearityLayer(net['logsigma_BN'], nonlinearity=lrelu, name='logsigma_lrelu')

    net['output_sample'] = GaussianSampleLayer(net['output_mu'], net['output_logsigma'], L=L)
    print(lasagne.layers.get_output_shape(net['output_sample']))

    net['dec_l1'] = DenseLayer(net['output_sample'], 8*8*256, nonlinearity=None, name='dec_l1')
    net['dec_l1_reshape'] = ReshapeLayer(net['dec_l1'], (-1, 256, 8, 8))
    net['dec_l1_BN'] = BatchNormLayer(net['dec_l1_reshape'], alpha=alpha, epsilon=1e-4, name='dec_l1_BN')
    net['dec_l1_lrelu'] = NonlinearityLayer(net['dec_l1_BN'], nonlinearity=lrelu, name='dec_l1_lrelu')
    print(lasagne.layers.get_output_shape(net['dec_l1_reshape']))

    net['dec_l2_deconv'] = Deconv2DLayer(net['dec_l1_lrelu'], num_filters=256, filter_size=5, stride=2,
                                         crop='same', nonlinearity=None, output_size=[16, 16], name='dec_l2_deconv')
    net['dec_l2_BN'] = BatchNormLayer(net['dec_l2_deconv'], alpha=alpha, epsilon=1e-4, name='dec_l2_BN')
    net['dec_l2_lrelu'] = NonlinearityLayer(net['dec_l2_BN'], nonlinearity=lrelu, name='dec_l2_lrelu')
    print(lasagne.layers.get_output_shape(net['dec_l2_deconv']))

    net['dec_l3_deconv'] = Deconv2DLayer(net['dec_l2_lrelu'], num_filters=128, filter_size=5, stride=2,
                                         crop='same', nonlinearity=None, output_size=[32, 32], name='dec_l3_deconv')
    net['dec_l3_BN'] = BatchNormLayer(net['dec_l3_deconv'], alpha=alpha, epsilon=1e-4, name='dec_l3_BN')
    net['dec_l3_lrelu'] = NonlinearityLayer(net['dec_l3_BN'], nonlinearity=lrelu, name='dec_l3_lrelu')
    print(lasagne.layers.get_output_shape(net['dec_l3_deconv']))

    net['dec_l4_deconv'] = Deconv2DLayer(net['dec_l3_lrelu'], num_filters=32, filter_size=vis_W_size, stride=vis_stride,
                                         crop='same', nonlinearity=None, output_size=img_size, name='dec_l4_deconv')
    net['dec_l4_BN'] = BatchNormLayer(net['dec_l4_deconv'], alpha=alpha, epsilon=1e-4, name='dec_l4_BN')
    net['dec_l4_lrelu'] = NonlinearityLayer(net['dec_l4_BN'], nonlinearity=lrelu, name='dec_l4_lrelu')
    print(lasagne.layers.get_output_shape(net['dec_l4_deconv']))

    net['dec_l5_Conv'] = Conv2DLayer(net['dec_l4_lrelu'], num_filters=3, filter_size=5, stride=1,
                                         pad='same', nonlinearity=None, name='dec_l5_Conv')
    net['dec_l5_BN'] = BatchNormLayer(net['dec_l5_Conv'], alpha=alpha, epsilon=1e-4, name='dec_l5_BN')
    net['output'] = NonlinearityLayer(net['dec_l5_BN'], nonlinearity=sigmoid, name='output')
    print(lasagne.layers.get_output_shape(net['dec_l5_Conv']))

    return net

def build_model_mnist(L=2, binary=True):
    net = dict()
    net['input'] = InputLayer((None, 1, 28, 28))

    print(lasagne.layers.get_output_shape(net['input']))

    net['l1'] = DenseLayer(net['input'], num_units=1024, nonlinearity=tanh)

    net['output_mu'] = DenseLayer(net['l1'], 2, nonlinearity=None, name='mu')
    print(lasagne.layers.get_output_shape(net['output_mu']))

    net['output_logsigma'] = DenseLayer(net['l1'], 2, nonlinearity=None, name='logsigma')

    net['output_sample'] = GaussianSampleLayer(net['output_mu'], net['output_logsigma'], L=L)
    print(lasagne.layers.get_output_shape(net['output_sample']))

    net['l1dec'] = DenseLayer(net['output_sample'], 1024, nonlinearity=tanh)
    if binary:
        net['output'] = DenseLayer(net['l1dec'], num_units=28*28, nonlinearity=sigmoid)
    else:
        net['mudec'] = DenseLayer(net['l1dec'], num_units=28*28, nonlinearity=None)
        net['logsigmadec'] = DenseLayer(net['l1dec'], num_units=28*28, nonlinearity=lambda a: rectify(a+10.0)-10.0)
        net['output'] = GaussianSampleLayer(net['mudec'], net['logsigmadec'])

    return net
