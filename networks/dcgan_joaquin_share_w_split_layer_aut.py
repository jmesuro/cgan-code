import theano
import theano.tensor as T
import lasagne
from lasagne.layers import InputLayer, DenseLayer, Conv2DLayer, Deconv2DLayer, NonlinearityLayer, GlobalPoolLayer
from src.normalization import BatchNormLayer
from lasagne.layers import ReshapeLayer, FlattenLayer
from lasagne.init import Normal
from lasagne.nonlinearities import tanh, sigmoid
from src.theanofuns import relu, lrelu




def build_generator(alpha=1.0, gen_dim=100, img_size=[64,64], nchannels=3, ngf=128,
                    W_init=Normal(std=0.02)):
    print 'gen'
    if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
        # La imagen debe ser cuadrada y multiplo de 32
        raise 1

    Z = InputLayer((None, gen_dim))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    hs = 4
    ws = 4
    ishape = lasagne.layers.get_output_shape(Z)
    print ishape

    ### LAYER 1
    h = DenseLayer(Z, num_units=ngf * 8 * hs * ws, W=W_init, b=None, nonlinearity=None, name='gen_l1_Dense')
    w1 = h.W
    h = BatchNormLayer(h, alpha=alpha, epsilon=1e-4, name='gen_l1_BN')
    h = NonlinearityLayer(h, nonlinearity=relu, name='gen_l1_relu')
    ishape1 = lasagne.layers.get_output_shape(h)
    h = ReshapeLayer(h, ([0], ngf * 8, hs, ws))
    ishape = lasagne.layers.get_output_shape(h)
    print str(ishape1) + ' reshape -> ' + str(ishape)

    ### LAYER 2
    x1 = Deconv2DLayer(h, num_filters=ngf * 4,
                       filter_size=5, stride=2,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=[8, 8], name='gen_l%d_Deconv' % 2)
    w2 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN' % 2)
    h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu' % 2)
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 3
    x1 = Deconv2DLayer(h, num_filters=ngf * 2,
                       filter_size=5, stride=2,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=[16, 16], name='gen_l%d_Deconv' % 3)
    w3 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN' % 3)
    h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu' % 3)
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 4
    x1 = Deconv2DLayer(h, num_filters=ngf * 1,
                       filter_size=5, stride=2,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=[32, 32], name='gen_l%d_Deconv' % 4)
    w4 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN' % 4)
    h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu' % 4)
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 5
    x1 = Deconv2DLayer(h, num_filters=nchannels,
                       filter_size=vis_W_size, stride=vis_stride,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=img_size, name='gen_l5_Deconv')
    w5 = x1.W
    x = NonlinearityLayer(x1, nonlinearity=tanh, name='gen_l5_tanh')
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    return x,[w1,w2,w3,w4,w5]

def build_discriminator(alpha=1.0, img_size=[64,64], nchannels=3, ndf=128, W_init=Normal(std=0.02), global_pool=False,
                        wgan_output=False, gen_dim = 100):
    print 'dis'
    if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
        # La imagen debe ser cuadrada y multiplo de 32
        raise 1

    X = InputLayer((None, nchannels, img_size[0], img_size[1]))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    h = X
    ishape = lasagne.layers.get_output_shape(X)
    print ishape


    ### LAYER 1
    x1 = Conv2DLayer(h, num_filters=ndf, filter_size=vis_W_size, stride=vis_stride, pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l1_Conv')
    w1 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l1_BN')
    h = NonlinearityLayer(x2, nonlinearity=lrelu, name='dis_l1_lrelu')
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 2
    x1 = Conv2DLayer(h, num_filters=2 * ndf, filter_size=(5, 5), stride=(2, 2), pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv' % 2)
    w2 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN' % 2)
    h = NonlinearityLayer(x2, nonlinearity=lrelu)
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 3
    x1 = Conv2DLayer(h, num_filters=4 * ndf, filter_size=(5, 5), stride=(2, 2), pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv' % 3)
    w3 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN' % 3)
    h = NonlinearityLayer(x2, nonlinearity=lrelu)
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 4
    x1 = Conv2DLayer(h, num_filters=8 * ndf, filter_size=(5, 5), stride=(2, 2), pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv' % 4)
    w4 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN' % 4)
    h = NonlinearityLayer(x2, nonlinearity=lrelu)
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 5
    if global_pool:
        h = GlobalPoolLayer(h, pool_function=T.max, name='dis_last_code')
    else:
        h = FlattenLayer(h, name='dis_last_code')
    h_shape = lasagne.layers.get_output_shape(h)
    wsig = theano.shared(W_init.sample((h_shape[1], 1)))
    wsigA = theano.shared(W_init.sample((h_shape[1], 100)))

    yA = DenseLayer(h, num_units=gen_dim, W=wsigA, b=None, nonlinearity=None, name='aut_l5_DenseLinear')
    wa = yA.W

    if wgan_output:
        yD = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=None, name='dis_l5_DenseLinear')
        ishape = lasagne.layers.get_output_shape(yD)
        print ishape
        w5 = yD.W
    else:
        yD = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=sigmoid, name='dis_l5_DenseSigmoid')
        ishape = lasagne.layers.get_output_shape(yD)
        print ishape
        w5 = yD.W

    return yD, yA,[w1,w2,w3,w4,w5], wa
