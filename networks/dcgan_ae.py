import theano
import theano.tensor as T
import lasagne
from lasagne.layers import InputLayer, DenseLayer, Conv2DLayer, Deconv2DLayer, NonlinearityLayer, GlobalPoolLayer
from src.normalization import BatchNormLayer
from lasagne.layers import ReshapeLayer, FlattenLayer
from lasagne.init import Normal
from lasagne.nonlinearities import tanh, sigmoid
from network import Network
from src.theanofuns import relu, lrelu


class DcganAENet(Network):
    @staticmethod
    def build_generator(alpha=1.0, gen_dim=100, img_size=[64,64], nchannels=3, ngf=128,
                        W_init=Normal(std=0.02)):
        print 'gen'
        if img_size[0] % 32 is not 0 or img_size[1] != img_size[0]:
            # La imagen debe ser cuadrada y multiplo de 32
            raise 1

        net = dict()
        net['Z'] = InputLayer((None, gen_dim))

        vis_W_size = img_size[0] / 16 + 1
        vis_stride = img_size[0] / 32
        hs = 4
        ws = 4
        ishape = lasagne.layers.get_output_shape(net['Z'])
        print ishape
        net['gen_l1_Dense'] = DenseLayer(net['Z'], num_units=ngf * 8 * hs * ws, W=W_init, b=None, name='gen_l1_Dense')
        net['gen_l1_BN'] = BatchNormLayer(net['gen_l1_Dense'], alpha=alpha, epsilon=1e-4, name='gen_l1_BN')
        net['gen_l1_relu'] = NonlinearityLayer(net['gen_l1_BN'], nonlinearity=relu, name='gen_l1_relu')
        net['gen_l1_reshape'] = ReshapeLayer(net['gen_l1_relu'], ([0], ngf * 8, hs, ws))
        ishape = lasagne.layers.get_output_shape(net['gen_l1_reshape'])
        print ishape

        net['gen_l2_Deconv'] = Deconv2DLayer(net['gen_l1_reshape'], num_filters=ngf * 4,
                                           filter_size=5, stride=2,
                                           crop='same', W=W_init, b=None, nonlinearity=None,
                                           output_size=[8, 8], name='gen_l2_Deconv')
        ishape = lasagne.layers.get_output_shape(net['gen_l2_Deconv'])
        print ishape

        net['gen_l2_BN'] = BatchNormLayer(net['gen_l2_Deconv'], alpha=alpha, epsilon=1e-4, name='gen_l2_BN')
        net['gen_l2_relu'] = NonlinearityLayer(net['gen_l2_BN'], nonlinearity=relu, name='gen_l2_relu')

        net['gen_l3_Deconv'] = Deconv2DLayer(net['gen_l2_relu'], num_filters=ngf * 2,
                                           filter_size=5, stride=2,
                                           crop='same', W=W_init, b=None, nonlinearity=None,
                                           output_size=[16, 16], name='gen_l3_Deconv')
        ishape = lasagne.layers.get_output_shape(net['gen_l3_Deconv'])
        print ishape

        net['gen_l3_BN'] = BatchNormLayer(net['gen_l3_Deconv'], alpha=alpha, epsilon=1e-4, name='gen_l3_BN')
        net['gen_l3_relu'] = NonlinearityLayer(net['gen_l3_BN'], nonlinearity=relu, name='gen_l3_relu')

        net['gen_l4_Deconv'] = Deconv2DLayer(net['gen_l3_relu'], num_filters=ngf * 1,
                                           filter_size=5, stride=2,
                                           crop='same', W=W_init, b=None, nonlinearity=None,
                                           output_size=[32, 32], name='gen_l4_Deconv')
        ishape = lasagne.layers.get_output_shape(net['gen_l4_Deconv'])
        print ishape

        net['gen_l4_BN'] = BatchNormLayer(net['gen_l4_Deconv'], alpha=alpha, epsilon=1e-4, name='gen_l4_BN')
        net['gen_l4_relu'] = NonlinearityLayer(net['gen_l4_BN'], nonlinearity=relu, name='gen_l4_relu')

        net['gen_l5_Deconv'] = Deconv2DLayer(net['gen_l4_relu'], num_filters=nchannels,
                           filter_size=vis_W_size, stride=vis_stride,
                           crop='same', W=W_init, b=None, nonlinearity=None,
                           output_size=img_size, name='gen_l5_Deconv')
        net['gen_l5_tanh'] = NonlinearityLayer(net['gen_l5_Deconv'], nonlinearity=tanh, name='gen_l5_tanh')
        ishape = lasagne.layers.get_output_shape(net['gen_l5_Deconv'])
        print ishape

        ImageLayer = net['gen_l5_tanh']
        genx = net['gen_l5_tanh']

        net['dec_l5_Conv'] = Conv2DLayer(ImageLayer, num_filters=ngf * 1, filter_size=vis_W_size, stride=vis_stride, pad='same',
                         b=None, nonlinearity=None, W=net['gen_l5_Deconv'].W, name='dec_l5_Conv')
        net['dec_l5_BN'] = BatchNormLayer(net['dec_l5_Conv'], alpha=alpha, epsilon=1e-4, name='dec_l5_BN')
        net['dec_l5_relu'] = NonlinearityLayer(net['dec_l5_BN'], nonlinearity=relu, name='dec_l5_relu')
        ishape = lasagne.layers.get_output_shape(net['dec_l5_Conv'])
        print ishape

        net['dec_l4_Conv'] = Conv2DLayer(net['dec_l5_relu'], num_filters=ngf * 2, filter_size=5, stride=2, pad='same',
                         b=None, nonlinearity=None, W=net['gen_l4_Deconv'].W, name='dec_l4_Conv')
        net['dec_l4_BN'] = BatchNormLayer(net['dec_l4_Conv'], alpha=alpha, epsilon=1e-4, name='dec_l4_BN')
        net['dec_l4_relu'] = NonlinearityLayer(net['dec_l4_BN'], nonlinearity=relu, name='dec_l4_relu')
        ishape = lasagne.layers.get_output_shape(net['dec_l4_Conv'])
        print ishape

        net['dec_l3_Conv'] = Conv2DLayer(net['dec_l4_relu'], num_filters=ngf * 4, filter_size=5, stride=2, pad='same',
                         b=None, nonlinearity=None, W=net['gen_l3_Deconv'].W, name='dec_l3_Conv')
        net['dec_l3_BN'] = BatchNormLayer(net['dec_l3_Conv'], alpha=alpha, epsilon=1e-4, name='dec_l3_BN')
        net['dec_l3_relu'] = NonlinearityLayer(net['dec_l3_BN'], nonlinearity=relu, name='dec_l3_relu')
        ishape = lasagne.layers.get_output_shape(net['dec_l3_Conv'])
        print ishape

        net['dec_l2_Conv'] = Conv2DLayer(net['dec_l3_relu'], num_filters=ngf * 8, filter_size=5, stride=2, pad='same',
                         b=None, nonlinearity=None, W=net['gen_l2_Deconv'].W, name='dec_l2_Conv')
        net['dec_l2_BN'] = BatchNormLayer(net['dec_l2_Conv'], alpha=alpha, epsilon=1e-4, name='dec_l2_BN')
        net['dec_l2_relu'] = NonlinearityLayer(net['dec_l2_BN'], nonlinearity=relu, name='dec_l2_relu')
        ishape = lasagne.layers.get_output_shape(net['dec_l2_Conv'])
        print ishape

        net['dec_l1_flatten'] = FlattenLayer(net['dec_l2_relu'])
        net['dec_l1_Dense'] = DenseLayer(net['dec_l1_flatten'], num_units=gen_dim, b=None, nonlinearity=None, name='dec_l1_Dense')
        ishape = lasagne.layers.get_output_shape(net['dec_l1_Dense'])
        print ishape

        encx = net['dec_l1_Dense']

        return genx, encx, net

    @staticmethod
    def build_discriminator(alpha=1.0, img_size=[64,64], nchannels=3, ndf=128, W_init=Normal(std=0.02), global_pool=False, wgan_output=False):
        print 'dis'
        if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
            # La imagen debe ser cuadrada y multiplo de 32
            raise 1

        X = InputLayer((None, nchannels, img_size[0], img_size[1]))

        vis_W_size = img_size[0] / 16 + 1
        vis_stride = img_size[0] / 32
        h = X
        ishape = lasagne.layers.get_output_shape(X)
        print ishape
        x1 = Conv2DLayer(h, num_filters=ndf, filter_size=vis_W_size, stride=vis_stride, pad='same',
                         W=W_init, b=None, nonlinearity=None, name='dis_l1_Conv')
        x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l1_BN')
        h = NonlinearityLayer(x2, nonlinearity=lrelu, name='dis_l1_lrelu')
        ishape = lasagne.layers.get_output_shape(x1)
        print ishape
        for a,i in zip([2,4,8],[2,3,4]):
            x1 = Conv2DLayer(h, num_filters=a * ndf, filter_size=(5, 5), stride=(2, 2), pad='same',
                             W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv'%i)
            x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN'%i)
            h = NonlinearityLayer(x2, nonlinearity=lrelu)
            ishape = lasagne.layers.get_output_shape(x1)
            print ishape

        if global_pool:
            h = GlobalPoolLayer(h, pool_function=T.max, name='dis_last_code')
        else:
            h = FlattenLayer(h, name='dis_last_code')
        h_shape = lasagne.layers.get_output_shape(h)
        wsig = theano.shared(W_init.sample((h_shape[1], 1)))
        if wgan_output:
            y = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=None, name='dis_l5_DenseLinear')
        else:
            y = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=sigmoid, name='dis_l5_DenseSigmoid')

        return y


class AEDcganNet(Network):
    @staticmethod
    def build_generator(alpha=1.0, gen_dim=100, img_size=[64,64], nchannels=3, ngf=128,
                        W_init=Normal(std=0.02)):
        print 'gen'
        if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
            # La imagen debe ser cuadrada y multiplo de 32
            raise 1

        Z = InputLayer((None, gen_dim))

        vis_W_size = img_size[0] / 16 + 1
        vis_stride = img_size[0] / 32
        hs = 4
        ws = 4
        ishape = lasagne.layers.get_output_shape(Z)
        print ishape
        h = DenseLayer(Z, num_units=ngf * 8 * hs * ws, W=W_init, b=None, name='gen_l1_Dense')
        h = BatchNormLayer(h, alpha=alpha, epsilon=1e-4, name='gen_l1_BN')
        h = NonlinearityLayer(h, nonlinearity=relu, name='gen_l1_relu')
        h = ReshapeLayer(h, ([0], ngf * 8, hs, ws))
        ishape = lasagne.layers.get_output_shape(h)
        print ishape

        for a,outsize,i in zip([4, 2, 1],[[8, 8], [16, 16], [32, 32]],[2,3,4]):
            x1 = Deconv2DLayer(h, num_filters=ngf * a,
                               filter_size=5, stride=2,
                               crop='same', W=W_init, b=None, nonlinearity=None,
                               output_size=outsize, name='gen_l%d_Deconv'%i)
            ishape = lasagne.layers.get_output_shape(x1)
            print ishape

            x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN'%i)
            h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu'%i)

        x1 = Deconv2DLayer(h, num_filters=nchannels,
                           filter_size=vis_W_size, stride=vis_stride,
                           crop='same', W=W_init, b=None, nonlinearity=None,
                           output_size=img_size, name='gen_l5_Deconv')
        x = NonlinearityLayer(x1, nonlinearity=tanh, name='gen_l5_tanh')
        ishape = lasagne.layers.get_output_shape(x1)
        print ishape

        return x

    @staticmethod
    def build_discriminator(alpha=1.0, img_size=[64,64], nchannels=3, ndf=128, W_init=Normal(std=0.02), global_pool=False, wgan_output=False, gen_dim=100):
        print 'dis'
        if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
            # La imagen debe ser cuadrada y multiplo de 32
            raise 1

        X = InputLayer((None, nchannels, img_size[0], img_size[1]))

        vis_W_size = img_size[0] / 16 + 1
        vis_stride = img_size[0] / 32
        h = X
        ishape = lasagne.layers.get_output_shape(X)
        print ishape
        x1 = Conv2DLayer(h, num_filters=ndf, filter_size=vis_W_size, stride=vis_stride, pad='same',
                         W=W_init, b=None, nonlinearity=None, name='dis_l1_Conv')
        x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l1_BN')
        h = NonlinearityLayer(x2, nonlinearity=lrelu, name='dis_l1_lrelu')
        ishape = lasagne.layers.get_output_shape(x1)
        print ishape
        for a,i in zip([2,4,8],[2,3,4]):
            x1 = Conv2DLayer(h, num_filters=a * ndf, filter_size=(5, 5), stride=(2, 2), pad='same',
                             W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv'%i)
            x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN'%i)
            h = NonlinearityLayer(x2, nonlinearity=lrelu)
            ishape = lasagne.layers.get_output_shape(x1)
            print ishape

        if global_pool:
            h = GlobalPoolLayer(h, pool_function=T.max, name='dis_last_code')
        else:
            h = FlattenLayer(h, name='dis_last_code')
        h_shape = lasagne.layers.get_output_shape(h)
        wsig = theano.shared(W_init.sample((h_shape[1], 1)))
        if wgan_output:
            y = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=None, name='dis_l5_DenseLinear')
        else:
            y = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=sigmoid, name='dis_l5_DenseSigmoid')

        z = DenseLayer(h, num_units=gen_dim, nonlinearity=None, name='ae_l5_DenseLinear')
        return y, z
