import theano
import theano.tensor as T
import lasagne
from lasagne.layers import InputLayer, DenseLayer, Conv2DLayer, Deconv2DLayer, NonlinearityLayer, GlobalPoolLayer
from src.normalization import BatchNormLayer_Uzal as BatchNormLayer
from lasagne.layers import ReshapeLayer, FlattenLayer
from lasagne.init import Normal
from lasagne.nonlinearities import tanh, sigmoid
from src.theanofuns import relu, lrelu


def build_generator(alpha=1.0, gen_dim=100, img_size=[64,64], nchannels=3, ngf=128,
                    W_init=Normal(std=0.02), batchnormlayer=BatchNormLayer):
    print 'gen'
    if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
        # La imagen debe ser cuadrada y multiplo de 32
        raise 1

    Z = InputLayer((None, gen_dim))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    hs = 4
    ws = 4
    ishape = lasagne.layers.get_output_shape(Z)
    print ishape
    h = DenseLayer(Z, num_units=ngf * 8 * hs * ws, W=W_init, b=None, nonlinearity=None, name='gen_l1_Dense')
    h = batchnormlayer(h, alpha=alpha, epsilon=1e-4, name='gen_l1_BN')
    h = NonlinearityLayer(h, nonlinearity=relu, name='gen_l1_relu')
    h = ReshapeLayer(h, ([0], ngf * 8, hs, ws))
    ishape = lasagne.layers.get_output_shape(h)
    print ishape

    for a,outsize,i in zip([4, 2, 1],[[8, 8], [16, 16], [32, 32]],[2,3,4]):
        x1 = Deconv2DLayer(h, num_filters=ngf * a,
                           filter_size=5, stride=2,
                           crop='same', W=W_init, b=None, nonlinearity=None,
                           output_size=outsize, name='gen_l%d_Deconv'%i)
        ishape = lasagne.layers.get_output_shape(x1)
        print ishape

        x2 = batchnormlayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN'%i)
        h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu'%i)

    x1 = Deconv2DLayer(h, num_filters=nchannels,
                       filter_size=vis_W_size, stride=vis_stride,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=img_size, name='gen_l5_Deconv')
    x = NonlinearityLayer(x1, nonlinearity=tanh, name='gen_l5_tanh')
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    return x

def build_discriminator(alpha=1.0, img_size=[64,64], nchannels=3, ndf=128,
                        W_init=Normal(std=0.02), global_pool=False, wgan_output=False, batchnormlayer=BatchNormLayer):
    print 'dis'
    if img_size[0] % 32 is not 0 or img_size[1]!=img_size[0]:
        # La imagen debe ser cuadrada y multiplo de 32
        raise 1

    X = InputLayer((None, nchannels, img_size[0], img_size[1]))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    h = X
    ishape = lasagne.layers.get_output_shape(X)
    print ishape
    x1 = Conv2DLayer(h, num_filters=ndf, filter_size=vis_W_size, stride=vis_stride, pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l1_Conv')
    x2 = batchnormlayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l1_BN')
    h = NonlinearityLayer(x2, nonlinearity=lrelu, name='dis_l1_lrelu')
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape
    for a,i in zip([2,4,8],[2,3,4]):
        x1 = Conv2DLayer(h, num_filters=a * ndf, filter_size=(5, 5), stride=(2, 2), pad='same',
                         W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv'%i)
        x2 = batchnormlayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN'%i)
        h = NonlinearityLayer(x2, nonlinearity=lrelu)
        ishape = lasagne.layers.get_output_shape(x1)
        print ishape

    if global_pool:
        h = GlobalPoolLayer(h, pool_function=T.max, name='dis_last_code')
    else:
        h = FlattenLayer(h, name='dis_last_code')
    h_shape = lasagne.layers.get_output_shape(h)
    wsig = theano.shared(W_init.sample((h_shape[1], 1)))
    if wgan_output:
        y = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=None, name='dis_l5_DenseLinear')
    else:
        y = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=sigmoid, name='dis_l5_DenseSigmoid')

    return y
