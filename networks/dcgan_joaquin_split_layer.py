import theano
import lasagne
from lasagne.layers import InputLayer, DenseLayer, Conv2DLayer, Deconv2DLayer, NonlinearityLayer, GlobalPoolLayer
from src.normalization import BatchNormLayer
from lasagne.layers import ReshapeLayer, FlattenLayer
from lasagne.init import Normal
from lasagne.nonlinearities import tanh, sigmoid
from src.theanofuns import relu, lrelu


def build_generator(alpha=1.0, gen_dim=100, img_size=[64, 64], nchannels=3, ngf=128,
                    W_init=Normal(std=0.02), bs=128):
    print 'gen'
    if img_size[0] % 32 is not 0 or img_size[1] != img_size[0]:
        # La imagen debe ser cuadrada y multiplo de 32
        raise 1

    Z = InputLayer((bs, gen_dim))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    hs = 4
    ws = 4
    ishape = lasagne.layers.get_output_shape(Z)
    print ishape

    ### LAYER 1
    h = DenseLayer(Z, num_units=ngf * 8 * hs * ws, W=W_init, b=None, nonlinearity=None, name='gen_l1_Dense')
    w1 = h.W
    h = BatchNormLayer(h, alpha=alpha, epsilon=1e-4, name='gen_l1_BN')
    h = NonlinearityLayer(h, nonlinearity=relu, name='gen_l1_relu')
    ishape1 = lasagne.layers.get_output_shape(h)
    h = ReshapeLayer(h, ([0], ngf * 8, hs, ws))
    ishape = lasagne.layers.get_output_shape(h)
    print str(ishape1) + ' reshape -> ' + str(ishape)

    ### LAYER 2
    sh1 = lasagne.layers.get_output_shape(h)[1] / 2
    h = ReshapeLayer(h, (bs * 2, sh1, [2], [3]))
    ishape1 = lasagne.layers.get_output_shape(h)
    print str(ishape) + ' reshape -> ' + str(ishape1)
    x1 = Deconv2DLayer(h, num_filters=ngf * 4 / 2,
                       filter_size=5, stride=2,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=[8, 8], name='gen_l%d_Deconv' % 2)
    sh1 = lasagne.layers.get_output_shape(x1)[1] * 2
    x1 = ReshapeLayer(x1, (bs, sh1, [2], [3]))
    ishape = lasagne.layers.get_output_shape(x1)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    w2 = x1.input_layer.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN' % 2)
    h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu' % 2)

    ### LAYER 3
    sh1 = lasagne.layers.get_output_shape(h)[1] / 2
    h = ReshapeLayer(h, (bs * 2, sh1, [2], [3]))
    ishape1 = lasagne.layers.get_output_shape(h)
    print str(ishape) + ' reshape -> ' + str(ishape1)
    x1 = Deconv2DLayer(h, num_filters=ngf * 2 / 2,
                       filter_size=5, stride=2,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=[16, 16], name='gen_l%d_Deconv' % 3)
    sh1 = lasagne.layers.get_output_shape(x1)[1] * 2
    x1 = ReshapeLayer(x1, (bs, sh1, [2], [3]))
    ishape = lasagne.layers.get_output_shape(x1)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    w3 = x1.input_layer.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN' % 3)
    h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu' % 3)

    ### LAYER 4
    sh1 = lasagne.layers.get_output_shape(h)[1] / 2
    h = ReshapeLayer(h, (bs * 2, sh1, [2], [3]))
    ishape1 = lasagne.layers.get_output_shape(h)
    print str(ishape) + ' reshape -> ' + str(ishape1)
    x1 = Deconv2DLayer(h, num_filters=ngf * 1 / 2,
                       filter_size=5, stride=2,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=[32, 32], name='gen_l%d_Deconv' % 4)
    sh1 = lasagne.layers.get_output_shape(x1)[1] * 2
    x1 = ReshapeLayer(x1, (bs, sh1, [2], [3]))
    ishape = lasagne.layers.get_output_shape(x1)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    w4 = x1.input_layer.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='gen_l%d_BN' % 4)
    h = NonlinearityLayer(x2, nonlinearity=relu, name='gen_l%d_relu' % 4)

    ### LAYER 5
    x1 = Deconv2DLayer(h, num_filters=nchannels,
                       filter_size=vis_W_size, stride=vis_stride,
                       crop='same', W=W_init, b=None, nonlinearity=None,
                       output_size=img_size, name='gen_l5_Deconv')
    w5 = x1.W
    x = NonlinearityLayer(x1, nonlinearity=tanh, name='gen_l5_tanh')
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    return x, [w1, w2, w3, w4, w5]


def build_discriminator(alpha=1.0, img_size=[64, 64], nchannels=3, ndf=128, W_init=Normal(std=0.02), bs=128,
                        wgan_output=False):
    print 'dis'
    if img_size[0] % 32 is not 0 or img_size[1] != img_size[0]:
        # La imagen debe ser cuadrada y multiplo de 32
        raise 1

    X = InputLayer((bs, nchannels, img_size[0], img_size[1]))

    vis_W_size = img_size[0] / 16 + 1
    vis_stride = img_size[0] / 32
    h = X
    ishape = lasagne.layers.get_output_shape(X)
    print ishape

    ### LAYER 1
    x1 = Conv2DLayer(h, num_filters=ndf, filter_size=vis_W_size, stride=vis_stride, pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l1_Conv')
    w1 = x1.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l1_BN')
    h = NonlinearityLayer(x2, nonlinearity=lrelu, name='dis_l1_lrelu')
    ishape = lasagne.layers.get_output_shape(x1)
    print ishape

    ### LAYER 2
    sh1 = lasagne.layers.get_output_shape(h)[1] / 2
    h = ReshapeLayer(h, (bs * 2, sh1, [2], [3]))
    ishape1 = lasagne.layers.get_output_shape(h)
    print str(ishape) + ' reshape -> ' + str(ishape1)
    x1 = Conv2DLayer(h, num_filters=2 * ndf / 2, filter_size=(5, 5), stride=(2, 2), pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv' % 2)
    sh1 = lasagne.layers.get_output_shape(x1)[1] * 2
    x1 = ReshapeLayer(x1, (bs, sh1, [2], [3]))
    ishape1 = ishape
    ishape = lasagne.layers.get_output_shape(x1)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    w2 = x1.input_layer.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN' % 2)
    h = NonlinearityLayer(x2, nonlinearity=lrelu)
    ishape1 = lasagne.layers.get_output_shape(x1)

    ### LAYER 3
    sh1 = lasagne.layers.get_output_shape(h)[1] / 2
    h = ReshapeLayer(h, (bs * 2, sh1, [2], [3]))
    ishape = lasagne.layers.get_output_shape(h)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    x1 = Conv2DLayer(h, num_filters=4 * ndf / 2, filter_size=(5, 5), stride=(2, 2), pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv' % 3)
    sh1 = lasagne.layers.get_output_shape(x1)[1] * 2
    x1 = ReshapeLayer(x1, (bs, sh1, [2], [3]))
    ishape1 = ishape
    ishape = lasagne.layers.get_output_shape(x1)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    w3 = x1.input_layer.W
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN' % 3)
    h = NonlinearityLayer(x2, nonlinearity=lrelu)
    ishape1 = lasagne.layers.get_output_shape(x1)

    ## LAYER 4
    sh1 = lasagne.layers.get_output_shape(h)[1] / 2
    h = ReshapeLayer(h, (bs * 2, sh1, [2], [3]))
    ishape = lasagne.layers.get_output_shape(h)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    x1 = Conv2DLayer(h, num_filters=8 * ndf / 2, filter_size=(5, 5), stride=(2, 2), pad='same',
                     W=W_init, b=None, nonlinearity=None, name='dis_l%d_Conv' % 4)
    sh1 = lasagne.layers.get_output_shape(x1)[1] * 2
    x1 = ReshapeLayer(x1, (bs, sh1, [2], [3]))
    ishape1 = ishape
    ishape = lasagne.layers.get_output_shape(x1)
    print str(ishape1) + ' reshape -> ' + str(ishape)
    w4 = x1.input_layer.W  # dudoso, igual tiene sentido que este despues del reshape, para que quede como antes
    x2 = BatchNormLayer(x1, alpha=alpha, epsilon=1e-4, name='dis_l%d_BN' % 4)
    h = NonlinearityLayer(x2, nonlinearity=lrelu)

    ### LAYER 5
    h = FlattenLayer(h, name='dis_last_code')
    h_shape = lasagne.layers.get_output_shape(h)
    wsig = theano.shared(W_init.sample((h_shape[1], 1)))

    if wgan_output:
        yD = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=None, name='dis_l5_DenseSigmoid')
        ishape = lasagne.layers.get_output_shape(yD)
        print ishape
        w5 = yD.W
    else:
        yD = DenseLayer(h, num_units=1, W=wsig, b=None, nonlinearity=sigmoid, name='dis_l5_DenseSigmoid')
        ishape = lasagne.layers.get_output_shape(yD)
        print ishape
        w5 = yD.W

    return yD, [w1, w2, w3, w4, w5]
