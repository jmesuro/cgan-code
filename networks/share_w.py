

def share_layers_dis_to_gen(n_layers, wg, wd):
    if (n_layers == 0):
        return
    if (n_layers == 1):
        wg[1].container.data = wd[3].container.data
        return
    if (n_layers == 2):
        wg[1].container.data = wd[3].container.data
        wg[2].container.data = wd[2].container.data
        return
    if (n_layers== 3):
        wg[1].container.data = wd[3].container.data
        wg[2].container.data = wd[2].container.data
        wg[3].container.data = wd[1].container.data
        return
    if (n_layers == 4):
        wg[1].container.data = wd[3].container.data
        wg[2].container.data = wd[2].container.data
        wg[3].container.data = wd[1].container.data
        wg[4].container.data = wd[0].container.data
        return
    return


def share_layers_gen_to_dis(n_layers, wd, wg):
    if (n_layers == 0):
        return
    if (n_layers == 1):
        wd[3].container.data = wg[1].container.data
        return
    if (n_layers == 2):
        wd[3].container.data = wg[1].container.data
        wd[2].container.data = wg[2].container.data
        return
    if (n_layers == 3):
        wd[3].container.data = wg[1].container.data
        wd[2].container.data = wg[2].container.data
        wd[1].container.data = wg[3].container.data
        return
    if (n_layers== 4):
        wd[3].container.data = wg[1].container.data
        wd[2].container.data = wg[2].container.data
        wd[1].container.data = wg[3].container.data
        wd[0].container.data = wg[4].container.data
        return
    return
