#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lasagne.init import Normal

from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values


from src import gan
import datasets.datalist as datalist
from networks.dcgan import build_discriminator, build_generator

last_model_dir = 'last_model'
best_model_dir = 'best_model'
samples_dir = 'samples'
if not os.path.exists('logs/'):
    os.makedirs('logs/')
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(best_model_dir):
    os.makedirs(best_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)


parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='stl10')
parser.add_argument("--datapath", type=str, default='/share/datasets')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=50000)
parser.add_argument("--gen_iter", type=int, default=1)
parser.add_argument("--dis_iter", type=int, default=30)
parser.add_argument("--monitor_size", type=int, default=196)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--noise_mult", type=int, default=0.9)


args = parser.parse_args()
if os.path.isfile("%s/args.p"%last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p"%last_model_dir, "rb"))
    args.n_iter = n_iter
else:
    resume = False

print args

np_rng = RandomState(args.seed)


# DATASET
data = datalist.get_dataset(args.dataname, args.datapath, img_size=args.img_size)
nchannels = data._nchannels


# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()
data_noise = T.tensor4()
gen_noise = T.tensor4()

# MODELS
gen_network = build_generator(gen_dim=args.gen_dim,
                                       img_size=(args.img_size,args.img_size),
                                       nchannels=nchannels,
                                       ngf=args.model_width,
                                       W_init=Normal(std=args.init_scale))

dis_network = build_discriminator(img_size=(args.img_size, args.img_size),
                                           nchannels=nchannels,
                                           ndf=args.model_width,
                                           W_init=Normal(std=args.init_scale),
                                           global_pool=True)
if resume:
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
    args_load = pickle.load(open("%s/args.p" %(last_model_dir), "rb"))

disX = lasagne.layers.get_output(dis_network, X+data_noise,
                                 batch_norm_use_averages=True,
                                 batch_norm_update_averages=True)

genX = lasagne.layers.get_output(gen_network, Z,
                                 batch_norm_use_averages=True,
                                 batch_norm_update_averages=True)

disgenX = lasagne.layers.get_output(dis_network, genX+gen_noise,
                                    batch_norm_use_averages=True,
                                    batch_norm_update_averages=False)


# LOSS FUNCTIONS
genX_loss, disX_loss, disgenX_loss = gan.compute_loss(disX, disgenX)
gen_loss = genX_loss
dis_loss = disX_loss + disgenX_loss

# PARAMS
dis_params = lasagne.layers.get_all_params(dis_network, trainable=True)
gen_params = lasagne.layers.get_all_params(gen_network, trainable=True)

# UPDATES
dis_updates = adam(dis_loss, dis_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
gen_updates = adam(gen_loss, gen_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_gen = theano.function([Z, gen_noise], gen_loss, updates=gen_updates, on_unused_input='ignore')
train_dis = theano.function([X, Z, data_noise, gen_noise], dis_loss, updates=dis_updates, on_unused_input='ignore')
test_dis = theano.function([X, Z, data_noise, gen_noise], dis_loss, on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], genX, no_default_updates=True, on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)


X_sample = data.get_unlab_batch(0,args.monitor_size)
X_sample = data.image_crop(X_sample, args.img_size)

color_grid_vis(X_sample.transpose(0, 2, 3, 1), (14, 14), samples_dir+'/real_%s_sample.png'%(args.dataname))

Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.monitor_size, args.gen_dim)))


print "starting training"
if not resume:
    with open('errors.log', 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss')
        f.write('\n')

n_epochs = args.n_iter*args.batch_size/data.unlab_size

dis_param_values = lasagne.layers.get_all_param_values(dis_network, trainable=True)

last_it = 0
t = time()
last_save = t - 3601.0 # Fuerza que en la primer iteracion guarde
noise = 0.0
for it in xrange(args.start_iter,args.n_iter):
    epoch = it*args.batch_size/data.unlab_size
    #lasagne.layers.set_all_param_values(dis_network, dis_param_values, trainable=True)
    effective_iters = 0
    Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
    X_batch = data.get_unlab_batch(it * args.dis_iter, args.batch_size)
    X_batch = data.scale_data(data.image_crop(X_batch, args.img_size))

    data_noise_batch = floatX(np_rng.normal(0., noise, size=X_batch.shape))
    gen_noise_batch = floatX(np_rng.normal(0., noise, size=X_batch.shape))
    dis_loss = test_dis(X_batch, Z_batch, data_noise_batch, gen_noise_batch)

    for k_it in range(args.dis_iter):
        if dis_loss<0.1:
            break
        Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
        X_batch = data.get_unlab_batch(it*args.dis_iter+k_it,args.batch_size)
        X_batch = data.scale_data(data.image_crop(X_batch, args.img_size))

        data_noise_batch = floatX(np_rng.normal(0., noise, size=X_batch.shape))
        gen_noise_batch = floatX(np_rng.normal(0., noise, size=X_batch.shape))
        dis_loss = train_dis(X_batch, Z_batch, data_noise_batch, gen_noise_batch)
        effective_iters = k_it

    for k_it in range(args.gen_iter):

        Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
        gen_noise_batch = floatX(np_rng.normal(0., noise, size=X_batch.shape))
        gen_loss = train_gen(Z_batch, gen_noise_batch)

        if gen_loss < dis_loss:
            break

    if dis_loss>10.25:
        noise = noise*args.noise_mult
    if dis_loss<0.0:
        noise = noise/args.noise_mult

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):

        samples = np.asarray(test_gen(Z_sample))
        color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (14, 14), samples_dir+'/%d.png'%it)

        with open('errors.log', 'a') as f:
            f.write( " ".join(map(str, (it+1,(it+1)*args.batch_size,epoch,effective_iters) ))+" ")
            f.write( " ".join(map(str, (dis_loss,gen_loss) ))+" ")
            f.write("\n")

        t2 = time()-t
        t += t2
        horas = t2/(1+it-last_it)/3600.*10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones"%(it+1,args.n_iter,epoch,horas)
        last_it = it+1

    if time()-last_save>3600.: # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
        save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
        pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) )  #almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter # Corrida completada
save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) ) #Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan

print "Fin train gan\n"

