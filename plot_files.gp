set term postscript enhanced color defaultplex "Times-Roman" 11 size 8cm,2cm eps

set output 'temp.eps'

set style data lines
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ccccff' notitle,\
     '' u 1:2 notitle lt 1 lw 0.5 lc rgb 'red'


