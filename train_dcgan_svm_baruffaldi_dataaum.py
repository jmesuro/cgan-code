#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
import distutils.util
from lasagne.updates import adam
from lasagne.init import Normal

from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values, save_arr_values, load_arr_values

from sklearn import svm
from sklearn.cross_validation import cross_val_score
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.externals import joblib

from scipy.misc import imsave
from lib.data_utils import sample_labels

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

from sklearn import metrics
from sklearn.linear_model import LogisticRegression as LR
from sklearn.svm import LinearSVC as LSVC
from sklearn.semi_supervised import label_propagation
from sklearn.metrics import classification_report, confusion_matrix

from datasets.augmentation import Augmentation, zoom_and_crop

from src import gan
import datasets.datalist as datalist
from networks.dcgan import build_discriminator, build_generator

patch_model_dir = 'patch_model'
if not os.path.exists(patch_model_dir):
    print "Error no existe modelo pre entrenado de patches"
env_model_dir = 'env_model'
if not os.path.exists(env_model_dir):
    print "Error no existe modelo pre entrenado de entornos"

last_model_dir = 'last_model' 
best_model_dir = 'best_model'
samples_dir = 'samples'
if not os.path.exists('logs/'):
    os.makedirs('logs/')
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(best_model_dir):
    os.makedirs(best_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)


parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='soja')
parser.add_argument("--datapath", type=str, default='/share/datasets')
parser.add_argument("--datafolder", type=str, default='data')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--n_iter", type=int, default=1)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--monitor_size", type=int, default=196)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--load_transform", type=distutils.util.strtobool, default=True)
parser.add_argument("--load_svm", type=distutils.util.strtobool, default=False)
parser.add_argument("--loaddataset", type=distutils.util.strtobool, default=True)
parser.add_argument("--cross", type=distutils.util.strtobool, default=False)
parser.add_argument("--env", type=distutils.util.strtobool, default=False)
parser.add_argument("--data_aum", type=distutils.util.strtobool, default=True)


print "Corridas previas"
args = parser.parse_args()
"""
if os.path.isfile("%s/args.p"%last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    #args = pickle.load(open("%s/args.p"%last_model_dir, "rb"))
    #args.n_iter = n_iter
else:
    resume = False
"""
resume = True

print args

if os.path.isfile("%s/Code_batches_X.npz"%last_model_dir):
    print '<<<RESUMING FROM PREVIOUS DCGAN TRAIN>>>'
    if args.load_transform:    
        dcgan_completo = True
    else:
        dcgan_completo = False
else:
    dcgan_completo = False



np_rng = RandomState(args.seed)

print "Obtenemos dataset"
# DATASET Supervizado
data = datalist.get_dataset(args.dataname, args.datapath, data_folder=args.datafolder, supervised=True, shuffle=True, env=args.env, cache=args.loaddataset) 

if args.data_aum:
    augm = Augmentation(rotation_range=15, # En grados
                        width_shift_range=0, #0.15
                        height_shift_range=0, #0.15
                        horizontal_flip=True,
                        shear_range=0.1745, # En radianes
                        zoom_range=(0.9,1.2), # >1 zoom out; <1 zoom in
                        channel_shift_range=20.0, # 0-255
                        fill_mode='nearest',
                        seed=args.seed)
nchannels = data._nchannels

print "Inicializando..."
# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()


# MODELS
dis_network_patch = build_discriminator(img_size=(args.img_size, args.img_size),
                                           nchannels=nchannels,
                                           ndf=args.model_width,
                                           W_init=Normal(std=args.init_scale),
                                           global_pool=True)

if args.env:
    dis_network_env = build_discriminator(img_size=(args.img_size, args.img_size),
                                           nchannels=nchannels,
                                           ndf=args.model_width,
                                           W_init=Normal(std=args.init_scale),
                                           global_pool=True)

print "Cargando modelos"

if resume:
    load_network_values(dis_network_patch, os.path.join(patch_model_dir, 'dis_network.npz'))
    if args.env:
        load_network_values(dis_network_env, os.path.join(env_model_dir, 'dis_network.npz'))
    #args_load = pickle.load(open("%s/args.p" %(patch_model_dir), "rb"))
else:
    print "Error al cargar el modelo"
    exit()


## ------------------------------------ Load Data Supervised ---------------------------------------- ##


print("Transformando datos...")

XX_train, XX_val, XX_test = data.X_train, data.X_valid, data.X_test
y_train, y_val, y_test = data.y_train, data.y_valid, data.y_test

X_train = XX_train.reshape((XX_train.shape[0],XX_train.shape[1]*XX_train.shape[2]*XX_train.shape[3]))
X_val = XX_val.reshape((XX_val.shape[0],XX_val.shape[1]*XX_val.shape[2]*XX_val.shape[3]))
X_test = XX_test.reshape((XX_test.shape[0],XX_test.shape[1]*XX_test.shape[2]*XX_test.shape[3]))

## Pasamos a minibatches Borrar esto si no se usa
X_sample,y_sample = data.get_train_batch(0,args.monitor_size,one_hot=False)
X_crop = data.image_crop(X_sample, args.img_size)
color_grid_vis(X_crop.transpose(0, 2, 3, 1), (14, 14), samples_dir+'/real_%s_sample.png'%(args.dataname))

if args.data_aum:
    X_sample = augm.random_transform(X_sample)
    X_crop = data.image_crop(X_sample, args.img_size)
    color_grid_vis(X_crop.transpose(0, 2, 3, 1), (14, 14), samples_dir+'/augmented_%s_sample.png'%(args.dataname))
    #Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.monitor_size, args.gen_dim)))



"""
print ("Size X_train: ",X_train.shape)
print (X_val.shape)
print (X_test.shape)
print ("Size y_train: ",y_train.shape)
print (y_val.shape)
print (y_test.shape)
"""
print "Datos de entrenamiento cargados"


## --------------------------------------- Last Layer -------------------------------------------------##

# Tomamos la penultima capa para luego en la ultima hacer una red cnn

##Patches
dis_layers_p = lasagne.layers.get_all_layers(dis_network_patch)

for l in dis_layers_p:
    if l.name == 'dis_last_code': #Asumimos que siempre existe
        dis_last_code_layer_p = l
 
print ("Imprimimos nombre de ultima capa patch",dis_last_code_layer_p.name)


##Env
if args.env:
    dis_layers_e = lasagne.layers.get_all_layers(dis_network_env)
    for l in dis_layers_e:
        if l.name == 'dis_last_code': #Asumimos que siempre existe
            dis_last_code_layer_e = l  
    print ("Imprimimos nombre de ultima capa env",dis_last_code_layer_e.name)


## ------------------------------ Creamos las formulas de la red ----------------------------------- ##

X = T.tensor4()

##Patches
code_formula_p = lasagne.layers.get_output(dis_last_code_layer_p,X,deterministic=True)
coding_func_p = theano.function([X],code_formula_p)

##Env
if args.env:
    code_formula_e = lasagne.layers.get_output(dis_last_code_layer_e,X,deterministic=True)
    coding_func_e = theano.function([X],code_formula_e)

# ----------------------------------- Init Built  ------------------------------------ ##

print "vamos a los datos"

n_iter_completa = (X_train.shape[0]/args.batch_size)+1
n_iter = n_iter_completa*args.n_iter
cont_it_compl = 0
print n_iter


t = time()
if dcgan_completo:
    print "Cargamos datos transformados"
    Code_batches_X = load_arr_values(os.path.join(last_model_dir, 'Code_batches_X.npz'))[0]
    Code_batches_y = load_arr_values(os.path.join(last_model_dir, 'Code_batches_y.npz'))[0]
else: 
    print "Transformamos"
    for it in xrange(n_iter):
        if (it % n_iter_completa == 0):    
            cont_it_compl += 1
            print "Iteracion numero: " + str(cont_it_compl)

        ## ----------- Train ----------- ##

        if args.env:
            X_batch,y_batch,X_batch_e = data.get_train_batch(it,args.batch_size,env=args.env,one_hot=False)          
        else:
            X_batch,y_batch = data.get_train_batch(it,args.batch_size,one_hot=False)
        
        if args.data_aum:
            if cont_it_compl >= 1: 
                X_batch = augm.random_transform(X_batch)

        X_batch = data.scale_data(data.image_crop(X_batch,args.img_size))        
        code_batch_p = coding_func_p(X_batch) 

        if args.env:
            code_batch_e = coding_func_e(X_batch_e) 
            code_batch = np.concatenate((code_batch_p,code_batch_e),axis=1)   
        else:        
            code_batch = code_batch_p        

        #concatenar con np los code_batch
        if it==0:
            Code_batches_X = code_batch  
            Code_batches_y = y_batch
        else:
            Code_batches_X = np.concatenate((Code_batches_X,code_batch))
            Code_batches_y = np.concatenate((Code_batches_y,y_batch))

    ## Almacenar Code_batches y cargarlos segun argumento para poder probar cosas mas rapido
    save_arr_values(Code_batches_X, os.path.join(last_model_dir, 'Code_batches_X.npz'))
    save_arr_values(Code_batches_y, os.path.join(last_model_dir, 'Code_batches_y.npz'))        

print Code_batches_X.shape 
print Code_batches_y.shape 

t2 = (time()-t)*3600.
print "Tiempo tardado: "
print t2

print "Init SVM"


##----- SVM---------##

t = time()
if args.load_svm:
    model = joblib.load(os.path.join(last_model_dir, 'svm.pkl'))
    print "Modelo de SVM Cargado"
else:
    ## Ejecutamos un SVM sobre los datos
    if args.cross:
        print "Cross Validation"
        cs = [0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01] 
        #cs = [0.0005, 0.001, 0.002] 

        from sklearn.cross_validation import cross_val_score, KFold
     
        crossvalidation = KFold(n=Code_batches_X.shape[0], n_folds=5, shuffle=True, random_state=args.seed)

        mean_va_accs = []
        for c in cs:
            print 'C = %.4f' %c
            model = svm.SVC(kernel='linear', C=c)
            scores = cross_val_score(model, Code_batches_X, Code_batches_y, cv=crossvalidation, n_jobs=-1)

            print 'Folds: %i, mean accuracy: %.2f std: %.2f' %(len(scores),np.mean(np.abs(scores)),np.std(scores)) 
            #print scores
            #print("Accuracy: %0.6f (+/- %0.6f)" % (scores.mean(), scores.std() * 2))
            mean_va_accs.append(np.mean(np.abs(scores)))

        print mean_va_accs
        best_va_idx = np.argmax(mean_va_accs)
        best_va_c = cs[best_va_idx]
        print 'best c: %.4f'%best_va_c

    else:
        best_va_c = 0.002

    model = svm.SVC(kernel='linear', C=best_va_c, random_state=args.seed)
    print "Entrenamos modelo de SVM"
    model.fit(Code_batches_X, Code_batches_y)
   
    joblib.dump(model, os.path.join(last_model_dir, 'svm.pkl'), compress=9) 

print 'Accuracy: %0.6f' %(model.score(Code_batches_X, Code_batches_y))

t2 = (time()-t)*3600.
print "Tiempo tardado: "
print t2

print "Fin SVM train"

# ----------------------------------- Fin Built  ------------------------------------ ##

t = time()

dir_labels='labels'
if not os.path.exists(dir_labels):
    os.makedirs(dir_labels)
else: 
    os.system('rm -rf %s'%dir_labels)
    os.makedirs(dir_labels)
dir_labels_0='labels/0'
if not os.path.exists(dir_labels_0):
    os.makedirs(dir_labels_0)
dir_labels_1='labels/1'
if not os.path.exists(dir_labels_1):
    os.makedirs(dir_labels_1)
dir_labels_2='labels/2'
if not os.path.exists(dir_labels_2):
    os.makedirs(dir_labels_2)

X_sample,y_sample = data.get_test_batch(0,args.monitor_size,one_hot=False)
X_crop = data.image_crop(X_sample, args.img_size)
color_grid_vis(X_crop.transpose(0, 2, 3, 1), (14, 14), samples_dir+'/t_real_%s_sample.png'%(args.dataname))

if args.data_aum:
    X_sample = augm.random_transform(X_sample)
    X_crop = data.image_crop(X_sample, args.img_size)
    color_grid_vis(X_crop.transpose(0, 2, 3, 1), (14, 14), samples_dir+'/t_augmented_%s_sample.png'%(args.dataname))


classes = np.array((0,0,0))
n_iter_completa = (X_test.shape[0]/args.batch_size)+1
n_iter = n_iter_completa
cont_it_compl = 0
print n_iter
for it in xrange(n_iter):
    if (it % n_iter_completa == 0):    
        cont_it_compl += 1    
        print "Iteracion a dataset completo: " + str(cont_it_compl)
        
    #Test
    if args.env:
        X_Test_batch,y_Test_batch,X_Test_batch_e = data.get_test_batch(it,args.batch_size,env=args.env,one_hot=False)
    else:    
        X_Test_batch,y_Test_batch = data.get_test_batch(it,args.batch_size,one_hot=False)

    X_Test_batch = data.scale_data(data.image_crop(X_Test_batch,args.img_size))
    code_Test_batch_p = coding_func_p(X_Test_batch)

    if args.env:     
        code_Test_batch_e = coding_func_e(X_Test_batch_e)     
        code_Test_batch =  np.concatenate((code_Test_batch_p,code_Test_batch_e),axis=1)   
    else:
        code_Test_batch = code_Test_batch_p

    label = model.predict(code_Test_batch) #predecimos code_bacht correspondiente al X_Test_bacht de la iteracion it
    #print label
    
    tr_acc = metrics.accuracy_score(y_Test_batch, label)
    #print 'minibatch acc %.4f'%tr_acc

    #concatenar con np los code_Test_batch
    if it==0:
        labeles = label
        Code_Test_batches_X = code_Test_batch
        Code_Test_batches_y = y_Test_batch
    else:
        labeles = np.concatenate((labeles,label))
        Code_Test_batches_X = np.concatenate((Code_Test_batches_X,code_Test_batch))
        Code_Test_batches_y = np.concatenate((Code_Test_batches_y,y_Test_batch))        
    for ib in xrange(args.batch_size):
        classes[int(label[ib])] += 1
        if nchannels==3:       
            imsave('%s/%s/cuad_%s.png'%(dir_labels,int(label[ib]),ib+it*args.batch_size),X_Test_batch[ib].transpose(1,2,0))
        else:
            if nchannels==1:
                nX = X_Test_batch[ib].transpose(1,2,0)
                nX = nX.reshape((len(nX[0]),len(nX[1])))
                imsave('%s/%s/cuad_%s.png'%(dir_labels,int(label[ib]),ib+it*args.batch_size),nX)
            else:
                print "Error in imsave, channels != 1 y 3"

print 'Clases: '
print classes
#print Code_Test_batches_X.shape 
#print Code_Test_batches_y.shape 
print "Fin minibatches "

#tr_pred = model.predict(Code_Test_batches_X)
#print tr_pred
#print y_train
#tr_acc = metrics.accuracy_score(Code_Test_batches_y, tr_pred)
#print 'GLOBAL: train acc %.4f'%tr_acc

tr_acc = metrics.accuracy_score(Code_Test_batches_y, labeles)
print 'GLOBAL: test acc %.4f'%tr_acc

t2 = (time()-t)*3600.
print "Tiempo tardado: "
print t2
     
#Cada X_batch tiene 128 cuadraditos de 64x64. Labels es un arreglo de 128 etiquetas que predice de los 128 cuadraditos del X_batch
#print labeles.shape

# ----------------------------------- Init Plot  ------------------------------------ ##

"""
pca = PCA(n_components=2)
X_r = pca.fit(Code_Test_batches_X).transform(Code_Test_batches_X)
print X_r.shape

fig = plt.figure()
colors = ['navy', 'turquoise', 'darkorange']
lw = 2

for color, i in zip(colors, [0, 1, 2]):
    plt.scatter(X_r[labeles == i, 0], X_r[labeles == i, 1], color=color, alpha=.8, lw=lw, label=i)

plt.legend(loc='best', shadow=False, scatterpoints=1)
plt.title('PCA de reconocimiento sobre test')
plt.savefig('dcganPCA.png')
plt.close(fig)


#for i, c in enumerate(centroids):
#    if num_cluster_points.count(i) < 100:
#        os.system('for x in %s/%s/*; do cp $x $x\_2.png ; done'%(dir_labels,i))

#sample_labels('/home/gpu/Tesis/cgan-code',dir_labels,'0',10,10,'samples_0.png')
#sample_labels('/home/gpu/Tesis/cgan-code',dir_labels,'1',10,10,'samples_1.png')
#sample_labels('/home/gpu/Tesis/cgan-code',dir_labels,'2',10,10,'samples_2.png')
"""

## --------------------------------- Fin aprendizaje supervisado ----------------------------------------- ##

print "Fin SVM DCGAN\n"

