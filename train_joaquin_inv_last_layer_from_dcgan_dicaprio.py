#!/usr/bin/env python2.7
import os
import argparse
import pickle
from time import time
from networks.share_w import share_layers_dis_to_gen, share_layers_gen_to_dis
import datetime
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from datasets import dicaprio
from lasagne.init import Normal
from lib.lasagne_utils import load_network_values, save_network_values
from src import wgan
import datasets.datalist as datalist
from lib import save_images
from networks.dcgan_inv_share_w import build_discriminator, build_generator
from lib.comunes import init_folders

# -----------------------------------------------------------------------------
# Entrena una inversa (solo ultima capa), tomando un discriminador ya entrenado
# Meterle n_iter = iters con que entreno el disc + las que queremos
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='dicaprio')
parser.add_argument("--datapath", type=str,
                    default='/home/jmesuro/datasets/dicaprio/all_movies_ancho35-221_alto70-256.h5')
parser.add_argument("--batch_size", type=int, default=64)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=100000)  # OJO ACA!!!
parser.add_argument("--gen_iter", type=int, default=1)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--exp_name", type=str, default='dcgan_share_w_inv_last_layer')
args = parser.parse_args()

EXPERIMENT_PATH = '/home/jmesuro/cgan-code/out/26_abril_05_38_share_w_dicaprio_iter_50000/'
# EXPERIMENT_PATH = ''
last_model_dir, best_models_dir, samples_dir, logs, errors_log, resume = \
    init_folders(args, os, datetime, time, experiment_path=EXPERIMENT_PATH)

np_rng = RandomState(args.seed)

# DATASET
nchannels = 3

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()
rmix = T.vector()

# MODELS
gen_network, WsG = build_generator(gen_dim=args.gen_dim,
                                   img_size=(args.img_size, args.img_size),
                                   nchannels=nchannels,
                                   ngf=args.model_width)

dis_network, inv_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                                        nchannels=nchannels,
                                                        ndf=args.model_width,
                                                        W_init=Normal(std=args.init_scale),
                                                        global_pool=True)

if resume:
    # Retomar una corrida interrumpida
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter  # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p" % last_model_dir, "rb"))
    args.n_iter = n_iter
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
    load_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))
    args_load = pickle.load(open("%s/args.p" % (last_model_dir), "rb"))
else:
    with open(errors_log, 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss inv_loss')
        f.write('\n')

disX = lasagne.layers.get_output(dis_network, X)
genX = lasagne.layers.get_output(gen_network, Z)
disgenX = lasagne.layers.get_output(dis_network, genX)
invX = lasagne.layers.get_output(inv_network, genX)
test_invX = lasagne.layers.get_output(gen_network, invX, deterministic=True)
test_genX = lasagne.layers.get_output(gen_network, Z, deterministic=True)

from lasagne.objectives import squared_error as se

inv_loss = se(invX, Z).mean()

# PARAMS
inv_params = [lasagne.layers.get_all_params(inv_network, trainable=True)[-1]]  # solo actualizamos la capa densa

# UPDATES
inv_updates = adam(inv_loss, inv_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_inv = theano.function([Z], inv_loss, updates=inv_updates, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, no_default_updates=True, on_unused_input='ignore')
test_inv = theano.function([Z], test_invX, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)

# PRINT GROUND_TRUTH

unlab_size = 488435
(inf_gen,) = dicaprio.load_64x64(args.batch_size, data_dir=args.datapath)


def inf_train_gen():
    while True:
        for (images,) in inf_gen():
            yield images


X_sample = inf_train_gen().next()
save_images.save_images(X_sample.reshape((args.batch_size, 3, 64, 64)),
                        os.path.join(samples_dir, 'samples_groundtruth_inv.jpg'))

Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))


def scale_data(X, new_min=-1.0, new_max=1.0):
    scale = 255.0 - 0.0
    new_scale = new_max - new_min
    return floatX((X - 0.0) * new_scale / scale + new_min)


print "starting training"

Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))
unlab_size = unlab_size
n_epochs = args.n_iter * args.batch_size / unlab_size
last_it = 0
t = time()
last_save = t - 3601.0  # Fuerza que en la primer iteracion guarde
dis_loss_value = gen_loss_value = inv_loss_value = 0.
best_inv_loss = 100.
# ITERATION
for it in xrange(args.start_iter, args.n_iter):
    epoch = it * args.batch_size / unlab_size

    # TRAIN INVERSE

    Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
    inv_loss_value = train_inv(Z_batch)

    if inv_loss_value < best_inv_loss:
        inv_loss_value = best_inv_loss
        save_network_values(gen_network, os.path.join(best_models_dir, 'best_gen_network_inv.npz'))
        save_network_values(dis_network, os.path.join(best_models_dir, 'best_dis_network_inv.npz'))
        save_network_values(inv_network, os.path.join(best_models_dir, 'best_inv_network_inv.npz'))

    # ITERATION SAVE
    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):
        # SAMPLE GENERATION
        samples = np.asarray(test_gen(Z_sample))
        samples_inv = np.asarray(test_inv(Z_sample))
        color_grid_vis(samples.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir + '/%d_2.png' % it)
        color_grid_vis(samples_inv.transpose(0, 2, 3, 1),
                       (args.grid_size, args.grid_size), samples_dir + '/%d_2_inv.png' % it)

        # LOG
        with open(errors_log, 'a') as f:
            np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                            dis_loss_value, gen_loss_value, inv_loss_value]], fmt='%1.3e')

        t2 = time() - t
        t += t2
        horas = t2 / (1 + it - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones" % (it + 1, args.n_iter, epoch, horas)
        last_it = it + 1

    # SAFE SAVE
    if time() - last_save > 3600.:  # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network_inv.npz'))
        save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network_inv.npz'))
        save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network_inv.npz'))
        pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

# LAST MODEL SAVE
save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network_inv.npz'))
save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network_inv.npz'))
save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network_inv.npz'))

# Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan
args.start_iter = args.n_iter  # Corrida completada
pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))

print "Fin train gan\n"
