from lib.theano_utils import floatX
from lib.data_utils import OneHot
from image import ImageDataGenerator


class Dataset(object):

    def __init__(self, batch_size=32, X_train=None, y_train=None,
                 X_valid=None, y_valid=None,
                 X_test =None, y_test=None,
                 X_unlab=None, x_min=0, x_max=255, ymin=0.00, ymax=1.00,
                 nclasses=None, **kwargs):

        self.X_train = X_train
        self.X_valid = X_valid
        self.X_test = X_test
        self.y_train = y_train
        self.y_valid = y_valid
        self.y_test = y_test

        self.X_unlab = X_unlab

        self.x_min = x_min
        self.x_max = x_max
        self.ymin = ymin
        self.ymax = ymax

        self.nclasses = nclasses

        self.datagen = ImageDataGenerator(**kwargs)

        if X_train is not None:
            self._nchannels = X_train.shape[1]
            self.datagen.fit(X_train)
            if self.nclasses is None:
                self.nclasses = y_train.shape[1]
        elif X_unlab is not None:
            self._nchannels = X_unlab.shape[1]
            self.datagen.fit(X_train)
        else:
            self._nchannels = None

        if X_train is not None:
            self.X_train_flow = self.datagen.flow(X_train, y_train, batch_size=batch_size, **kwargs)
            self.train_size = X_train.shape[0]
        if X_valid is not None:
            self.X_valid_flow = self.datagen.flow(X_valid, y_valid, batch_size=batch_size, **kwargs)
            self.valid_size = X_valid.shape[0]
        if X_test is not None:
            self.X_test_flow = self.datagen.flow(X_test, y_test, batch_size=batch_size, **kwargs)
            self.test_size = X_test.shape[0]
        if X_unlab is not None:
            self.X_unlab_flow = self.datagen.flow(X_unlab, batch_size=batch_size, **kwargs)
            self.unlab_size = X_unlab.shape[0]

    def get_train_batch(self, random_transform=True):
        X, y  = self.X_train_flow.next(random_transform=random_transform)
        y = floatX(OneHot(y.astype(int)-1,n=10))
        return X, self.smooth_labels(y,self.ymin,self.ymax)

    def get_valid_batch(self, random_transform=False):
        X, y  = self.X_valid_flow.next(random_transform=random_transform)
        y = floatX(OneHot(y.astype(int)-1,n=10))
        return X, self.smooth_labels(y,self.ymin,self.ymax)

    def get_test_batch(self, random_transform=False):
        X, y  = self.X_test_flow.next(random_transform=random_transform)
        y = floatX(OneHot(y.astype(int)-1,n=10))
        return X, self.smooth_labels(y,self.ymin,self.ymax)

    def get_unlab_batch(self, random_transform=False):
        X = self.X_unlab_flow.next(random_transform=random_transform)
        return X

    def scale_data(self, X, new_min=-1.0, new_max=1.0):
        self.new_min = new_min
        self.new_max = new_max
        scale = self.x_max - self.x_min
        new_scale = new_max - new_min
        return floatX((X-self.x_min)*new_scale/scale+new_min)

    def smooth_labels(self, y, ymin=0.1, ymax=0.9):
        return y*(ymax-ymin)+ymin

    def image_crop(self, X, ph, pw=None, random_state=None):

        if pw is None:
            pw = ph

        h, w = X.shape[2:4]

        if h == ph and w == pw:
            return X

        if random_state:
            j = random_state.random_integers(0, h - ph)
            i = random_state.random_integers(0, w - pw)
        else:
            j = int(round((h - ph)/2.))
            i = int(round((w - pw)/2.))

        return X[:,:,j:j+ph, i:i+pw]

    def inv_scale_data(self, X, old_min=-1.0, old_max=1.0):
        scale = self.x_max - self.x_min
        old_scale = old_max - old_min
        return floatX((X-old_min)*scale/old_scale+self.x_min)

