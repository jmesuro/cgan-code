import os
import sys
sys.path.append('..')

import numpy as np

from data import *
from lib.utils import Log

from lib.data_utils import img2array, detec_nchannels

class soja(Dataset):
    def __init__(self, which_set='unlabeled',data_dir='/share/datasets/soja', data_folder='data', supervised=False, shuffle=True, env=False, sup_and_notsup=False, cache=True): #'/home/gpu/Tesis/soja'): # '/media/barufa/D/Tesis/drone_soja/soja'): #'/home/juan/TesinaJM/soja'): #/share/datasets/drone_soya
        
        super(soja, self).__init__(None,"drone_soya", which_set, data_dir)
        
        self._logger              = Log(0)
        
        self._supported_datasets=['unlabeled', 'weed_binary']
        assert which_set in self._supported_datasets, self._logger.log(Log.ERROR, "Dataset not supported")
        
        self._logger.log(Log.INFO, "===== DRONE SOYA =====")

        self._which_set           = which_set
        self._data_dir            = data_dir
        self._side                = 64
        self._big_raw_set         = 'unlabeled'
        self._labels_raw_set      = 'weed_binary'
        self._raw_data_format     = ".png"
        self._data_folder         = data_folder
        self._whole_frames        = "whole"
        self._whole_frames_format = ".png"
        self._raw_metadata_file   = "log"
        self._labels_file         = "labels"
        self._envs_file           = "envs"
        self._data_file           = data_folder
        self._data_file_s         = "data"
        self._env_file            = "env"
        self._built_data_ext      = ".npy"
        self.nclasses             = 2
        
        print "ENV %i"%env
        print "SUPERVISED %i"%supervised
        print "SHUFFLE %i"%shuffle
        print "SUPERVISED AND NOT SUPERVISED %i"%sup_and_notsup
        

        if sup_and_notsup:
            self._which_set = 'unlabeled'
            X_unlab, y_unlab = self.load_data(cache=cache,supervised=False,shuffle=shuffle, env=False) 
            print "Size: " + str(X_unlab.shape) + " labels: " + str(y_unlab.shape) + "\n"   
            self.unlab_size = X_unlab.shape[0] # 100000
            self.X_unlab = X_unlab
            self.y_unlab = y_unlab
            self._which_set = 'weed_binary'
            if env:
				X_supervised, y_supervised, X_env = self.load_data(cache=cache,supervised=True,shuffle=shuffle, env=True) 
				print X_env.shape
            else:
                X_supervised, y_supervised = self.load_data(cache=cache,supervised=True,shuffle=shuffle, env=False)

            print "Supervised: " + str(X_supervised.shape) + " labels: " + str(y_supervised.shape) + "\n"   
            print "Cantidades (train,valid,test)"

			##train 50%, val 25%, test 25%
			#cant_train = int(X_supervised.shape[0]*0.5) #deberia estar en base a un argumento y a la cantidad total de datos
			#cant_val = int(cant_train+X_supervised.shape[0]*0.25)
			#cant_test = int(cant_val+X_supervised.shape[0]*0.25)

			##Actualmente tenemos 5102 para train y 4075 para test
            cant_train = int(5102*0.75+1)
            cant_val = int(cant_train+ 5102*0.25)
			#cant_train = 5102
			#cant_val = cant_train + 0            
            cant_test = 4075

            print (cant_train,cant_val-cant_train,X_supervised.shape[0]-cant_val)
            self.train_size = cant_train
            self.valid_size = cant_val
            self.test_size = cant_test
            self.X_train, self.X_valid, self.X_test = X_supervised[:cant_train], X_supervised[cant_train:cant_val], X_supervised[cant_val:]
            self.y_train, self.y_valid, self.y_test = y_supervised[:cant_train], y_supervised[cant_train:cant_val], y_supervised[cant_val:]  

            if env:
                self.X_e_train, self.X_e_valid, self.X_e_test = X_env[:cant_train], X_env[cant_train:cant_val], X_env[cant_val:]
            else:     
                print "FIN LOAD DATA SUPERVISED AND NOT SUPERVISED"
        else:       
            if not supervised: 
                self._which_set = 'unlabeled'               
                X_unlab, y_unlab = self.load_data(cache=cache,supervised=False,shuffle=shuffle, env=False) 
                #print "Size: " + str(X_unlab.shape[0]) + "\n"   
                self.unlab_size = X_unlab.shape[0] # 100000
                self.X_unlab = X_unlab
                self.y_unlab = y_unlab
            else:
                self._which_set = 'weed_binary'
                if env:
                    X_supervised, y_supervised, X_env = self.load_data(cache=cache,supervised=True,shuffle=shuffle, env=True) 
                    print X_env.shape   
                else:
                    X_supervised, y_supervised = self.load_data(cache=cache,supervised=True,shuffle=shuffle, env=False)        
                
                print "Cantidades (train,valid,test)"

                ##train 50%, val 25%, test 25%
                #cant_train = int(X_supervised.shape[0]*0.5) #deberia estar en base a un argumento y a la cantidad total de datos
                #cant_val = int(cant_train+X_supervised.shape[0]*0.25)
                #cant_test = int(cant_val+X_supervised.shape[0]*0.25)

                ##Actualmente tenemos 5102 para train y 4075 para test
                #cant_train = int(5102*0.75+1)
                #cant_val = int(cant_train+ 5102*0.25)
                cant_train = 5102
                cant_val = cant_train + 0            
                cant_test = 4075

                print (cant_train,cant_val-cant_train,X_supervised.shape[0]-cant_val)
                
                self.train_size = cant_train
                self.valid_size = cant_val
                self.test_size = cant_test
                self.X_train, self.X_valid, self.X_test = X_supervised[:cant_train], X_supervised[cant_train:cant_val], X_supervised[cant_val:]
                self.y_train, self.y_valid, self.y_test = y_supervised[:cant_train], y_supervised[cant_train:cant_val], y_supervised[cant_val:]  
            
                if env:
                    self.X_e_train, self.X_e_valid, self.X_e_test = X_env[:cant_train], X_env[cant_train:cant_val], X_env[cant_val:]

    def path_to_raw_data(self, what):
        #print "Path to raw data\n"
        which_set = ((what == self._labels_file) or (what == self._envs_file)) and self._labels_raw_set or self._big_raw_set
        #print "Data dir: " + self._data_dir + "\n" + which_set + "\n" + what + "\n"
        #for dirpath, dirnames, filenames in os.walk(os.path.join(self._data_dir, which_set, what)):
            #print dirpath, "consumes",
            #for name in filenames:
                #print name        
            #print "Return: " + dirpath + "\n" + dirnames + "\n" + filenames + "\n"       
       
        return os.path.join(self._data_dir, which_set, what)

    def path_to_built_data(self, what, ext=""):
        return os.path.join(self._data_dir, self._which_set, what + ext)

    def get_files_in_raw_data(self):
        self._logger.log(Log.INFO, "Traversing images dir...")
        #print "Get files\n"
        files = []
        for (dirpath, dirnames, filenames) in os.walk(self.path_to_raw_data(self._data_folder)):
            #print "Dir: " + dirpath + dirnames + filenames
            files.extend(os.path.join(dirpath, x) for x in filenames if x.endswith(self._raw_data_format))
        return sorted(files)

    def add_raw_labels(self, labels):
        f = open(self.path_to_raw_data(self._labels_file), "a")
        for index, label in labels.items():
            f.write("%s %s\n" % (str(index), str(label)))

    def make_metadata(self):
        files = self.get_files_in_raw_data()
         
        self._logger.log(Log.INFO, "Building metadata from log...")
        path_to_metadata     = self.path_to_raw_data(self._raw_metadata_file)
        path_to_whole_frames = self.path_to_raw_data(self._whole_frames)
        meta = []
        f = open(path_to_metadata)
        for i, line in enumerate(f):
            data = {}
            data["loc"] = map(int, line.split(" - ")[1].split(":"))
            aux = files[i].split('/video')
            data["video_label"] = int(aux[0][-1])
            data["whole_frame"] = os.path.join(path_to_whole_frames, str(data["video_label"]), 'video' + aux[1][:4], 'frame' + aux[1].split('frame')[1][:4])
            data["whole_frame"] = data["whole_frame"] + self._whole_frames_format
            meta.append(data)

        return meta

    def make_labels(self, supervised=False, env=False):
        print "Make Labels"
        labels = {}
        #print "Supervised: " supervised
        if supervised:        
            try:         
                f = open(self.path_to_raw_data(self._labels_file), "r") 
                if env:
                    try: 
                        f_e = open(self.path_to_raw_data(self._envs_file), "r")
                    except:
                        print "Error al cargar los entornos"
            except:
                return labels
                    
            if env:
                for i, line in enumerate(f):
                    #print ("Linea ",i)
                    line_e = f_e.readline()
                    #print line_e
                    #path, label = map(int, line.split(" "))
                    path, label = line.split(" ")
                    path_e, label_e = line_e.split(" ")    
                    label = int(label)
                    labels[i] = (path,label,path_e)
                    #print ("Index labels: ",i)
                    #print ("Path labels: ",path)
                    #print ("label: ",label)
                return labels
            else:
                for i, line in enumerate(f):
                    #print ("Linea ",i)
                    #path, label = map(int, line.split(" "))
                    path, label = line.split(" ")
                    label = int(label)
                    labels[i] = (path,label)
                    #print ("Index labels: ",i)
                    #print ("Path labels: ",path)
                    #print ("label: ",label)
                return labels
        else:
            return labels

    def make_data(self, supervised=False, env=False):
        print "Make Data"
        files = self.get_files_in_raw_data()
        
        self._nchannels = detec_nchannels(files[0])
        self._logger.log(Log.INFO, "Building labels...")
        labels = self.make_labels(supervised=supervised, env=env)

        if supervised:         
            print len(labels)        

        #print ("Supervised: ",supervised)
        if supervised: #Si tenemos datos en el archivo labels hacemos aprendizaje supervizado, si no hacemos no supervizado
            files = [labels[i][0] for i in range(len(labels))]
            #files = [files[i] for i in labels.keys()]
            if env:
                files_e = [labels[i][2] for i in range(len(labels))]
        
        #if supervised:
         #   print files        

        y = np.array([labels[i][1] for i in range(len(labels))])

        self._logger.log(Log.INFO, "Building images...")

        X = np.array([img2array(path) for path in files])
        
        print X.shape        
        if len(X.shape)<2:
            print "Error al cargar el datasets"
            sys.exit(0)
        
        X = np.transpose(X, (0, 3, 1, 2))
        
        
        self._logger.log(Log.INFO, "Done! Shape X: %s - Shape Y: %s"%(str(X.shape), str(y.shape)))
 
        if env:
            X_e = np.array([img2array(path) for path in files_e])
            
            print X_e.shape        
            if len(X_e.shape)<2:
                print "Error al cargar el datasets"
                sys.exit(0)
            
            X_e = np.transpose(X_e, (0, 3, 1, 2))

            return X, y, X_e
        
        else:
            return X, y

    def load_data(self, ndata=None, shuffle=False, cache=False, supervised=False, env=False):
        #assert cache == (self._data_dir  is not None), self._logger.log(Log.ERROR, "Can't cache without a cache folder, use set_cache_folder() method.")
        print "Cargando dataset"
        
        print ("Supervised: ",supervised)

        if supervised:
            built_data_path   = self.path_to_built_data(self._data_file_s, self._built_data_ext)
        else:
            built_data_path   = self.path_to_built_data(self._data_file, self._built_data_ext)
    
        built_labels_path = self.path_to_built_data(self._labels_file, self._built_data_ext)
        
        if env:        
            built_env_path   = self.path_to_built_data(self._env_file, self._built_data_ext)

        if cache:
            try:
                if supervised:
                    print "No load supervised data"
                    raise
 
                X = np.load(built_data_path)
                y = np.load(built_labels_path)
                
                if env:
                    X_e = np.load(built_env_path)
                print "Cargando..."
            except MemoryError:
                self._logger.log(Log.ERROR, "Not enough memory for loading the dataset!")
                print "Not enough memory for loading the dataset!"                
                raise
            except:
                self._logger.log(Log.INFO, "Failed to load built data files. Building them!")
                print "Failed to load built data files. Building them!"
                X, y = self.make_data(supervised=supervised)
                if env:
                    X_e = self.make_data(supervised=supervised, env=True)
                if not os.path.exists(self.path_to_built_data("")):
                    os.makedirs(self.path_to_built_data(""))
                
                np.save(built_data_path, X)
                np.save(built_labels_path, y)
                if env:
                    np.save(built_env_path, X_e)    
        else:
            if env:
               X, y, X_e = self.make_data(supervised=supervised, env=True)
            else:
               X, y = self.make_data(supervised=supervised)            
            
        assert X.shape[0] == y.shape[0] or y.shape[0] == 0, self._logger.log(Log.ERROR, "X and y shape don't match")

        if shuffle:
            walk = np.random.permutation(X.shape[0])
            #print walk
            X = X[walk]
            if (y.shape[0] > 0): #supervised
                print "New y shuffle"                
                y = y[walk]
            if env:
                print "New X_e shuffle"
                #print X_e.shape                
                X_e = X_e[walk]
        if ndata:
            X = X[:ndata]
            if (y.shape[0] > 0): #supervised
                print "New y ndata" 
                y = y[:ndata]
            if env:
                print "New X_e ndata"                
                X_e = X_e[:ndata]

        print "Datos cargados\n"        

        if env:
            return X, y, X_e
        else:
            return X, y


if __name__ == "__main__":
    print "Main"
    dsgs = soja()
    dsgs.load_data()
