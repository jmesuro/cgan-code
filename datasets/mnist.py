import gzip
import numpy as np
from data import Dataset
from os.path import join

class mnist(Dataset):
    def __init__(self, dir='/share/datasets'):
        # De Lasagne examples
        def load_mnist_images(filename):
            # Read the inputs in Yann LeCun's binary format.
            with gzip.open(filename, 'rb') as f:
                data = np.frombuffer(f.read(), np.uint8, offset=16)
            # The inputs are vectors now, we reshape them to monochrome 2D images,
            # following the shape convention: (examples, channels, rows, columns)
            data = data.reshape(-1, 1, 28, 28)
            return data

        def load_mnist_labels(filename):
            # Read the labels in Yann LeCun's binary format.
            with gzip.open(filename, 'rb') as f:
                data = np.frombuffer(f.read(), np.uint8, offset=8)
            # The labels are vectors of integers now, that's exactly what we want.
            return data

        # We can now download and read the training and test set images and labels.
        X_train = load_mnist_images(join(dir, 'mnist/train-images-idx3-ubyte.gz'))
        y_train = load_mnist_labels(join(dir, 'mnist/train-labels-idx1-ubyte.gz'))
        X_test = load_mnist_images(join(dir, 'mnist/t10k-images-idx3-ubyte.gz'))
        y_test = load_mnist_labels(join(dir, 'mnist/t10k-labels-idx1-ubyte.gz'))

        # We reserve the last 10000 training examples for validation.
        X_train, X_val = X_train[:-10000], X_train[-10000:]
        y_train, y_val = y_train[:-10000], y_train[-10000:]

        # We just return all the arrays in order, as expected in main().
        # (It doesn't matter how we do this as long as we can read them again.)
        super(mnist, self).__init__(X_train, y_train, X_val, y_val, X_test, y_test)
