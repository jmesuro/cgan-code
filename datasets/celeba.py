from os.path import join
import numpy as np
from PIL import Image as pil_image
from data import Dataset


# Esto debe ir a un archivo aparte
def img_to_array(img, data_format=None):
    """Converts a PIL Image instance to a Numpy array.
    # Arguments
        img: PIL Image instance.
        data_format: Image data format.
    # Returns
        A 3D Numpy array.
    # Raises
        ValueError: if invalid `img` or `data_format` is passed.
    """
    if data_format is None:
        data_format = 'channels_last'
    if data_format not in {'channels_first', 'channels_last'}:
        raise ValueError('Unknown data_format: ', data_format)
    # Numpy array x has format (height, width, channel)
    # or (channel, height, width)
    # but original PIL image has format (width, height, channel)
    x = np.asarray(img, dtype='float32')
    if len(x.shape) == 3:
        if data_format == 'channels_first':
            x = x.transpose(2, 0, 1)
    elif len(x.shape) == 2:
        if data_format == 'channels_first':
            x = x.reshape((1, x.shape[0], x.shape[1]))
        else:
            x = x.reshape((x.shape[0], x.shape[1], 1))
    else:
        raise ValueError('Unsupported image shape: ', x.shape)
    return x

def load_img(path, grayscale=False, target_size=None):
    """Loads an image into PIL format.
    # Arguments
        path: Path to image file
        grayscale: Boolean, whether to load the image as grayscale.
        target_size: Either `None` (default to original size)
            or tuple of ints `(img_height, img_width)`.
    # Returns
        A PIL Image instance.
    # Raises
        ImportError: if PIL is not available.
    """
    if pil_image is None:
        raise ImportError('Could not import PIL.Image. '
                          'The use of `array_to_img` requires PIL.')
    img = pil_image.open(path)
    if grayscale:
        if img.mode != 'L':
            img = img.convert('L')
    else:
        if img.mode != 'RGB':
            img = img.convert('RGB')
    if target_size:
        wh_tuple = (target_size[1], target_size[0])
        if img.size != wh_tuple:
            img = img.resize(wh_tuple)
    return img

class celeba(Dataset):
    def __init__(self, dir='/share/datasets', img_size=(160, 160)):
        self.unlab_size = 202599
        self.orig_shape = (218, 178)
        self.x_max = 255.0
        self.x_min = 0.0
        self._nchannels = 3
        self.img_dir = join(dir, 'celeba/img_align_celeba_png')
        if type(img_size) is int:
            self.img_size = (img_size, img_size)
        else:
            self.img_size = img_size

    def _get_batch_idx(self, size, index, batch_size):
        n1 = (index * batch_size) % size
        n2 = ((index + 1) * batch_size - 1) % size + 1
        if n1 > n2:
            return range(n1, size) + range(n2)
        else:
            return range(n1, n2)

    def get_unlab_batch(self, index, batch_size):
        idxs = self._get_batch_idx(self.unlab_size, index, batch_size)
        return np.array([img_to_array(load_img(self.img_dir + '/%06d.png'%(idx+1), target_size=self.img_size), data_format='channels_first') for idx in idxs])

class celeba_64x64(Dataset):
    def __init__(self, dir='/share/datasets'):
        import h5py
        self.unlab_size = 202599
        self.x_max = 255.0
        self.x_min = 0.0
        self._nchannels = 3
        self.h5file = join(dir, 'celeba/celeb.cropped.64x64.h5')
        self.img_size = (64, 64)
        f = h5py.File(self.h5file)

        super(celeba_64x64, self).__init__(X_unlab=f['X'])

class celeba_96x96(Dataset):
    def __init__(self, dir='/share/datasets'):
        import h5py
        self.unlab_size = 202599
        self.x_max = 255.0
        self.x_min = 0.0
        self._nchannels = 3
        self.h5file = join(dir, 'celeba/celeb.cropped.96x96.h5')
        self.img_size = (96, 96)
        f = h5py.File(self.h5file)

        super(celeba_96x96, self).__init__(X_unlab=f['X'])

def mk_celeba_64x64(img_dir, filename):
    import h5py
    f = h5py.File(filename)
    X = f.create_dataset('X', (202599, 3, 64, 64), dtype='f')
    orig_shape = (178, 218)
    crop = (orig_shape[0]/10, orig_shape[1]/10)
    for idx in range(202599):
        img = load_img(img_dir + '/%06d.png'%(idx+1))
        img = img.crop((crop[0], crop[1]*2, orig_shape[0]-crop[0], orig_shape[1]-crop[1]))
        img = img.resize((64, 64))
        img = img_to_array(img, data_format='channels_first')
        X[idx] = img
        if idx%100==0:
            print("%d..."%idx)
    f.close()

def mk_celeba_96x96(img_dir, filename):
    import h5py
    f = h5py.File(filename)
    X = f.create_dataset('X', (202599, 3, 96, 96), dtype='f')
    orig_shape = (178, 218)
    crop = (orig_shape[0]/10, orig_shape[1]/10)
    for idx in range(202599):
        img = load_img(img_dir + '/%06d.png'%(idx+1))
        img = img.crop((crop[0], crop[1], orig_shape[0]-crop[0], orig_shape[1]-crop[1]))
        img = img.resize((96, 96))
        img = img_to_array(img, data_format='channels_first')
        X[idx] = img
        if idx%100==0:
            print("%d..."%idx)
    f.close()
