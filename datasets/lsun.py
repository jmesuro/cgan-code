import numpy as np

from lib.theano_utils import floatX, sharedX
from lib.data_utils import OneHot

from lsunio import LSunDataProvider
from data import Dataset

class lsun(Dataset):

    def __init__(self, img_size=227, data = (None, None, None)):

        (train_db, valid_db, test_db) = data

        if train_db is None:
            train_db='/share/datasets/lsun/bedroom_train_lmdb'

        self.dp = LSunDataProvider(train_db, None, None)
        self.img_size = img_size
        self.ymin = 0.00
        self.ymax = 1.00

        self.x_min = 0.00
        self.x_max = 255.00

        self.unlab_size = 3033042

        self._nchannels = 3

    def get_unlab_batch(self, index, batch_size):
        X = self.dp.get_train_mb(batch_size, self.img_size).next()
        return X

    def get_train_batch(self, index, batch_size):
        raise NotImplemented
