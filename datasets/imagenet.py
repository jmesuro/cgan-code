import numpy as np

from lib.theano_utils import floatX, sharedX
from lib.data_utils import OneHot

from imageio import ImageNetDataProvider
from data import Dataset

class imagenet(Dataset):

    def __init__(self, cropped_size=227, data = (None, None, None, None)):

        (mean_file, train_db, valid_db, test_db) = data

        if mean_file is None:
            mean_file = '/share/datasets/imagenet/imagenet_mean.binaryproto'

        if train_db is None:
            train_db='/share/datasets/imagenet/ilsvrc12_train_lmdb'

        if valid_db is None:
            val_db='/share/datasets/imagenet/ilsvrc12_val_lmdb'

        if test_db is None:
            test_db='/share/datasets/imagenet/ilsvrc12_test_lmdb'

        self.dp = ImageNetDataProvider(mean_file, train_db, val_db, test_db)
        self.cropped_size = cropped_size
        self.ymin = 0.00
        self.ymax = 1.00

        self.x_min = 0.00
        self.x_max = 255.00


        self.unlab_size = 3.2 * 1000000


    def get_unlab_batch(self, index, batch_size):
        (X, _) = self.dp.get_train_mb(batch_size, self.cropped_size).next()
        return X

    def get_train_batch(self, index, batch_size):

        (X, y) = self.dp.get_train_mb(batch_size, self.cropped_size).next()
        y = floatX(OneHot(y.astype(int)-1,n=1000))
        return X, self.smooth_labels(y,self.ymin,self.ymax)


