import sys,os,gc
import lmdb
import numpy as np
import numpy.random
import cv2
from caffe_pb2 import Datum

# this code was based on: https://github.com/fyu/lsun/blob/master/data.py

class LSunDataProvider:
    def __init__(self, train_db, val_db, test_db):

        self.train_db = train_db
        self.val_db = val_db
        self.test_db = test_db

    def get_train_mb(self, mb_size, size=227):
        env = lmdb.open(self.train_db, readonly=True, map_size=1099511627776, max_readers=100)
        samples = np.zeros([mb_size, 3, size, size], dtype=np.float32)
        count = 0
        with env.begin(write=False) as txn:
            cursor = txn.cursor()
            for key, value in cursor:
                im = cv2.imdecode(numpy.fromstring(value, dtype=numpy.uint8), 1)
                im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
                im = cv2.resize(im, (size, size))
                im = np.transpose(im, (2, 0, 1))

                samples[count, :] = im
                count = count + 1
                if count == mb_size:
                    yield samples
                    count = 0
        if count != mb_size:
            delete_idx = np.arange(count, mb_size)
            yield np.delete(samples, delete_idx, 0)

if __name__ == '__main__':
    dp = LSunDataProvider(
            train_db='/share/datasets/lsun/bedroom_train_lmdb',
            val_db=None,
            test_db=None)
    count = 0
    for samples in dp.get_train_mb(256):
        print count, ':', samples.shape
        print samples[0,0:10]
        count = count + 1
        if count % 10 == 0:
            break
