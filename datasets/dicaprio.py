# -*- coding: utf-8 -*-

import os
import numpy as np
import time

import random
import cPickle as pkl
from lib.utils2 import listFiles, listFillTracks, load_img, pathCheck
from PIL import Image as pil_image
import h5py


#~ def make_generator(path, n_files, batch_size):
    #~ epoch_count = [1]
    #~ def get_epoch():
        #~ images = np.zeros((batch_size, 3, 64, 64), dtype='int32')
        #~ files = range(n_files)
        #~ random_state = np.random.RandomState(epoch_count[0])
        #~ random_state.shuffle(files)
        #~ epoch_count[0] += 1
        #~ for n, i in enumerate(files):
            #~ image = scipy.misc.imread(os.path.join(path, str(i+1).zfill(len(str(n_files)))+".jpg"))
            #~ images[n % batch_size] = image.transpose(2,0,1)
            #~ if n > 0 and n % batch_size == 0:
                #~ yield (images,)
    #~ return get_epoch

def make_generator(path, n_files, batch_size, all_to_ram = True):
    epoch_count = [1]
    fp = h5py.File(path, 'r')
    if all_to_ram:
        all_unlab_data_h5 = np.array(fp["X"])
    else:
        all_unlab_data_h5 = fp["X"]
    def get_epoch():
        images = np.zeros((batch_size, 3, 64, 64), dtype='int32')
        files = range(n_files)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n,idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield (images,)
    return get_epoch

#~ def load_64x64(batch_size, data_dir='/home/ishaan/data/imagenet64'):
    #~ return (
        #~ make_generator(os.path.join(data_dir,'train'), 488435, batch_size),
        #~ make_generator(os.path.join(data_dir,'valid'), 46530, batch_size)
    #~ )

def load_64x64(batch_size, data_dir='/home/ishaan/data/imagenet64', all_to_ram = True):
    return (
        make_generator(data_dir, 488435, batch_size, all_to_ram = all_to_ram),
    )

def createFullRelevantFillTrack(savePath,directoryOfFTs):
    objFiles = listFillTracks(directoryOfFTs)
    relevantFTs = []
    for objF in objFiles:
        with open(objF,'rb') as fp:
            fillTracks = pkl.load(fp)
            fp.close()
        for ft in fillTracks:
            path=ft.get_facePath()
            isRelevant = (path.split("/")[-3] == "relevantID")
            if isRelevant:
                relevantFTs.append(ft)
    with open(os.path.join(savePath,"fullRelevantFillTracks.obj"),'wb') as fp:
        pkl.dump(relevantFTs,fp)
        fp.close()


#exec example: mk_diCaprio64_jpg(split_prop=0.1,img_dir="/home/lucas/faceDetector/identified/leo", save_dir="/home/lucas/tesina_zanetti/datasets/diCaprio/all_movies_restrictedFace_split0.1_jpg", fillTracksPath="/home/lucas/tesina_zanetti/fillTracks/fullRelevantFillTracks.obj", fillLimit=25)
def mk_diCaprio64_jpg(split_prop=0.10,img_dir="/home/lucas/faceDetector/identified/leo", save_dir="/home/lucas/tesina_zanetti/datasets/diCaprio/all_movies_restrictedFace_jpg", fillTracksPath="/home/lucas/tesina_zanetti/fillTracks/fullRelevantFillTracks.obj", fillLimit=25):
    from tqdm import tqdm

    print("Analizando directorio: {}".format(img_dir))
    imgs = listFiles(img_dir)

    print("Creando diccionario con fillTracks: {}".format(fillTracksPath))
    with open(fillTracksPath,'rb') as fp:
        fillTracks = pkl.load(fp)
    ftDict = {}
    for ft in fillTracks:
        ftDict[(os.path.basename(ft.get_facePath()),ft.get_fg())] = ft.get_fills()

    print("Borrando imgs con fillTracks>fillLimit={}".format(fillLimit))
    to_delete = []
    for im in imgs:
        imFG = int(im.split("/")[-2].split("fgID")[-1])
        k = (os.path.basename(im),imFG)
        imFills=ftDict[k]
        m = max(imFills.values())
        if m > fillLimit:
            to_delete.append(im)
            #~ print("{} con maxFill={}>{}, borrada".format(k,m,fillLimit))

    to_delete = set(to_delete)
    imgs = [i for i in imgs if not(i in to_delete)]
    n_files = len(imgs)
    print("Quedaron {} imágenes luego de filtrar".format(n_files))

    random.seed(2017)
    np.random.shuffle(imgs)
    valid_cant = int(n_files*split_prop)
    valid_imgs_idxs = random.sample(xrange(0,n_files),valid_cant)
    print("")
    print("random sample (20 ejs): {}".format(valid_imgs_idxs[:20]))
    print("")
    valid_imgs_idxs = set(valid_imgs_idxs)
    train_imgs_idxs = [i for i in range(0,n_files) if i not in valid_imgs_idxs]
    train_cant = len(train_imgs_idxs)
    valid_imgs_idxs = list(valid_imgs_idxs)
    #~ train_imgs = [im for i,im in enumerate(imgs) if i not in valid_imgs_idxs]
    #~ valid_imgs = [im for i,im in enumerate(imgs) if i in valid_imgs_idxs]
    print("{} imgs de train y {} imgs de valid".format(train_cant,valid_cant))

    print("")
    print("Cargando train_imgs a RAM, recortando y redimensionando:")
    train_imgs = []
    for i in tqdm(range(0,train_cant)):
        img = load_img(imgs[train_imgs_idxs[i]])
        img = img.crop((35,70,221,256)) #restringimos pixeles a: 35 a 221 a lo ancho y 70 a 256 a lo alto
        img = img.resize((64, 64),resample=pil_image.LANCZOS)
        train_imgs.append(img)
    print("Guardando train_imgs a disco en: {}".format(os.path.join(save_dir,"train")))
    train_imgs = enumerate(train_imgs)
    for i,img in tqdm(train_imgs,total=train_cant):
        save_path = os.path.join(pathCheck(os.path.join(save_dir,"train")),str(i+1).zfill(len(str(n_files)))+".jpg")
        img.save(save_path)

    print("")
    print("Cargando valid_imgs a RAM, recortando y redimensionando:")
    valid_imgs = []
    for i in tqdm(range(0,valid_cant)):
        img = load_img(imgs[valid_imgs_idxs[i]])
        img = img.crop((35,70,221,256)) #restringimos pixeles a: 35 a 221 a lo ancho y 70 a 256 a lo alto
        img = img.resize((64, 64),resample=pil_image.LANCZOS)
        valid_imgs.append(img)
    print("Guardando valid_imgs a disco en: {}".format(os.path.join(save_dir,"valid")))
    valid_imgs = enumerate(valid_imgs)
    for i,img in tqdm(valid_imgs,total=valid_cant):
        save_path = os.path.join(pathCheck(os.path.join(save_dir,"valid")),str(i+1).zfill(len(str(n_files)))+".jpg")
        img.save(save_path)

    #~ print("Guardando valid_imgs redimensionadas y filtradas en: {}".format(os.path.join(save_dir,"valid")))
    #~ for i in tqdm(range(0,len(valid_imgs_idxs))):
        #~ img = load_img(imgs[i])
        #~ img = img.crop((35,70,221,256)) #restringimos pixeles a: 35 a 221 a lo ancho y 70 a 256 a lo alto
        #~ img = img.resize((64, 64),resample=pil_image.LANCZOS)
        #~ save_path = os.path.join(pathCheck(os.path.join(save_dir,"valid")),str(i+1).zfill(len(str(n_files)))+".jpg")
        #~ img.save(save_path)

if __name__ == '__main__':
    train_gen, valid_gen = load(64)
    t0 = time.time()
    for i, batch in enumerate(train_gen(), start=1):
        print "{}\t{}".format(str(time.time() - t0), batch[0][0,0,0,0])
        if i == 1000:
            break
        t0 = time.time()
