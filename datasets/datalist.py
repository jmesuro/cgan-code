
def get_dataset(dataname, datapath, **kwargs):

    if dataname == 'soja':
        import datasets.drone_soja as dataset
        data = dataset.soja(data_dir=datapath+'/soja', **kwargs)
    elif dataname == 'stl10':
        import datasets.stl10_data as dataset
        data = dataset.stl10(data_dir=datapath+'/stl10', **kwargs)
    elif dataname == 'imagenet':
        import datasets.imagenet as dataset
        data = dataset.imagenet()
    elif dataname == 'lsun':
        import datasets.lsun as dataset
        data = dataset.lsun(**kwargs)
    elif dataname == 'mnist':
        import datasets.mnist as dataset
        data = dataset.mnist(dir=datapath)
    elif dataname == 'celeba':
        import datasets.celeba as dataset
        data = dataset.celeba(dir=datapath, **kwargs)
    elif dataname == 'celeba64':
        import datasets.celeba as dataset
        data = dataset.celeba_64x64(dir=datapath)
    elif dataname == 'celeba96':
        import datasets.celeba as dataset
        data = dataset.celeba_96x96(dir=datapath)
    elif dataname == 'soja2':
        import datasets.soja2 as dataset
        data = dataset.soja2_64x64(dir=datapath)
    else:
        raise NameError(dataname)

    return data
