import os, sys, tarfile, urllib
import numpy as np
import gzip
import csv
from data import Dataset


class numpy_dataset(Dataset):

    def __init__(self, data_dir, nclasses = None,
                 train_file='train.npz',
                 test_file='test.npz',
                 unlab_file='unlabeled.npz',
                 fold_indices='fold_indices.csv.gz',
                 fold=0,
                 n_folds=1,
                 shuffle=True,
                 seed=1234,
                 min_value=0,
                 max_value=255,
                 **kwargs):

        rs = np.random.RandomState(seed)

        X_train = None
        y_train = None
        X_valid = None
        y_valid = None
        X_test = None
        y_test = None
        X_unlab = None

        y_max = -1

        if train_file is not None:
            file_path = os.path.join(data_dir,train_file)
            if os.path.isfile(file_path):
                with np.load(file_path) as data:
                    X_train = data['X']
                    y_train = data['y']
                if len(X_train.shape) == 3:
                    X_train = X_train.reshape(-1,1,X_train.shape[1],X_train.shape[2])
                if fold_indices is not None or n_folds > 1:
                    file_path = os.path.join(data_dir, fold_indices)
                    if os.path.isfile(file_path):
                        train_indices, valid_indices = self.load_fold_indices(file_path, fold)
                    else:
                        train_indices, valid_indices = self.make_fold_indices(X_train.shape[0], n_folds, fold, rs)

                    if shuffle:
                        rs.shuffle(train_indices)
                        rs.shuffle(valid_indices)

                    X_valid = X_train[valid_indices]
                    y_valid = y_train[valid_indices]
                    self.valid_size = X_valid.shape[0]
                    X_train = X_train[train_indices]
                    y_train = y_train[train_indices]
                    y_max = max(y_valid.max(),y_max)
                self.train_size = X_train.shape[0]
                y_max = max(y_train.max(),y_max)

        if test_file is not None:
            file_path = os.path.join(data_dir,test_file)
            if os.path.isfile(file_path):
                with np.load(file_path) as data:
                    X_test = data['X']
                    y_test = data['y']
                if len(X_test.shape) == 3:
                    X_test = X_test.reshape(-1,1,X_test.shape[1],X_test.shape[2])
                self.test_size = X_test.shape[0]
                test_indices = np.arange(self.test_size)
                if shuffle:
                    rs.shuffle(test_indices)
                    X_test = X_test[test_indices]
                    y_test = y_test[test_indices]
                y_max = max(y_test.max(),y_max)

        if unlab_file is not None:
            file_path = os.path.join(data_dir,unlab_file)
            if os.path.isfile(file_path):
                with np.load(file_path) as data:
                    X_unlab = data['X']
                if len(X_unlab.shape) == 3:
                    X_unlab = X_unlab.reshape(-1,1,X_unlab.shape[1],X_unlab.shape[2])
                self.unlab_size = X_unlab.shape[0]
                if shuffle:
                    rs.shuffle(X_unlab)

        if nclasses is None and y_max>-1:
            nclasses = y_max + 1

        super(numpy_dataset, self).__init__(X_train=X_train, y_train=y_train,
                                    X_valid=X_valid, y_valid=y_valid,
                                    X_test =X_test, y_test=y_test,
                                    X_unlab=X_unlab,x_min=min_value,x_max=max_value,nclasses=nclasses,**kwargs)

    def load_fold_indices(self, file_path, fold):
        print "Leyendo fold_indices desde " + file_path
        with gzip.open(file_path) as f:
            fold_indices = [list(map(int,rec)) for rec in csv.reader(f)]

        indices = [item for sublist in fold_indices for item in sublist]
        valid_indices = fold_indices[fold]
        train_indices = [item for item in indices if item not in valid_indices]

        return train_indices, valid_indices

    def make_fold_indices(self, size, n_folds, fold, rs):
        indices = range(size)
        rs.shuffle(indices)
        if fold == n_folds - 1:
            valid_indices = indices[fold * (size / n_folds):]
        if fold < n_folds - 1:
            valid_indices = indices[fold * (size / n_folds):(fold + 1) * (size / n_folds)]

        train_indices = [item for item in indices if item not in valid_indices]

        return train_indices, valid_indices