#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lasagne.init import Normal

from lib.vis import color_grid_vis, grayscale_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values


from datasets.numpy_dataset import numpy_dataset
from datasets.augmentation import Augmentation, zoom_and_crop
from networks.cnn import build_classifier

from lasagne.objectives import categorical_crossentropy
from lasagne.regularization import regularize_layer_params, l2

from sklearn.metrics import confusion_matrix

last_model_dir = 'last_model'
best_model_dir = 'best_model'
samples_dir = 'samples'
if not os.path.exists('logs/'):
    os.makedirs('logs/')
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(best_model_dir):
    os.makedirs(best_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)


parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='soybean_pods/npdata_2015Dec')
parser.add_argument("--datapath", type=str, default='/share/datasets')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=20000)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--model_width", type=int, default=64)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--wd", type=float, default=0.01)


args = parser.parse_args()
if os.path.isfile("%s/args.p"%last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p"%last_model_dir, "rb"))
    args.n_iter = n_iter
else:
    resume = False

print args

np_rng = RandomState(args.seed)


# DATASET
data_dir = os.path.join(args.datapath,args.dataname)
print data_dir
data = numpy_dataset(data_dir)
augm = Augmentation(rotation_range=5, # En grados
                    width_shift_range=0.05,
                    height_shift_range=0.05,
                    horizontal_flip=True,
                    shear_range=0.1745, # En radianes
                    zoom_range=(0.95,1.2), # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0, # 0-255
                    fill_mode='constant', #'nearest',
                    random_curves_strength=0.5,
                    seed=args.seed)
nchannels = data._nchannels
img_shape = data.X_train.shape[2:4]
class_labels = None # TODO
print  data.X_train.shape, img_shape
X_sample, y_sample = data.get_train_batch(0, args.grid_size**2)
color_grid_vis(X_sample.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir+'/real_sample.png')
X_sample = augm.random_transform(X_sample)
color_grid_vis(X_sample.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir+'/augmented_sample.png')

# SYMBOLIC INPUTS
X = T.tensor4()
y = T.matrix()

# MODELS
classifier = build_classifier(nclasses=data.nclasses,
                             img_size=img_shape,
                             nchannels=nchannels,
                             ndf=args.model_width,
                             global_pool=True,
                              strides=[2,2,2,2])

if resume:
    load_network_values(classifier,  os.path.join(last_model_dir, 'classifier.npz'))
    args_load = pickle.load(open("%s/args.p" %(last_model_dir), "rb"))

clsX = lasagne.layers.get_output(classifier, X)
test_clsX = lasagne.layers.get_output(classifier, X, deterministic=True)
test_clsX_out = T.argmax(test_clsX, axis=1)

# LOSS FUNCTIONS
weight_decay = regularize_layer_params(classifier, l2)
cls_loss = categorical_crossentropy(clsX,y).mean() + args.wd * weight_decay

# PARAMS
cls_params = lasagne.layers.get_all_params(classifier, trainable=True)

# UPDATES
cls_updates = adam(cls_loss, cls_params, learning_rate=args.lr, beta1=0.9, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_cls = theano.function([X, y], cls_loss, updates=cls_updates)
print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
predict = theano.function(
        inputs=[X],
        outputs=test_clsX,
        updates=None
)
print '%.2f seconds to compile theano functions' % (time() - t)




print "starting training"
if not resume:
    with open('accuracies.log', 'w') as f:
        f.write('# iter data_seen epoch cls_loss train_acc valid_acc')
        f.write('\n')
    with open('best_acc.log', 'w') as f:
        f.write('# iter data_seen epoch valid_acc test_acc')
        f.write('\n')

n_epochs = args.n_iter*args.batch_size/data.train_size

last_it = 0
it_best = 0
best_acc = 0.0
t = time()
last_save = t - 3601.0 # Fuerza que en la primer iteracion guarde
for it in xrange(args.start_iter,args.n_iter):
    epoch = it*args.batch_size/data.train_size

    X_batch,y_batch = data.get_train_batch(it,args.batch_size)
    X_batch = augm.random_transform(X_batch)
    X_batch = data.scale_data(X_batch)
    cls_loss_value = train_cls(X_batch,y_batch)

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):

        y_pred = np.asarray([])
        y_true = np.asarray([])
        for valit in range(data.valid_size/args.batch_size):
            X_valid, y_valid = data.get_valid_batch(valit,args.batch_size)
            X_valid = data.scale_data(X_valid)
            y_pred = np.append(y_pred, np.argmax(predict(X_valid), axis=1))
            y_true = np.append(y_true, np.argmax(y_valid, axis=1))
        valid_acc = (y_pred == y_true).mean()
        valid_cm = confusion_matrix(y_true,y_pred,class_labels)
        y_pred = np.argmax(predict(X_batch), axis=1)
        y_true = np.argmax(y_batch,axis=1)
        train_acc = (y_pred == y_true).mean()
        train_cm = confusion_matrix(y_true, y_pred, class_labels)

        if best_acc<valid_acc:
            best_acc = valid_acc
            it_best = it
            args.start_iter = it + 1
            save_network_values(classifier, os.path.join(best_model_dir, 'classifier.npz'))
            pickle.dump(args,
                        open("%s/args.p" % best_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

            y_pred = np.asarray([])
            y_true = np.asarray([])
            for testit in range(data.test_size / args.batch_size):
                X_test, y_test = data.get_test_batch(testit, args.batch_size)
                X_test = data.scale_data(X_test)
                y_pred = np.append(y_pred, np.argmax(predict(X_test), axis=1))
                y_true = np.append(y_true, np.argmax(y_test, axis=1))
            test_acc = (y_pred == y_true).mean()
            test_cm = confusion_matrix(y_true, y_pred, class_labels)

            with open('best_acc.log', 'a') as f:
                np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                                valid_acc, test_acc]], fmt='%1.3e')
            print "Best valid accuracy reached: %2.2f%%  Test Acc.: %2.2f%%"%(valid_acc*100,test_acc*100)
            print "valid CM\n", valid_cm
            print "test CM\n", test_cm

        with open('accuracies.log', 'a') as f:
            np.savetxt(f, [[it+1, (it+1)*args.batch_size, epoch,
                          cls_loss_value, train_acc, valid_acc]], fmt='%1.3e')

        t2 = time()-t
        t += t2
        horas = t2/(1+it-last_it)/3600.*10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones"%(it+1,args.n_iter,epoch,horas)
        last_it = it+1

    if time()-last_save>3600.: # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(classifier,  os.path.join(last_model_dir, 'classifier.npz'))
        pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) )  #almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter # Corrida completada
save_network_values(classifier,  os.path.join(last_model_dir, 'classifier.npz'))
pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) ) #Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan

print "Fin train\n"

print "Reload best\n"
load_network_values(classifier, os.path.join(best_model_dir, 'classifier.npz'))

def mk_classification_images(which):
    if which=='train':
        size = data.train_size
        samps = data.X_train
        file = 'class_train_pred'
    elif which=='valid':
        size = data.valid_size
        samps = data.X_valid
        file = 'class_valid_pred'
    else:
        size = data.test_size
        samps = data.X_test
        file = 'class_test_pred'

    y_pred = np.asarray([])
    y_true = np.asarray([])
    y_maxprob = np.asarray([])
    for it in range(size / args.batch_size + 1):
        if which=='train':
            X, y = data.get_train_batch(it, args.batch_size)
        elif which=='valid':
            X, y = data.get_valid_batch(it, args.batch_size)
        else:
            X, y = data.get_test_batch(it, args.batch_size)
        X = data.scale_data(X)
        pred = predict(X)
        y_pred = np.append(y_pred, np.argmax(pred, axis=1))
        y_true = np.append(y_true, np.argmax(y, axis=1))
        y_maxprob = np.append(y_maxprob, np.max(pred, axis=1))
    y_pred_arr = np.array(y_pred[:size])
    y_true_arr = np.array(y_true[:size])
    y_maxprob_arr = np.array(y_maxprob[:size])

    for predclass, trueclass in [(pr, tr) for pr in range(3) for tr in range(3)]:
        indexes = np.arange(size)[np.logical_and(y_pred_arr==predclass, y_true_arr==trueclass)]
        if len(indexes)!=0:
            y_probs = y_maxprob_arr[indexes]
            order = np.argsort(-y_probs)
            ordered_indexes = indexes[order]
            ordered_probs = y_probs[order]
            samples = samps[ordered_indexes].transpose(0, 2, 3, 1)
            shp = samples.shape
            samples = samples.reshape((shp[0], shp[1], shp[2]))
            grayscale_grid_vis(samples, (len(indexes)/10 + 1, 10), samples_dir+"/%s%d_true%d.png"%(file, predclass, trueclass))
            np.savetxt(samples_dir+"/%s%d_true%d_probs.txt"%(file, predclass, trueclass), ordered_probs, fmt="%.3f")

mk_classification_images('train')
mk_classification_images('valid')
mk_classification_images('test')
