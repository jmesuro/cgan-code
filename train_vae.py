#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lasagne.init import Normal
from lasagne.objectives import binary_crossentropy

from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values
from networks.vae import build_model

import datasets.datalist as datalist

last_model_dir = 'last_model'
best_model_dir = 'best_model'
samples_dir = 'samples'
if not os.path.exists('logs/'):
    os.makedirs('logs/')
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(best_model_dir):
    os.makedirs(best_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)
if not os.path.exists(samples_dir+'/reconstructions'):
    os.makedirs(samples_dir+'/reconstructions')


parser = argparse.ArgumentParser()
parser.add_argument("--dataname", type=str, default='lsun')
parser.add_argument("--datapath", type=str, default='/share/datasets')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=500000)
parser.add_argument("--monitor_size", type=int, default=196)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--z_dim", type=int, default=2)


if os.path.isfile("%s/args.p"%last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    # Se ignoran los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p"%last_model_dir, "rb"))
else:
    resume = False
    args = parser.parse_args()

print args

np_rng = RandomState(args.seed)


# DATASET
data = datalist.get_dataset(args.dataname, args.datapath)
nchannels = data._nchannels


# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()


# MODELS
L = 2
vae_network = build_model()

if resume:
    pass
#    load_network_values(vae_network, os.path.join(last_model_dir, 'vae_network.npz'))
#   args_load = pickle.load(open("%s/args.p" %(last_model_dir), "rb"))

vae_expr = lasagne.layers.get_output([vae_network['output_mu'], vae_network['output_logsigma'], vae_network['output']], X,
                                     batch_norm_use_averages = True, batch_norm_update_averages = True)
vae_mu = vae_expr[0]
vae_ls = vae_expr[1]
vae_out = vae_expr[2]

encode_expr = lasagne.layers.get_output(vae_network['output_mu'], X,
                                        batch_norm_use_averages=True,
                                        batch_norm_update_averages=False)

decode_expr = lasagne.layers.get_output(vae_network['output'], {vae_network['output_sample']:Z},
                                        batch_norm_use_averages=True, batch_norm_update_averages=False)

encode_decode = lasagne.layers.get_output(vae_network['output'], {vae_network['output_sample']:encode_expr},
                                        batch_norm_use_averages=True, batch_norm_update_averages=False)

# LOSS FUNCTIONS
#kl_div = 0.5 * T.sum(1 + 2*vae_ls - T.sqr(vae_mu) -T.exp(2*vae_ls))  # Del ejemplo de lasagne
#logpxz = binary_crossentropy(vae_out, T.tile(X, (L, 1, 1, 1)))
#vae_loss = (-1 * (logpxz + kl_div)).mean()
#vae_loss = (logpxz + kl_div).mean()
X_flat = (T.tile(X, (L, 1, 1, 1))).flatten(2)
gen_loss = lasagne.objectives.binary_crossentropy(vae_out, X_flat).sum() * (-1./L)
#gen_loss = lasagne.objectives.squared_error(vae_out, T.tile(X, (L, 1, 1, 1))).mean()
kl_div = -0.5*T.sum(T.sqr(vae_mu) + T.square(T.exp(vae_ls)) - 2*vae_ls -1)
vae_loss = -1 * (gen_loss + kl_div)
#vae_loss = gen_loss
#param = T.scalar()
#vae_loss = gen_loss + param * kl_div
# PARAMS
vae_params = lasagne.layers.get_all_params(vae_network['output'], trainable=True)

# UPDATES
vae_updates = adam(vae_loss, vae_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_vae = theano.function([X], vae_loss, updates=vae_updates, on_unused_input='ignore')
#train_vae = theano.function([X, param], vae_loss, updates=vae_updates, on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_vae = theano.function([X], encode_decode.reshape((-1, 1, 28, 28)), on_unused_input='ignore')
decode = theano.function([Z], decode_expr.reshape((-1, 1, 28, 28)), on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)


X_sample = data.get_unlab_batch(0,args.monitor_size)[0]

color_grid_vis(X_sample.transpose(0, 2, 3, 1), (14, 14), samples_dir+'/real_%s_sample.png'%(args.dataname))

Z_sample = floatX(np_rng.normal(size=(args.monitor_size, args.z_dim)))


print "starting training"
if not resume:
    with open('errors.log', 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss')
        f.write('\n')

n_epochs = args.n_iter*args.batch_size/data.unlab_size

def param_weight(it):
    if it<2000:
        return 0.0
    elif it>102000:
        return 1.0
    else:
        return (it-2000.0)/100000.0

last_it = 0
last_save = t = time()
for it in xrange(args.start_iter,args.n_iter):
    epoch = it*args.batch_size/data.unlab_size
    X_batch = data.get_unlab_batch(it,args.batch_size)[0]
    #X_batch = data.scale_data(data.image_crop(X_batch,args.img_size), new_min=0.0, new_max=1.0)
    X_batch = data.scale_data(X_batch, new_min=0.0, new_max=1.0)

    pval = param_weight(it)

    batch_loss = train_vae(X_batch)

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):

        samples = np.asarray(decode(Z_sample))
        color_grid_vis(data.inv_scale_data(samples, old_min=0.0, old_max=1.0).transpose(0, 2, 3, 1), (14, 14), samples_dir+'/%d.png'%it)
        reconstructions = np.asarray(test_vae(X_sample))
        color_grid_vis(data.inv_scale_data(reconstructions, old_min=0.0, old_max=1.0).transpose(0, 2, 3, 1), (14, 14), samples_dir+'/reconstructions/%d.png'%it)

        with open('errors.log', 'a') as f:
            f.write(" %d %d %d %4.4f %4.4f\n"%(it+1, (it+1)*args.batch_size, epoch, pval, batch_loss))

        t2 = time()-t
        t += t2
        horas = t2/(1+it-last_it)/3600.*10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones"%(it+1,args.n_iter,epoch,horas)
        last_it = it+1

    if time()-last_save>3600.: # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        #save_network_values(vae_network, os.path.join(last_model_dir, 'vae_network.npz'))
        #pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) )  #almacenar args aca tamb por si se corta la luz

#save_network_values(vae_network, os.path.join(last_model_dir, 'vae_network.npz'))
#pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) ) #Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan

print "Fin train vae\n"

