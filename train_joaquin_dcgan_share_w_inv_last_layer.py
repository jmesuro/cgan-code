#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lasagne.init import Normal
from networks.share_w import share_layers_dis_to_gen, share_layers_gen_to_dis
from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values
from lasagne.objectives import squared_error as se

from src import gan
import datasets.datalist as datalist
from networks.dcgan_inv_share_w import build_discriminator, build_generator

last_model_dir = 'last_model'
best_model_dir = 'best_model'
samples_dir = 'samples'
logs_dir = 'logs'
if not os.path.exists(logs_dir):
    os.makedirs(logs_dir)
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(best_model_dir):
    os.makedirs(best_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)

parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='celeba')
parser.add_argument("--datapath", type=str, default='/home/jmesuro/datasets')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=50000)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--shared_layers", type=int, default=0)
args = parser.parse_args()

if os.path.isfile("%s/args.p" % last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter  # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p" % last_model_dir, "rb"))
    args.n_iter = n_iter
else:
    resume = False

print args

np_rng = RandomState(args.seed)

# DATASET
data = datalist.get_dataset(args.dataname, args.datapath, img_size=args.img_size)
nchannels = data._nchannels

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()

# MODELS
gen_network, WsG = build_generator(gen_dim=args.gen_dim,
                                   img_size=(args.img_size, args.img_size),
                                   nchannels=nchannels,
                                   ngf=args.model_width,
                                   W_init=Normal(std=args.init_scale))

dis_network, inv_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                                        nchannels=nchannels,
                                                        ndf=args.model_width,
                                                        W_init=Normal(std=args.init_scale),
                                                        global_pool=True)
if resume:
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
    load_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))
    args_load = pickle.load(open("%s/args.p" % (last_model_dir), "rb"))

disX = lasagne.layers.get_output(dis_network, X)
genX = lasagne.layers.get_output(gen_network, Z)
disgenX = lasagne.layers.get_output(dis_network, genX, batch_norm_update_averages=False)
invX = lasagne.layers.get_output(inv_network, genX)
test_invX = lasagne.layers.get_output(gen_network, invX, deterministic=True)
test_genX = lasagne.layers.get_output(gen_network, Z, deterministic=True)

# LOSS FUNCTIONS
genX_loss, disX_loss, disgenX_loss = gan.compute_loss(disX, disgenX)
gen_loss = genX_loss
dis_loss = disX_loss + disgenX_loss
#inv_loss = se(invX, Z).mean()

# PARAMS
dis_params = lasagne.layers.get_all_params(dis_network, trainable=True)
gen_params = lasagne.layers.get_all_params(gen_network, trainable=True)
#inv_params = [lasagne.layers.get_all_params(inv_network, trainable=True)[-1]]  # solo actualizamos la capa densa

# UPDATES
dis_updates = adam(dis_loss, dis_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
gen_updates = adam(gen_loss, gen_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
#inv_updates = adam(inv_loss, inv_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_gen = theano.function([Z], gen_loss, updates=gen_updates, on_unused_input='ignore')
train_dis = theano.function([X, Z], dis_loss, updates=dis_updates, on_unused_input='ignore')
#train_inv = theano.function([Z], inv_loss, updates=inv_updates, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, on_unused_input='ignore')
#test_inv = theano.function([Z], test_invX, on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)

X_sample = data.get_unlab_batch(0, args.grid_size ** 2)
X_sample = data.image_crop(X_sample, args.img_size, random_state=np_rng)
color_grid_vis(X_sample.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
               samples_dir + '/real_%s_sample.png' % (args.dataname))

Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))

print "starting training"
if not resume:
    with open('errors.log', 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss inv_loss')
        f.write('\n')

n_epochs = args.n_iter * args.batch_size / data.unlab_size
last_it = 0
t = time()
last_save = t - 3601.0  # Fuerza que en la primer iteracion guarde
best_dis_loss_value = 1000.
for it in xrange(args.start_iter, args.n_iter):
    epoch = it * args.batch_size / data.unlab_size
    Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
    X_batch = data.get_unlab_batch(it, args.batch_size)
    X_batch = data.scale_data(data.image_crop(X_batch, args.img_size, random_state=np_rng))

    dis_loss_value = train_dis(X_batch, Z_batch)
    gen_loss_value = train_gen(Z_batch)

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):
        samples = np.asarray(test_gen(Z_sample))
        #samples_inv = np.asarray(test_inv(Z_sample))
        color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir + '/%d.png' % it)

        with open('errors.log', 'a') as f:
            np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                            dis_loss_value, gen_loss_value, 0.]], fmt='%1.3e')

        if (dis_loss_value < best_dis_loss_value) and (it % args.iter_save == 0):
            best_dis_loss_value = last_model_dir
            save_network_values(gen_network, os.path.join(best_model_dir, 'gen_best_network.npz'))
            save_network_values(dis_network, os.path.join(best_model_dir, 'dis_best_network.npz'))
            save_network_values(inv_network, os.path.join(best_model_dir, 'inv_best_network.npz'))

        t2 = time() - t
        t += t2
        horas = t2 / (1 + it - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones" % (it + 1, args.n_iter, epoch, horas)
        last_it = it + 1

    if time() - last_save > 3600.:
        last_save = time()
        args.start_iter = it + 1
        save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
        save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
        save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))

        pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter  # Corrida completada
save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))

pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))

print "Fin train gan\n"
