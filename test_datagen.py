from numpy.random import RandomState
from lib.vis import color_grid_vis
import os
from time import time

import datasets.datalist as datalist
from datasets.augmentation import Augmentation
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--dataname", type=str, default='stl10')
parser.add_argument("--datapath", type=str, default='/share/datasets')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--n_iter", type=int, default=1000)
parser.add_argument("--augment", action='store_true')
parser.add_argument("--cshift", type=float, default=20.0)
parser.add_argument("--seed", type=int, default=42)
args = parser.parse_args()

np_rng = RandomState(args.seed)

samples_dir = 'samples'
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)


# DATASET
data = datalist.get_dataset(args.dataname, args.datapath)
augm = Augmentation(rotation_range=15, # En grados
                    width_shift_range=0.15,
                    height_shift_range=0.15,
                    horizontal_flip=True,
                    shear_range=0.1745, # En radianes
                    zoom_range=(1.1,1.5), # >1 zoom out; <1 zoom in
                    channel_shift_range=args.cshift, # 0-255
                    fill_mode='nearest',
                    seed=args.seed)


X_sample = data.get_unlab_batch(index=0, batch_size=args.batch_size)
X_crop = data.image_crop(X_sample, args.img_size, random_state=np_rng)
color_grid_vis(X_crop.transpose(0, 2, 3, 1), (8, args.batch_size/8), samples_dir+'/real_%s_sample.png'%(args.dataname))

#X_sample = augm.random_transform(X_sample)
#X_crop = data.image_crop(X_sample, args.img_size)
#color_grid_vis(X_crop.transpose(0, 2, 3, 1), (8, args.batch_size/8), samples_dir+'/augmented_%s_sample.png'%(args.dataname))

last_it = 0
t = time()
for it in xrange(args.start_iter,args.n_iter):
    epoch = it*args.batch_size/data.unlab_size
    X_batch = data.get_unlab_batch(index=it, batch_size=args.batch_size)
    if args.augment:
        X_batch = augm.random_transform(X_sample)
    X_batch = data.image_crop(X_batch, args.img_size)

    if X_batch.shape[0]!=args.batch_size:
        print "WARNING"

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):

        color_grid_vis(X_batch.transpose(0, 2, 3, 1), (8, args.batch_size / 8), samples_dir + '/%d.png' % it)

        t2 = time()-t
        t += t2
        horas = t2/(1+it-last_it)/3600.*10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones"%(it+1,args.n_iter,epoch,horas)
        last_it = it+1

