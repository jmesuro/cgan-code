class Log(object):
    ERROR, IMPORTANT, INFO, DEBUG = range(4)

    def __init__(self, verbosity=1):
        self._verbosity = verbosity

    def set_verbosity(self, verbosity=1):
        self._verbosity = verbosity

    def log(self, level, msg):
        if (self._verbosity >= level):
            if (level == Log.ERROR):
                out_msg = '\033[91m[ERROR]:\033[0m ' + msg
            elif (level == Log.IMPORTANT):
                out_msg = '\033[93m[IMPORTANT]:\033[0m ' + msg
            elif (level == Log.DEBUG):
                out_msg = '\033[92m[DEBUG]:\033[0m ' + msg
            elif (level == Log.INFO):
                out_msg = '\033[94m[INFO]:\033[0m ' + msg

            print(out_msg)
            return out_msg



