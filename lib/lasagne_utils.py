import numpy as np
import lasagne


def save_network_values(network, filepath):
    np.savez(filepath, *lasagne.layers.get_all_param_values(network))


def load_network_values(network, filepath):
    with np.load(filepath) as f:
        param_values = [f['arr_%d' % i] for i in range(len(f.files))]
    lasagne.layers.set_all_param_values(network, param_values)


def save_arr_values(arr, filepath):
    np.savez(filepath, arr)

def load_arr_values(filepath):
    with np.load(filepath) as f:
        arr = [f['arr_%d' % i] for i in range(len(f.files))]
    return arr
