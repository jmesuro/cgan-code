import numpy as np
# from sklearn import utils as skutils
from scipy import misc
from PIL import Image

from rng import np_rng, py_rng

def center_crop(x, ph, pw=None):
    if pw is None:
        pw = ph
    h, w = x.shape[:2]
    j = int(round((h - ph)/2.))
    i = int(round((w - pw)/2.))
    return x[j:j+ph, i:i+pw]

def patch(x, ph, pw=None):
    if pw is None:
        pw = ph
    h, w = x.shape[:2]
    j = py_rng.randint(0, h-ph)
    i = py_rng.randint(0, w-pw)
    x = x[j:j+ph, i:i+pw]
    return x

def list_shuffle(*data):
    idxs = np_rng.permutation(np.arange(len(data[0])))
    if len(data) == 1:
        return [data[0][idx] for idx in idxs]
    else:
        return [[d[idx] for idx in idxs] for d in data]

# def shuffle(*arrays, **options):
#     if isinstance(arrays[0][0], basestring):
#         return list_shuffle(*arrays)
#     else:
#         return skutils.shuffle(*arrays, random_state=np_rng)

def OneHot(X, n=None, negative_class=0.):
    X = np.asarray(X).flatten()
    if n is None:
        n = np.max(X) + 1
    Xoh = np.ones((len(X), n)) * negative_class
    Xoh[np.arange(len(X)), X] = 1.
    return Xoh

def iter_data(*data, **kwargs):
    size = kwargs.get('size', 128)
    try:
        n = len(data[0])
    except:
        n = data[0].shape[0]
    batches = n / size
    if n % size != 0:
        batches += 1

    for b in range(batches):
        start = b * size
        end = (b + 1) * size
        if end > n:
            end = n
        if len(data) == 1:
            yield data[0][start:end]
        else:
            yield tuple([d[start:end] for d in data])

#Obtiene informacion de la imagen y en base a eso ejecuta un modo u otro (RGB o Escala de Grises)
def img2array(path):
    try:
        imagen = Image.open(path)
        if imagen.mode == 'L':
            return img2arrayGS(path)
        if imagen.mode == 'RGB':
            return img2arrayRGB(path)
        pass
    except IOError:
        print ("IOError al cargar la imagen: " + path)
        pass
    
#Carga las imagenes en escala de grises manteniendo la estructura pero poniendo un 1 en el parametro de canales
def img2arrayGS(path):
    X = misc.imread(path)
    if len(X.shape)<1:
        print ("Error al cargar la imagen GS: " + path)
    nX = X.reshape((X.shape[0],X.shape[1],1))
    return nX

#Detecta la cantidad de canales de una imagen
def detec_nchannels(path):
    imagen = Image.open(path)
    if imagen.mode == 'L':
        return 1
    else:    
        if imagen.mode == 'RGB':
            return 3
        else:
            return 0

def img2arrayRGB(path):
    X = misc.imread(path)
    if len(X.shape)<2:
        print ("Error al cargar la imagen RGB: " + path)
        #import os
        #os.remove(path)
    return X 

def sample_labels(dir_labs,dir_labels,lab,n,m,save_name):
#Esta funcion crea una imagen de nxm imagenes de la carpeta de labels para tener una muestra
#dir_labs: path completo de donde va a estar la carpeta con los labels
#dir_labels: nombre de la carpeta que adentro contiene los labels
#lab: nombre de un lab en particular
    import os
    from lib.vis import color_grid_vis
    for dirpath, dirnames, filenames in os.walk(os.path.join(dir_labs,dir_labels,lab)):
        cnt=0    
        for name in filenames:
            if cnt==0:
                L0=[img2array(os.path.join(dir_labs,dir_labels,lab,name))]
                cnt=1
            else:
                if cnt<100:       
                    L0 = np.concatenate((L0,[img2array(os.path.join(dir_labs,dir_labels,lab,name))])) 
                    cnt=cnt+1
    color_grid_vis(L0, (n, m), '%s/%s/%s'%(dir_labels,lab,save_name))

