def init_folders(args, os, datetime, time, experiment_path=""):

    dir_path = os.path.dirname(os.path.realpath(__file__))
    out_dir = dir_path[0:-4] + '/out'

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    time = datetime.datetime.fromtimestamp(time()).strftime('%Y-%m-%d_%H:%M:%S')

    resume = True

    if experiment_path == "":
        experiment_path = out_dir + '/' + args.exp_name + \
                          '_data_name' + str(args.dataname) + \
                          '_bs' + str(args.batch_size) + \
                          '_gen_dim' + str(args.gen_dim) + \
                          '_n_iter' + str(args.n_iter) +  \
                          '_share_w' + str(args.shared_layers) + \
                          '_date' + time
        resume = False

    if not os.path.exists(out_dir):
        os.mkdir(experiment_path)

    last_model_dir = experiment_path + '/last_model'
    best_models_dir = experiment_path + '/best_models'
    samples_dir = experiment_path + '/samples'
    logs = experiment_path + '/logs'
    errors_log = logs + '/errors.log'

    if not os.path.exists(logs):
        os.makedirs(logs)
    if not os.path.exists(last_model_dir):
        os.makedirs(last_model_dir)
    if not os.path.exists(best_models_dir):
        os.makedirs(best_models_dir)
    if not os.path.exists(samples_dir):
        os.makedirs(samples_dir)

    return last_model_dir, best_models_dir, samples_dir, logs, errors_log, resume