#!/bin/bash
for file in $( ls monitor/*/*.txt.gz ); do
	echo "${file}"
	zcat "${file}" > temp.txt

	gnuplot plot_files.gp

	convert -density 300 temp.eps temp.jpg
	mv temp.jpg "${file}.jpg"
	mv temp.eps "${file}.eps"

done
