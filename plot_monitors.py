#!/usr/bin/env python2.7

import argparse
import glob
import gzip
import numpy as np
import os
import matplotlib

#from src.monitor import load_log_from_txt
from pandas import read_csv

parser = argparse.ArgumentParser()

parser.add_argument("--data", type = str, default = 'monitor')

args = parser.parse_args()

out_std_disX_files = sorted(glob.glob('monitor/disX/out_std*l*_Conv*txt.gz'))
out_std_disgenX_files = sorted(glob.glob('monitor/disgenX/out_std*l*_Conv*txt.gz'))
out_mean_disX_files = sorted(glob.glob('monitor/disX/out_mean*l*_Conv*txt.gz'))
out_mean_disgenX_files = sorted(glob.glob('monitor/disgenX/out_mean*l*_Conv*txt.gz'))
directory = 'monitor/ratio'
if not os.path.exists(directory):
    os.makedirs(directory)

for l in range(len(out_std_disgenX_files)):

    disXfile = out_std_disX_files[l]
    with gzip.open(disXfile, 'r') as f_handle:
        out_std_disX = read_csv(f_handle, delimiter=" ", header=None).as_matrix()

    disgenXfile = out_std_disgenX_files[l]
    with gzip.open(disgenXfile, 'r') as f_handle:
        out_std_disgenX = read_csv(f_handle, delimiter=" ", header=None).as_matrix()

    disXfile = out_mean_disX_files[l]
    with gzip.open(disXfile, 'r') as f_handle:
        out_mean_disX = read_csv(f_handle, delimiter=" ", header=None).as_matrix()

    disgenXfile = out_mean_disgenX_files[l]
    with gzip.open(disgenXfile, 'r') as f_handle:
        out_mean_disgenX = read_csv(f_handle, delimiter=" ", header=None).as_matrix()

    ratio = np.empty_like(out_std_disX)
    ratio[:, 0] = out_std_disX[:, 0]
    ratio[:, 3:] = out_std_disgenX[:, 3:] / out_std_disX[:, 3:]
    ratio[:, 1] = np.mean(ratio[:, 3:], axis=1)
    ratio[:, 2] = np.std(ratio[:, 3:], axis=1)

    ratiofile = os.path.join(directory,'out_ratio_l%d_Conv.txt.gz'%(l+1))
    with gzip.open(ratiofile, 'w') as f_handle:
        np.savetxt(f_handle, ratio, fmt='%1.3e')

    ndiff = np.empty_like(out_mean_disX)
    ndiff[:, 0] = out_mean_disX[:, 0]
    ndiff[:, 3:] = (out_mean_disX[:, 3:] - out_mean_disgenX[:, 3:]) / out_std_disX[:, 3:]
    ndiff[:, 1] = np.mean(ndiff[:, 3:], axis=1)
    ndiff[:, 2] = np.std(ndiff[:, 3:], axis=1)

    ndifffile = os.path.join(directory, 'out_ndiff_l%d_Conv.txt.gz' % (l + 1))
    with gzip.open(ndifffile, 'w') as f_handle:
        np.savetxt(f_handle, ndiff, fmt='%1.3e')
