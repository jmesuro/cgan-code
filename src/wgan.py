import theano.tensor as T
from src.theanofuns import clamp


def compute_critic_loss(critic_output, label=True):
    if label:
        return -critic_output.mean()
    else:
        return critic_output.mean()

def compute_loss(disX, disgenX):

    disX_loss = compute_critic_loss(disX,label=True)
    disgenX_loss = compute_critic_loss(disgenX, label=False)
    genX_loss = compute_critic_loss(disgenX, label=True)

    return genX_loss, disX_loss, disgenX_loss


def compute_grad_penalty(X, disX_loss):

    grad = T.grad(disX_loss, X)
    norm_grad = T.sqrt(T.sum(T.square(grad),axis=[1,2,3]))
    grad_penalty = T.mean(T.square(norm_grad - 1))

    return grad_penalty

def get_clamp_updates(params, lower, upper):

    clamp_updates = [(param, clamp(param, lower, upper)) for param in params]

    return clamp_updates