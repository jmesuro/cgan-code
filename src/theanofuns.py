import theano.tensor as T


def relu(x):
    return T.switch(T.gt(x,0.0),x,0.0)
    #~ return T.nnet.relu(x)


def lrelu(x):
    return T.switch(T.gt(x,0.0),x,0.2*x)
    #~ return T.nnet.relu(x,0.2)


def clamp(x, low, up):
    xlow = T.switch(T.gt(x,low),x,low)
    return T.switch(T.gt(xlow,up), up, xlow)
