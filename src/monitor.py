import os
import os.path
import numpy as np
import theano
import theano.tensor as T
import lasagne
import gzip
from sklearn.externals import joblib

def compute_w_norm_return_bias(params,name_modifier='',name_from_list=None):
    if name_from_list is None:
        name_from_list = params
    norms = list()
    names = list()
    for array,name_from in zip(params,name_from_list):
        name = name_from.name
        if name is not None:
            ndim = array.ndim
            if ndim == 1:  # For Biases that are in 1d (e.g. b of DenseLayer)
                sum_over = ()
            elif ndim == 2:  # DenseLayer
                sum_over = (0,)
            elif ndim in [3, 4, 5]:  # Conv{1,2,3}DLayer
                sum_over = tuple(range(1, ndim))
            else:
                continue

            names.append('norm.' + name_modifier + name)
            if len(sum_over) == 0:
                norms.append(array.flatten())  # if bias shape retain full vector
            else:
                norms.append(T.sqrt(T.sum(array ** 2, axis=sum_over, keepdims=False)))


    return norms, names


def cos_and_norm_ratio(a_list, b_list, name_modifier='', name_from_list=None, filters=None):
    filters = format_filters(filters)
    if name_from_list is None:
        name_from_list = b_list
    cosines = list()
    cos_names = list()
    ratios = list()
    ratio_names = list()
    for a, b, name_from in zip(a_list, b_list, name_from_list):
        name = name_from.name
        if name is not None:
            if any(f in name for f in filters) or len(filters) == 0:
                cos_names.append('cos.' + name_modifier + name_from.name)
                ratio_names.append('ratio.' + name_modifier + name_from.name)
                ndim = a.ndim
                if ndim == 1:  # For Biases that are in 1d (e.g. b of DenseLayer)
                    sum_over = (0,)
                elif ndim == 2:  # DenseLayer
                    sum_over = (0,)
                elif ndim in [3, 4, 5]:  # Conv{1,2,3}DLayer
                    sum_over = tuple(range(1, ndim))
                else:
                    raise ValueError(
                        "Unsupported tensor dimensionality {}. "
                    )

                norm_a = T.sqrt(T.sum(a ** 2, axis=sum_over, keepdims=False))
                norm_b = T.sqrt(T.sum(b ** 2, axis=sum_over, keepdims=False))
                ab_dot = T.sum(a * b, axis=sum_over, keepdims=False)
                cosines.append(T.flatten(ab_dot / (norm_a * norm_b)))
                ratios.append(norm_a / norm_b)

    return cosines+ratios, cos_names+ratio_names

def save_log_to_txt(log, destdir, names, iter, epoch=None):
        it_col = [iter,]
        if epoch is not None:
            it_col.append(epoch)
        with open(destdir+'/iteration.txt','a') as f_handle:
            np.savetxt(f_handle,[it_col],fmt='%d')
        for array, name in zip(log,names):

            if array.ndim == 0:
                row = [[iter,array.item()]]
            elif array.ndim == 1:
                row = [np.concatenate([[iter,np.mean(array),np.std(array)],array])]
            else:
                raise ValueError(
                    "Unsupported tensor dimensionality in monitor {}. "
                )

            with gzip.open(destdir+'/'+name+'.txt.gz', 'a') as f_handle:
                np.savetxt(f_handle, row,fmt='%1.3e')

def format_filters(filters):

    if filters is None:
        filters = list()
    elif isinstance(filters, str):
        filters=[filters]
    elif isinstance(filters, list):
        filters = filters
    else:
        raise ValueError("Unsupported filters format")

    return filters


def load_log_from_txt(srcdir, layers = None):

    from pandas import read_csv

    dirs = ["genX", "disX", "disgenX","gan_training"]
    iters = []
    all_files = []
    data = dict()

    for d in dirs:
        data[d] = dict()

        print "loading", srcdir+'/'+d
        with open(srcdir+'/'+d+'/iteration.txt','r') as f_handle:
            data[d]["iterartions"] = np.loadtxt(f_handle)

        for x, y, files in os.walk(srcdir+'/'+d):
            for f in files:
                filename = x + "/".join(y) + "/" + f
                print "loading", filename
                if "iteration.txt" not in filename:
                    layer = int(filename.split("_l")[1][0])

                    if layers is not None:
                        if layer not in layers:
                            continue

                    if layer not in data[d]:
                        data[d][layer] = dict()

                    with gzip.open(filename,'r') as f_handle:
                        data[d][layer][f] = read_csv(f_handle, delimiter=" ", header=None).as_matrix()

    return data

class NetworkMonitor:

    def __init__(self, name, network, extra_inputs=list(), filters=['Dense','Conv','Deconv','BN'], act_mean=True, act_std=True, param_norm=True, epsilon=1e-6, **kwargs):

        self.epsilon = epsilon
        self.destdir = 'monitor/'+name
        if not os.path.exists(self.destdir):
            os.makedirs(self.destdir)

        self.filters = format_filters(filters)

        layers = lasagne.layers.get_all_layers(network)

        network_input_var = layers[0].input_var # supongo que network tiene un unico input

        layers = self._refine_layers_list(layers, self.filters)

        formulas = lasagne.layers.get_output(layers, **kwargs)

        log = list()
        self.names = list()
        if act_mean:
            means, names = self._compute_means(formulas,layers)
            log.extend(means)
            self.names.extend(names)
        if act_std:
            stds, names = self._compute_stds(formulas,layers)
            log.extend(stds)
            self.names.extend(names)
        if param_norm:
            params = lasagne.layers.get_all_params(network)
            norms, names = compute_w_norm_return_bias(params)
            log.extend(norms)
            self.names.extend(names)

        inputs = [network_input_var] + extra_inputs

        self._monitor = theano.function(inputs,log, on_unused_input='ignore')

    def _refine_layers_list(self,layers,filters):
        if len(filters)==0:
            return layers
        layers = filter(lambda l: l.name is not None, layers)
        return filter(lambda l: any(f in l.name for f in filters), layers)


    def _compute_means(self, formulas, layers):
        means = list()
        names = list()
        for x, l in zip(formulas,layers):
            if l.name is not None:
                names.append('out_mean.' + l.name)
                axes = (0,) + tuple(range(2, len(l.output_shape))) # contract over all but the second axis
                means.append(x.mean(axes))

        return means, names

    def _compute_stds(self, formulas, layers):
        means = list()
        names = list()
        for x, l in zip(formulas,layers):
            if l.name is not None:
                names.append('out_std.' + l.name)
                axes = (0,) + tuple(range(2, len(l.output_shape))) # contract over all but the second axis
                means.append(T.sqrt(x.var(axes) + self.epsilon))
        return means, names


    def save_log(self,network_input, iter, epoch=None):
        log = self._monitor(*network_input)
        save_log_to_txt(log,self.destdir,self.names,iter,epoch)


class GAN_GradientsMonitor:

    def __init__(self, genX, disgenX_loss, disX_loss, genX_loss, dis_params, gen_params, inputs_vars,
                 name='gan_gradients', filters=['.b','.W'], grad_norm=True, param_grad_cos=True, grad_grad_cos=True):

        self.destdir = 'monitor/'+name
        if not os.path.exists(self.destdir):
            os.makedirs(self.destdir)

        disgenXgrads = T.grad(disgenX_loss, dis_params)
        disXgrads = T.grad(disX_loss,dis_params)
        genXgrads = T.grad(genX_loss, gen_params)

        dis_log = list()
        gen_log = list()
        self.dis_names = list()
        self.gen_names = list()

        if grad_norm:
            norm_disgenX, names_disgenX = compute_w_norm_return_bias(disgenXgrads, 'grad_genX.',
                                                                     dis_params)
            norm_disX, names_disX = compute_w_norm_return_bias(disXgrads, 'grad_X.', dis_params)
            norm_genX, names_genX = compute_w_norm_return_bias(genXgrads, 'grad.', gen_params)

            dis_log.extend(norm_disgenX+norm_disX)
            gen_log.extend(norm_genX)
            self.dis_names.extend(names_disgenX+names_disX)
            self.gen_names.extend(names_genX)

        if param_grad_cos:
            cosines_disgenX, names_disgenX = cos_and_norm_ratio(disgenXgrads, dis_params, 'param_grad_genX.',filters=filters)
            cosines_disX, names_disX = cos_and_norm_ratio(disXgrads, dis_params, 'param_grad_X.', filters=filters)
            cosines_genX, names_genX = cos_and_norm_ratio(genXgrads, gen_params, 'param_grad.', filters=filters)
            dis_log.extend(cosines_disgenX+cosines_disX)
            gen_log.extend(cosines_genX)
            self.dis_names.extend(names_disgenX+names_disX)
            self.gen_names.extend(names_genX)

        if grad_grad_cos:
            cosines, names = cos_and_norm_ratio(disgenXgrads, disXgrads,
                                                       'grad_grad.', dis_params, filters=filters)
            dis_log.extend(cosines)
            self.dis_names.extend(names)

        self._gen_monitor = theano.function(inputs_vars,
                                            [genX, ]+gen_log,
                                            no_default_updates=True, on_unused_input='ignore')
        self._dis_monitor = theano.function(inputs_vars+[genX, ],
                                            dis_log,
                                            no_default_updates=True, on_unused_input='ignore')

    def save_log(self, input_values, iter, epoch=None):
        mon_out = self._gen_monitor(*input_values)
        genX = mon_out[0]
        gen_log = mon_out[1:]
        input_values.append(genX)
        dis_log = self._dis_monitor(*input_values)
        save_log_to_txt(gen_log+dis_log, self.destdir, self.gen_names+self.dis_names, iter, epoch)

class UpdatesMonitor:
    def __init__(self, updates, inputs_vars,
                 name='updates', filters=['.b','.W'], update_norm=True, update_param_cos=True):

        self.destdir = 'monitor/'+name
        if not os.path.exists(self.destdir):
            os.makedirs(self.destdir)

        params = updates.keys()
        deltas = list()
        for param, formula in updates.iteritems():
            delta = formula - param # Le resto el param porque solo quiero la actualizacion
            deltas.append(delta)

        log = list()
        self.names = list()

        if update_norm:
            norms, names = compute_w_norm_return_bias(deltas, 'update.', params)

            log.extend(norms)
            self.names.extend(names)

        if update_param_cos:
            cosines, cos_names = cos_and_norm_ratio(deltas, params, 'update_param.', filters=filters)
            log.extend(cosines)
            self.names.extend(cos_names)
            self.names.extend(cos_names)

        self._monitor = theano.function(inputs_vars, log, no_default_updates=True, on_unused_input='ignore')

    def save_log(self, input_values, iter, epoch=None):
        log = self._monitor(*input_values)
        save_log_to_txt(log, self.destdir, self.names, iter, epoch)


class WeightsMonitor:

    def __init__(self, name, network, filters=list(), meanwcosine=True, param_norm=False, epsilon=1e-8, **kwargs):

        self.epsilon = epsilon
        self.destdir = 'monitor/'+name
        if not os.path.exists(self.destdir):
            os.makedirs(self.destdir)

        self.filters = format_filters(filters)
        params = lasagne.layers.get_all_params(network)
        params = self._refine_param_list(params,filters)

        log = list()
        self.names = list()
        if param_norm:
            norms, names = compute_w_norm_return_bias(params)
            log.extend(norms)
            self.names.extend(names)
        if meanwcosine:
            meancos, names = self._compute_mean_w_cosines(params)
            log.extend(meancos)
            self.names.extend(names)

        self._monitor = theano.function([],log)

    def _compute_mean_w_cosines(self,params):
        meancos = list()
        names = list()
        for w in params:
            name = w.name
            if name is not None:
                ndim = w.ndim
                if ndim == 2:  # DenseLayer
                    sum_over = (0,)
                elif ndim in [3, 4, 5]:  # Conv{1,2,3}DLayer
                    sum_over = tuple(range(1, ndim))
                else:
                    raise ValueError(
                        "Unsupported tensor dimensionality {}. "
                    )

                w = w / T.sqrt(T.sum(w**2, axis=sum_over, keepdims=True)+self.epsilon)
                wcosw = T.tensordot(w,w,axes=[sum_over,sum_over])
                mcos = T.mean(abs(wcosw-T.identity_like(wcosw)))
                meancos.append(mcos)
                names.append('meancos.' + name)

        return meancos, names

    def _refine_param_list(self,params,filters):
        params = filter(lambda p: p.ndim > 1, params)
        if len(filters)==0:
            return params
        params = filter(lambda p: p.name is not None, params)
        params = filter(lambda p: any(f in p.name for f in filters), params)
        return params

    def save_log(self, iter, epoch=None):
        log = self._monitor()
        save_log_to_txt(log,self.destdir,self.names,iter,epoch)

class ImageGeneratorMonitor:
    def __init__(self, test_gen,  data, nchannels, img_size, name='genimages'):
        self.test_gen = test_gen
        self.data = data
        self.nchannels = nchannels
        self.img_size = img_size
        self.destdir = 'monitor/' + name

        if not os.path.exists(self.destdir):
            os.makedirs(self.destdir)

        with open(self.destdir + '/eval.sh', 'w') as f:
            f.write('#!/bin/bash\n\n')

    def save_log(self, Z_sample, alpha, iter, batch_size=100):
        gen_size = Z_sample.shape[0]
        imgs = np.zeros(shape=(gen_size, self.nchannels, self.img_size, self.img_size))
        for i in range(gen_size / batch_size):
            samples = np.asarray(self.test_gen(Z_sample[i * batch_size:(i + 1) * batch_size],alpha))
            imgs[i * batch_size:(i + 1) * batch_size] = self.data.inv_scale_data(samples)

        imgarr = np.array(imgs, dtype='uint8')

        save_file = self.destdir + '/images_it_%d.jl' % iter
        joblib.dump(imgarr, save_file)
        out_file = self.destdir + '/score_it_%d.txt' % iter

        with open(self.destdir + '/eval.sh', 'a') as f:
            f.write('qsub ../../tf_modules/calc_score.sh --imgfile %s --outfile %s\n' % (save_file, out_file))
