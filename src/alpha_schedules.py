from math import exp


def exp_alpha(tau=15000.0):
    return lambda t: exp(-10.*t/tau)


def const_alpha(const=1.0):
    return lambda t: const


def pulses_alpha(width=5000,max=1.0,min=0.0,shift=0):
    return lambda t: max if (t+shift)/width % 2 == 0 else min


def onoff_alpha(tau=15000.0,max=1.0,min=0.0,):
    return lambda t: max if t<tau else min

def select_schedule(name, parameter):
    if name == 'const':
        if parameter is None:
            alpha_schedule = const_alpha()
        else:
            alpha_schedule = const_alpha(parameter)
    elif name == 'exp':
        if parameter is None:
            alpha_schedule = exp_alpha()
        else:
            alpha_schedule = exp_alpha(parameter)
    elif name == 'pulses':
        if parameter is None:
            alpha_schedule = pulses_alpha()
        else:
            alpha_schedule = pulses_alpha(int(parameter))
    elif name == 'onoff':
        if parameter is None:
            alpha_schedule = onoff_alpha()
        else:
            alpha_schedule = onoff_alpha(int(parameter))
    else:
        raise RuntimeError(name + " not implemented")
    
    return alpha_schedule