from time import time

import theano
import theano.tensor as T

from lib import updates
from lib import inits
from lib.theano_utils import sharedX

bce = T.nnet.binary_crossentropy
cce = T.nnet.categorical_crossentropy

class GAN_trainer(object):

    def __init__(self,model, margin = 20.0, pulling_away=0.1,
                 dis_updater = updates.Adam(lr=sharedX(0.001), b1=0.5, regularizer=updates.Regularizer(l2=1e-5)),
                 gen_updater = updates.Adam(lr=sharedX(0.001), b1=0.5, regularizer=updates.Regularizer(l2=1e-5))):

        self.margin = margin
        X = model.X
        Z = model.Z
        m = T.scalar()

        genX = model.genX

        recX = model.recX
        recgenX = model.recgenX

        recX_loss = T.sqrt(T.sum(T.sqr(recX-X),axis=(1,2,3))).mean()
        recgenX_loss = T.sqrt(T.sum(T.sqr(recgenX-genX),axis=(1,2,3))).mean()
        
        dis_loss = recX_loss + T.maximum(0,m-recgenX_loss)
        gen_loss = recgenX_loss
        if pulling_away > 0.0:
            gen_loss = gen_loss + self.pulling_away_loss(model.codegenX)

        trainable_discrim_params = model.trainable_discrim_params
        trainable_gen_params = model.trainable_gen_params

        dis_updates = dis_updater(trainable_discrim_params, dis_loss)
        gen_updates = gen_updater(trainable_gen_params, gen_loss)

        print 'COMPILING'
        t = time()
        self._train_gen = theano.function([Z], gen_loss, updates=gen_updates)
        self._train_dis = theano.function([X, Z, m], dis_loss, updates=dis_updates)
        self._gen = theano.function([Z], genX)
        print '%.2f seconds to compile theano functions'%(time()-t)
        
    def pulling_away_loss(self,batch):
        s = batch / T.sqrt(T.sum(T.sqr(batch),axis=(1,2,3),keepdims=True))
        cos = T.tensordot(s,s,axes=[[1,2,3],[1,2,3]]) # cosine similarity
        cos = T.sqr(cos - T.identity_like(cos)) #   j != i Sera necesaria esta linea?
        return cos.mean()

    def train_on_batch(self,X_batch,Z_batch):
        cost_gen = self._train_gen(X_batch, Z_batch)
        cost_dis_real, cost_dis_gen = self._train_dis(X_batch, Z_batch)
        return cost_gen,cost_dis_real,cost_dis_gen

    def train_discriminator_on_batch(self,X_batch,Z_batch):
        return self._train_dis(X_batch, Z_batch, self.margin)

    def train_generator_on_batch(self,Z_batch):
        return self._train_gen(Z_batch)

