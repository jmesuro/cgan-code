import theano.tensor as T

from model import Model

import lasagne
from lasagne.layers import DenseLayer, BatchNormLayer, Conv2DLayer, Deconv2DLayer, InputLayer, NonlinearityLayer
from lasagne.layers import ReshapeLayer
from lasagne.init import Constant, GlorotNormal
from lasagne.nonlinearities import tanh


    
def relu(x):
    return T.switch(T.gt(x,0.0),x,0.0)


class GAN_model(Model):

    def __init__(self,
                 img_size = (64,64), # tienen que ser multiplos de 32
                 gen_dim = 100, ngf = 128, nchannels = 3):

        self.gen_dim = gen_dim
        self.ngf = ngf
        
        self.X = T.tensor4()
        xLayer = InputLayer((None, nchannels, img_size[0], img_size[1]))
        encoder = self.encoder_function(xLayer)
        decoder = self.decoder_function(encoder)
        self.codeX, self.recX = lasagne.layers.get_output([encoder,decoder],self.X)
        self.trainable_discrim_params = lasagne.layers.get_all_params(decoder,trainable=True)
        self.discrim_params = lasagne.layers.get_all_params(decoder)
        
        self.Z = T.matrix()
        zLayer = InputLayer((None, gen_dim), self.Z)
        generador = self.gen_function(zLayer, img_size, nchannels, ngf)
        self.genX = lasagne.layers.get_output(generador,self.Z)
        self.codegenX, self.recgenX = lasagne.layers.get_output([encoder,decoder],self.genX)
        self.trainable_gen_params = lasagne.layers.get_all_params(generador,trainable=True)
        self.gen_params = lasagne.layers.get_all_params(generador)
        
        self.params = self.gen_params + self.discrim_params
        
    
    def gen_function(self, Z, img_size, nchannels, ngf, 
                     init_size = (4,4), W_init = GlorotNormal()):
        print 'gen'
        if img_size[0]%32 is not 0 or img_size[1]%32 is not 0:
            raise 1
        
        vis_W_size = (img_size[0]/16+1, img_size[1]/16+1)
        vis_stride = (img_size[0]/32,img_size[1]/32)
        h = init_size[0]
        w = init_size[1]
        ishape= lasagne.layers.get_output_shape(Z)
        print ishape
        x1 = DenseLayer(Z, num_units=ngf*8*h*w, W=W_init, b=None)
        x2 = BatchNormLayer(x1, alpha=1.0, epsilon=1e-4)
        x3 = NonlinearityLayer(x2,nonlinearity=relu)
        h = ReshapeLayer(x3,([0], ngf*8, h, w))
        ishape= lasagne.layers.get_output_shape(h)
        print ishape
        
        outsize = [(8,8),(16,16),(32,32),(64,64)]
        i=0
        for a in (4,2,1):
            x1 = Deconv2DLayer(h, num_filters = ngf*a, 
                        filter_size = (5,5), stride=(2, 2), 
                        crop='same', W=W_init, b=None, nonlinearity=None,
                        output_size = outsize[i])
            i+=1
            ishape= lasagne.layers.get_output_shape(x1)
            print ishape
        
            x2 = BatchNormLayer(x1, alpha=1.0, epsilon=1e-4, gamma=None)
            h = NonlinearityLayer(x2,nonlinearity=relu)
            
        x1 = Deconv2DLayer(h, num_filters = nchannels, 
                    filter_size = vis_W_size, stride=vis_stride, 
                    crop='same', W=W_init, b=None, nonlinearity=None,
                    output_size = outsize[i])
        x = NonlinearityLayer(x1,nonlinearity=tanh)
        ishape= lasagne.layers.get_output_shape(x1)
        print ishape
        
        
        return x
        
    def decoder_function(self, S, map_sizes = (128,64,3), map_shape = [(14,14),(31,31),(64,64)],W_init = GlorotNormal()):
        print 'dec'
        h = S
        i=0
        ishape= lasagne.layers.get_output_shape(S)
        print ishape
        for n in map_sizes:
            x1 = Deconv2DLayer(h, num_filters = n, 
                        filter_size = (4,4), stride=(2, 2), 
                        crop='valid', W=W_init, b=None, nonlinearity=None,
                        output_size = map_shape[i])
            i+=1
            ishape= lasagne.layers.get_output_shape(x1)
            print ishape
            x2 = BatchNormLayer(x1, alpha=1.0, epsilon=1e-4, gamma=None)
            h = NonlinearityLayer(x2,nonlinearity=relu)
        
        
        x = NonlinearityLayer(x1,nonlinearity=tanh)
        
        return x
 
    def encoder_function(self, X, map_sizes = (64, 128, 256), W_init = GlorotNormal()):
        print 'enc'
        h = X
        ishape= lasagne.layers.get_output_shape(X)
        print ishape
        for n in map_sizes:
            x1 = Conv2DLayer(h, num_filters = n, filter_size = (4,4), stride=(2, 2), pad='valid', 
                             W=W_init, b=Constant(0.), nonlinearity=None)
            x2 = BatchNormLayer(x1, alpha=1.0, epsilon=1e-4, gamma=None)
            h = NonlinearityLayer(x2,nonlinearity=relu)
            ishape= lasagne.layers.get_output_shape(x1)
            print ishape
                             
        S = h
        
        return S
