from time import time
import theano
import theano.tensor as T

from lasagne.objectives import binary_crossentropy as bce
from gan import GAN_trainer
from src.theanofuns import clamp

class LSGAN_trainer(GAN_trainer):

    def __init__(self, model, alpha_schedule, update_type='adam', **kwargs):
        super(LSGAN_trainer, self).__init__(model, alpha_schedule, update_type, **kwargs)

    def define_loss(self, model):
        disX = model.disX
        disgenX = model.disgenX
        self.disX_loss = 0.5*(T.sqr(disX - 1.0)).mean()
        self.disgenX_loss = 0.5*(T.sqr(disgenX)).mean()
        self.genX_loss = 0.5*(T.sqr(disgenX-1.0)).mean()
        self.dis_loss = self.disX_loss + self.disgenX_loss
        self.gen_loss = self.genX_loss
