import theano.tensor as T
from lasagne.objectives import binary_crossentropy as bce


def compute_loss(disX, disgenX):
    
    disX_loss = bce(disX, T.ones(disX.shape)).mean()
    disgenX_loss = bce(disgenX, T.zeros(disgenX.shape)).mean()
    genX_loss = bce(disgenX, T.ones(disgenX.shape)).mean()

    return genX_loss, disX_loss, disgenX_loss