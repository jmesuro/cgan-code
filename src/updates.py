import numpy as np
import theano
import theano.tensor as T
from lasagne.utils import floatX
from lasagne.updates import get_or_compute_grads
from collections import OrderedDict

def compute_w_norm(array):

    ndim = array.ndim
    if ndim == 1:  # For Biases that are in 1d (e.g. b of DenseLayer)
        sum_over = ()
    elif ndim == 2:  # DenseLayer
        sum_over = (0,)
    elif ndim in [3, 4, 5]:  # Conv{1,2,3}DLayer
        sum_over = tuple(range(1, ndim))
    else:
        raise ValueError(
            "Unsupported tensor dimensionality {}. "
        )

    if len(sum_over) == 0:
        norm = 1.0 # if bias shape assume scale 1
    else:
        norm = T.sqrt(T.sum(array ** 2, axis=sum_over, keepdims=True))
    return norm


def adame(loss_or_grads, params, learning_rate=0.001, beta1=0.9,
         beta2=0.99, beta3=0.999, max_update=0.01, epsilon=1e-8):
    all_grads = get_or_compute_grads(loss_or_grads, params)
    t_prev = theano.shared(floatX(0.))
    updates = OrderedDict()

    # Using theano constant to prevent upcasting of float32
    one = T.constant(1)

    t = t_prev + 1
    a_t = learning_rate*T.sqrt(one-beta2**t)/(one-beta1**t)

    for param, g_t in zip(params, all_grads):
        value = param.get_value(borrow=True)
        m_prev = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                               broadcastable=param.broadcastable)
        v_prev = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                               broadcastable=param.broadcastable)

        m_t = beta1*m_prev + (one-beta1)*g_t
        v_t = beta2 * v_prev + (one - beta2) * g_t ** 2
        # s_t = beta3 * v_prev + (one - beta3) * g_t ** 2

        step = a_t*T.sgn(m_t)/(T.sqrt(v_t) + epsilon)


        norm_w = compute_w_norm(param)
        norm_step = compute_w_norm(step)

        aux = T.minimum(max_update*norm_w,norm_step)/norm_step
        step = step*aux

        updates[m_prev] = m_t
        updates[v_prev] = v_t
        updates[param] = param - step

    updates[t_prev] = t
    return updates
