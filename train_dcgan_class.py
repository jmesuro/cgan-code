#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lasagne.init import Normal

from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values


from src import gan
import datasets.datalist as datalist
from datasets.augmentation import Augmentation, zoom_and_crop
from networks.dcgan import build_discriminator_classifier, build_generator

from lasagne.objectives import categorical_crossentropy
from lasagne.regularization import regularize_layer_params, l2

last_model_dir = 'last_model'
best_model_dir = 'best_model'
samples_dir = 'samples'
if not os.path.exists('logs/'):
    os.makedirs('logs/')
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(best_model_dir):
    os.makedirs(best_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)


parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='stl10')
parser.add_argument("--datapath", type=str, default='/share/datasets')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=50000)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)


args = parser.parse_args()
if os.path.isfile("%s/args.p"%last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p"%last_model_dir, "rb"))
    args.n_iter = n_iter
else:
    resume = False

print args

np_rng = RandomState(args.seed)


# DATASET
data = datalist.get_dataset(args.dataname, args.datapath)
augm = Augmentation(rotation_range=15, # En grados
                    width_shift_range=0.15,
                    height_shift_range=0.15,
                    horizontal_flip=True,
                    shear_range=0.1745, # En radianes
                    zoom_range=(1.1,1.5), # >1 zoom out; <1 zoom in
                    channel_shift_range=20.0, # 0-255
                    fill_mode='nearest',
                    seed=args.seed)

nchannels = data._nchannels


# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()
y = T.matrix()

# MODELS
gen_network = build_generator(gen_dim=args.gen_dim,
                                       img_size=(args.img_size,args.img_size),
                                       nchannels=nchannels,
                                       ngf=args.model_width,
                                       W_init=Normal(std=args.init_scale))

dis_network, classifier = build_discriminator_classifier(nclasses=data.nclasses,
                                                         img_size=(args.img_size, args.img_size),
                                                         nchannels=nchannels,
                                                         ndf=args.model_width,
                                                         W_init=Normal(std=args.init_scale),
                                                         global_pool=True)

if resume:
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
    load_network_values(classifier,  os.path.join(last_model_dir, 'classifier.npz'))
    args_load = pickle.load(open("%s/args.p" %(last_model_dir), "rb"))

disX, clsX = lasagne.layers.get_output([dis_network,classifier], X)
genX = lasagne.layers.get_output(gen_network, Z)
disgenX = lasagne.layers.get_output(dis_network, genX, batch_norm_update_averages=False)

test_genX = lasagne.layers.get_output(gen_network, Z, deterministic=True)
test_clsX = lasagne.layers.get_output(classifier, X, deterministic=True)
test_clsX_out = T.argmax(test_clsX, axis=1)

# LOSS FUNCTIONS
genX_loss, disX_loss, disgenX_loss = gan.compute_loss(disX, disgenX)
gen_loss = genX_loss
dis_loss = disX_loss + disgenX_loss

weight_decay = regularize_layer_params(classifier, l2)
cls_loss = categorical_crossentropy(clsX,y).mean() + 0.01 * weight_decay

# PARAMS
dis_params = lasagne.layers.get_all_params(dis_network, trainable=True)
gen_params = lasagne.layers.get_all_params(gen_network, trainable=True)
cls_params = lasagne.layers.get_all_params(classifier, trainable=True)

# UPDATES
dis_updates = adam(dis_loss, dis_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
gen_updates = adam(gen_loss, gen_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
cls_updates = adam(cls_loss, cls_params, learning_rate=args.lr, beta1=0.9, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_gen = theano.function([Z], gen_loss, updates=gen_updates, on_unused_input='ignore')
train_dis = theano.function([X, Z], dis_loss, updates=dis_updates, on_unused_input='ignore')
train_cls = theano.function([X, y], cls_loss, updates=cls_updates)
print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, on_unused_input='ignore')
predict = theano.function(
        inputs=[X],
        outputs=test_clsX_out,
        updates=None
)
print '%.2f seconds to compile theano functions' % (time() - t)


X_sample = data.get_unlab_batch(0, args.grid_size**2)
X_crop = data.image_crop(X_sample, args.img_size)
color_grid_vis(X_crop.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir+'/real_%s_sample.png'%(args.dataname))

X_sample = augm.random_transform(X_sample)
X_crop = data.image_crop(X_sample, args.img_size)
color_grid_vis(X_crop.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size), samples_dir+'/augmented_%s_sample.png'%(args.dataname))

Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size**2, args.gen_dim)))


print "starting training"
if not resume:
    with open('errors.log', 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss')
        f.write('\n')
    with open('accuracies.log', 'w') as f:
        f.write('# iter data_seen epoch cls_loss train_acc valid_acc')
        f.write('\n')
    with open('best_acc.log', 'w') as f:
        f.write('# iter data_seen epoch valid_acc test_acc')
        f.write('\n')

n_epochs = args.n_iter*args.batch_size/data.unlab_size

last_it = 0
it_best = 0
best_acc = 0.0
t = time()
last_save = t - 3601.0 # Fuerza que en la primer iteracion guarde
for it in xrange(args.start_iter,args.n_iter):
    epoch = it*args.batch_size/data.unlab_size
    Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
    
    X_batch = data.get_unlab_batch(it, args.batch_size)
    X_batch = data.scale_data(data.image_crop(X_batch, args.img_size, random_state=np_rng))

    dis_loss_value = train_dis(X_batch, Z_batch)
    gen_loss_value = train_gen(Z_batch)

    X_batch,y_batch = data.get_train_batch(it,args.batch_size)
    X_batch = augm.random_transform(X_batch)
    X_batch = data.scale_data(data.image_crop(X_batch, args.img_size))
    cls_loss_value = train_cls(X_batch,y_batch)

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):

        valid_acc = 0.0
        for valit in range(data.valid_size/args.batch_size):
            X_valid, y_valid = data.get_valid_batch(valit,args.batch_size)
            X_valid = data.scale_data(data.image_crop(X_valid, args.img_size))
            y_pred = predict(X_valid)
            valid_acc += (y_pred == np.argmax(y_valid,axis=1)).mean()
        valid_acc /= data.valid_size/args.batch_size
        y_pred = predict(X_batch)
        train_acc = (y_pred == np.argmax(y_batch,axis=1)).mean()


        if best_acc<valid_acc:
            best_acc = valid_acc
            it_best = it
            args.start_iter = it + 1
            save_network_values(gen_network, os.path.join(best_model_dir, 'gen_network.npz'))
            save_network_values(dis_network, os.path.join(best_model_dir, 'dis_network.npz'))
            save_network_values(classifier, os.path.join(best_model_dir, 'classifier.npz'))
            pickle.dump(args,
                        open("%s/args.p" % best_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

            test_acc = 0.0
            for testit in range(data.test_size / args.batch_size):
                X_test, y_test = data.get_test_batch(testit, args.batch_size)
                X_test = data.scale_data(data.image_crop(X_test, args.img_size))
                y_pred = predict(X_test)
                test_acc += (y_pred == np.argmax(y_test, axis=1)).mean()
            test_acc /= data.test_size / args.batch_size

            with open('best_acc.log', 'a') as f:
                np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                                valid_acc, test_acc]], fmt='%1.3e')

        samples = np.asarray(test_gen(Z_sample))
        color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir+'/%d.png'%it)

        with open('errors.log', 'a') as f:
            np.savetxt(f, [[it+1, (it+1)*args.batch_size, epoch,
                            dis_loss_value, gen_loss_value]], fmt='%1.3e')

        with open('accuracies.log', 'a') as f:
            np.savetxt(f, [[it+1, (it+1)*args.batch_size, epoch,
                          cls_loss_value, train_acc, valid_acc]], fmt='%1.3e')

        t2 = time()-t
        t += t2
        horas = t2/(1+it-last_it)/3600.*10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones"%(it+1,args.n_iter,epoch,horas)
        last_it = it+1

    if time()-last_save>3600.: # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
        save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
        save_network_values(classifier,  os.path.join(last_model_dir, 'classifier.npz'))
        pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) )  #almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter # Corrida completada
save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
save_network_values(classifier,  os.path.join(last_model_dir, 'classifier.npz'))
pickle.dump( args, open( "%s/args.p"%last_model_dir, "wb" ) ) #Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan

print "Fin train gan\n"

