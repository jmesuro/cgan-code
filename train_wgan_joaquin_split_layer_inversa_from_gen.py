#!/usr/bin/env python2.7
import os
import argparse
import pickle
from time import time
from networks.share_w import share_layers_dis_to_gen, share_layers_gen_to_dis
import datetime
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values
import datasets.datalist as datalist
from networks.dcgan_joaquin_share_w_autoencoder import build_discriminator, build_generator
from lib.comunes import init_folders

parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='celeba64')
parser.add_argument("--datapath", type=str, default='/home/jmesuro/datasets')
parser.add_argument("--batch_size", type=int, default=64)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=400)
parser.add_argument("--gen_iter", type=int, default=1)
parser.add_argument("--dis_iter", type=int, default=1)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--grad_penalty", type=float, default=10.0)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument('--no-monitor', dest='monitor', action='store_false')
parser.add_argument("--genimages_size", type=int, default=5000)
parser.add_argument("--genimages_iter", type=int, default=0)
parser.add_argument("--shared_layers", type=int, default=4)
parser.add_argument("--exp_name", type=str, default='expr')
args = parser.parse_args()

LOAD_PATH = \
    '/home/jmesuro/cgan-workspace/cgan-code/out/' \
    'expr_bs64_gen_dim_n_iter1_share4_data_nameceleba64__date2017-12-04 14:36:14/last_model/'

# EXPERIMENT_PATH = '/home/jmesuro/cgan-code/out/expr_bs64_gen_dim_n_iter50_data_nameceleba64__share_w4_date2017-12-15 10:55:08'

last_model_dir, all_models_dir, samples_dir, logs, errors_log, resume = \
    init_folders(args, os, datetime, time, EXPERIMENT_PATH="")

if resume:
    # Retomar una corrida interrumpida
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter  # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p" % last_model_dir, "rb"))
    args.n_iter = n_iter

print args

np_rng = RandomState(args.seed)

# DATASET
data = datalist.get_dataset(args.dataname, args.datapath)
nchannels = data._nchannels

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()

# MODELS
gen_network, WsG = build_generator(gen_dim=args.gen_dim,
                                   img_size=(args.img_size, args.img_size),
                                   nchannels=nchannels,
                                   ngf=args.model_width)

_, inv_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                              nchannels=nchannels,
                                              ndf=args.model_width,
                                              wgan_output=True)

load_network_values(gen_network, os.path.join(LOAD_PATH, 'gen_network.npz'))

genX = lasagne.layers.get_output(gen_network, Z)
invX = lasagne.layers.get_output(inv_network, genX)

test_genX = lasagne.layers.get_output(gen_network, Z, deterministic=True)
test_invX = lasagne.layers.get_output(gen_network, invX, deterministic=True)

# LOSS FUNCTIONS
from lasagne.objectives import squared_error as se

inv_loss = se(invX, Z).mean()

# PARAMS
inv_params = [lasagne.layers.get_all_params(inv_network, trainable=True)[12]]  # solo actualizamos la capa densa

# UPDATES
inv_updates = adam(inv_loss, inv_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_inv = theano.function([Z], inv_loss, updates=inv_updates, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)
Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))

test_gen = theano.function([Z], test_genX, on_unused_input='ignore')
test_inv = theano.function([Z], test_invX, on_unused_input='ignore')

print "starting training"
if not resume:
    with open(errors_log, 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss')
        f.write('\n')

n_epochs = args.n_iter * args.batch_size / data.unlab_size

last_it = 0
t = time()
last_save = t - 3601.0  # Fuerza que en la primer iteracion guarde
for it in xrange(args.start_iter, args.n_iter):
    epoch = it * args.batch_size / data.unlab_size

    Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
    inv_loss = train_inv(Z_batch)

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):
        samples = np.asarray(test_gen(Z_sample))
        samples_inv = np.asarray(test_inv(Z_sample))
        color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir + '/%d.png' % it)
        color_grid_vis(data.inv_scale_data(samples_inv).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir + '/%d_inv.png' % it)

        with open(errors_log, 'a') as f:
            np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                            inv_loss]], fmt='%1.3e')

        t2 = time() - t
        t += t2
        horas = t2 / (1 + it - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones" % (it + 1, args.n_iter, epoch, horas)
        last_it = it + 1

    if time() - last_save > 3600.:  # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))
        pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter  # Corrida completada
save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))
pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))

print "Fin train gan\n"
