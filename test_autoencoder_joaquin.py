#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.init import Normal

from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values


import datasets.datalist as datalist
from networks.dcgan_joaquin_share_w_autoencoder import build_discriminator, build_generator




parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='celeba64')
parser.add_argument("--datapath", type=str, default='/home/jmesuro/datasets/')
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=20000)
parser.add_argument("--gen_iter", type=int, default=1)
parser.add_argument("--dis_iter", type=int, default=1)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--shared_layers", type=int, default=3)
parser.add_argument("--outputs_folder", type=str, default='outs_share_autoencoderAlso_w4_50000it')

args = parser.parse_args()

outputs_folder = args.outputs_folder

last_model_dir = outputs_folder + '/last_model'
samples_dir = outputs_folder + '/samples'

if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)

if os.path.isfile("%s/args.p"%last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p"%last_model_dir, "rb"))
    args.n_iter = n_iter
else:
    resume = False

print args

np_rng = RandomState(args.seed)


# DATASET
data = datalist.get_dataset(args.dataname, args.datapath, img_size=args.img_size)
nchannels = data._nchannels

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()

# MODELS
gen_network,WsG = build_generator(gen_dim=args.gen_dim,
                                       img_size=(args.img_size,args.img_size),
                                       nchannels=nchannels,
                                       ngf=args.model_width,
                                       W_init=Normal(std=args.init_scale))

dis_network, aut_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                           nchannels=nchannels,
                                           ndf=args.model_width,
                                           W_init=Normal(std=args.init_scale),
                                           global_pool=True, gen_dim=args.gen_dim)

if resume:
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
    load_network_values(aut_network, os.path.join(last_model_dir, 'aut_network.npz'))
    args_load = pickle.load(open("%s/args.p" %(last_model_dir), "rb"))

normal = Normal(std=args.init_scale)
wsigA = theano.shared(normal.sample((WsD[3].container.data.shape[0], 100)))
wa.container.data = wsigA.container.data

disX = lasagne.layers.get_output(dis_network, X,
                                 batch_norm_use_averages=True,
                                 batch_norm_update_averages=True)

genX = lasagne.layers.get_output(gen_network, Z,
                                 batch_norm_use_averages=True,
                                 batch_norm_update_averages=True)
autX = lasagne.layers.get_output(aut_network, X,
                                 batch_norm_use_averages=True,
                                 batch_norm_update_averages=True)

disgenX = lasagne.layers.get_output(dis_network, genX,
                                    batch_norm_use_averages=True,
                                    batch_norm_update_averages=False)



test_genX = lasagne.layers.get_output(gen_network, Z)
test_autX = lasagne.layers.get_output(aut_network, Z)
# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, on_unused_input='ignore')
test_aut = theano.function([X], test_autX, on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)

# Print real image
X_sample = data.get_unlab_batch(0,args.grid_size**2)
X_sample = data.image_crop(X_sample, args.img_size, random_state=np_rng)

color_grid_vis(X_sample.transpose(0, 2, 3, 1), (args.grid_size,args.grid_size),
               samples_dir+'/test_aut_real_sample.png')

# testing de autoencoder
Z_sample2 = np.asarray(test_aut(X_sample))
samples_from_aut = np.asarray(test_gen(Z_sample2)) # from image
color_grid_vis(data.inv_scale_data(samples_from_aut).transpose(0, 2, 3, 1),
               (args.grid_size,args.grid_size), samples_dir+'/test_aut_genaut_sample.png')

#testing gan
Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size**2, args.gen_dim)))
samples = np.asarray(test_gen(Z_sample)) # random
color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
               samples_dir+'/test_aut_gen_sample.png')

print "Fin test aut\n"

