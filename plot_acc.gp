# iter data_seen epoch cls_loss train_acc valid_acc
# iter data_seen epoch valid_acc test_acc
reset
set title "Classification Accuracies"
set xlabel "iter"
set ylabel "accuracy"
set style data lines
set yrange [0.2:1.0]
set key bottom
plot "accuracies.log" u 1:5 t "random train batch",\
        "" u 1:6 t "valid",\
     "best_acc.log" u 1:4 t "max valid" with points,\
     "" u 1:5 t "test" with points
