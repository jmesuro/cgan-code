#!/usr/bin/env python2.7

import os
import argparse
import pickle
from time import time
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lasagne.init import Normal

from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values

from src import gan_joaquin_autoencoder as gan
import datasets.datalist as datalist
from networks.dcgan_joaquin_share_w_autoencoder import build_discriminator, build_generator



parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='celeba')
parser.add_argument("--datapath", type=str, default='/home/jmesuro/datasets')
parser.add_argument("--batch_size", type=int, default=1)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=1)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument("--shared_layers", type=int, default=4)
parser.add_argument("--outputs_folder", type=str, default='outs_share_autoencoderAlso_w4_50000it')


args = parser.parse_args()

outputs_folder = args.outputs_folder
train_autoencoder = '/train_autoencoder'

last_model_dir = outputs_folder + '/last_model'
all_models_dir = outputs_folder + '/all_models'
samples_dir = outputs_folder + '/samples' + train_autoencoder
logs = outputs_folder + '/logs'
errors_log = outputs_folder + '/train_aut_errors.log'


if not os.path.exists(logs):
    os.makedirs(logs)
if not os.path.exists(last_model_dir):
    os.makedirs(last_model_dir)
if not os.path.exists(all_models_dir):
    os.makedirs(all_models_dir)
if not os.path.exists(samples_dir):
    os.makedirs(samples_dir)


if os.path.isfile("%s/args.p" % last_model_dir):
    # Retomar una corrida interrumpida
    resume = True
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
else:
    resume = False

print args

np_rng = RandomState(args.seed)

# DATASET
data = datalist.get_dataset(args.dataname, args.datapath, img_size=args.img_size)
nchannels = data._nchannels

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()

# MODELS
gen_network, WsG = build_generator(gen_dim=args.gen_dim,
                                   img_size=(args.img_size, args.img_size),
                                   nchannels=nchannels,
                                   ngf=args.model_width,
                                   W_init=Normal(std=args.init_scale))

dis_network, aut_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                                    nchannels=nchannels,
                                                    ndf=args.model_width,
                                                    W_init=Normal(std=args.init_scale),
                                                    global_pool=True, gen_dim=args.gen_dim)
if resume:
    load_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
    load_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
    load_network_values(aut_network, os.path.join(last_model_dir, 'aut_network.npz'))

# Reset dense layer in autoencoder
normal = Normal(std=args.init_scale)
wsigA = theano.shared(normal.sample((WsD[3].container.data.shape[0], 100)))
wa.container.data = wsigA.container.data


disX = lasagne.layers.get_output(dis_network, X)
genX = lasagne.layers.get_output(gen_network, Z)
disgenX = lasagne.layers.get_output(dis_network, genX, batch_norm_update_averages=False)
autX = lasagne.layers.get_output(aut_network, X)

test_genX = lasagne.layers.get_output(gen_network, Z)
test_autX = lasagne.layers.get_output(aut_network, X)

# LOSS FUNCTIONS
genX_loss, disX_loss, disgenX_loss, autX_loss = gan.compute_loss(disX, disgenX, autX, Z)
gen_loss = genX_loss
dis_loss = disX_loss + disgenX_loss
aut_loss = autX_loss


aut_params = aut_network.get_params()
#aut_params = lasagne.layers.get_all_params(aut_network, trainable=True)
aut_updates = adam(aut_loss, aut_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_aut = theano.function([X, Z], aut_loss, updates=aut_updates, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, on_unused_input='ignore')
test_aut = theano.function([X], test_autX, on_unused_input='ignore')
print '%.2f seconds to compile theano functions' % (time() - t)

X_sample = data.get_unlab_batch(0,args.grid_size**2)
X_sample = data.image_crop(X_sample, args.img_size, random_state=np_rng)

color_grid_vis(X_sample.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
               samples_dir+'/train_aut_real_sample.png')

print "starting training"
if not resume:
    with open(errors_log, 'w') as f:
        f.write('# iter data_seen epoch aut_loss')
        f.write('\n')

n_epochs = args.n_iter * args.batch_size / data.unlab_size
Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))
last_it = 0
t = time()
last_save = t - 3601.0  # Fuerza que en la primer iteracion guarde
for it in xrange(args.start_iter, args.n_iter):
    epoch = it * args.batch_size / data.unlab_size
    Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
    X_batch = data.get_unlab_batch(it, args.batch_size)
    X_batch = data.scale_data(data.image_crop(X_batch, args.img_size, random_state=np_rng))

    aut_loss = train_aut(X_batch, Z_batch)

    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):
        Z_sample = np.asarray(test_aut(X_sample))
        samples = np.asarray(test_gen(Z_sample))
        color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir + '/%d.png' % it)

        with open(errors_log, 'a') as f:
            np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                            aut_loss]], fmt='%1.3e')

        t2 = time() - t
        t += t2
        horas = t2 / (1 + it - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones" % (it + 1, args.n_iter, epoch, horas)
        last_it = it + 1

    if (it % 1000 == 0):
        save_network_values(aut_network, os.path.join(all_models_dir, 'aut_network' + '%d.npz' % it))

    if time() - last_save > 3600.:  # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(aut_network, os.path.join(last_model_dir, 'aut_network.npz'))
        pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter  # Corrida completada
save_network_values(aut_network, os.path.join(last_model_dir, 'aut_network.npz'))

pickle.dump(args, open("%s/args.p" % last_model_dir,
                       "wb"))  # Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan

print "Fin train aut\n"
