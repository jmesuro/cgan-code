reset
set term postscript enhanced color defaultplex "Times-Roman" 11 size 6cm,4cm eps

! python plot_monitors.py
cd 'monitor/ratio/'

set style data lines
set yrange [0:1.5]

set output 'ratio_l1.eps'
! zcat out_ratio_l1_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l1"

! convert -density 300 ratio_l1.eps ratio_l1.jpg

set output 'ratio_l2.eps'

! zcat out_ratio_l2_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l2"

! convert -density 300 ratio_l2.eps ratio_l2.jpg

set output 'ratio_l3.eps'

! zcat out_ratio_l3_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l3"

! convert -density 300 ratio_l3.eps ratio_l3.jpg

set output 'ratio_l4.eps'

! zcat out_ratio_l4_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l4"

! convert -density 300 ratio_l4.eps ratio_l4.jpg
! rm temp.txt

set yrange [-2:2]

set output 'ndiff_l1.eps'

! zcat out_ndiff_l1_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l1"

! convert -density 300 ndiff_l1.eps ndiff_l1.jpg
! rm temp.txt

set output 'ndiff_l2.eps'

! zcat out_ndiff_l2_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l2"

! convert -density 300 ndiff_l2.eps ndiff_l2.jpg
! rm temp.txt

set output 'ndiff_l3.eps'

! zcat out_ndiff_l3_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l3"

! convert -density 300 ndiff_l3.eps ndiff_l3.jpg
! rm temp.txt

set output 'ndiff_l4.eps'

! zcat out_ndiff_l4_Conv.txt.gz > temp.txt
plot 'temp.txt' using 1:($2-$3):($2+$3) with filledcurves lc rgb '#ffcccc' title 'StdDev',\
     '' u 1:2 t "l4"

! convert -density 300 ndiff_l4.eps ndiff_l4.jpg
! rm temp.txt