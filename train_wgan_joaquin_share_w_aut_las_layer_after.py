#!/usr/bin/env python2.7
import os
import argparse
import pickle
from time import time
from networks.share_w import share_layers_dis_to_gen, share_layers_gen_to_dis
import datetime
import numpy as np
from numpy.random import RandomState
import theano
import theano.tensor as T
import lasagne
from lasagne.updates import adam
from lib.vis import color_grid_vis
from lib.theano_utils import floatX
from lib.lasagne_utils import load_network_values, save_network_values
from src import wgan
import datasets.datalist as datalist
from networks.dcgan_joaquin_share_w_autoencoder import build_discriminator, build_generator
from lib.comunes import init_folders

parser = argparse.ArgumentParser()
parser.add_argument("--gen_dim", type=int, default=100)
parser.add_argument("--dataname", type=str, default='celeba64')
parser.add_argument("--datapath", type=str, default='/home/jmesuro/datasets')
parser.add_argument("--batch_size", type=int, default=64)
parser.add_argument("--start_iter", type=int, default=0)
parser.add_argument("--n_iter", type=int, default=1)
parser.add_argument("--gen_iter", type=int, default=1)
parser.add_argument("--dis_iter", type=int, default=1)
parser.add_argument("--grid_size", type=int, default=8)
parser.add_argument("--init_scale", type=float, default=0.02)
parser.add_argument("--grad_penalty", type=float, default=10.0)
parser.add_argument("--lr", type=float, default=0.0002)
parser.add_argument("--iter_save", type=int, default=100)
parser.add_argument("--img_size", type=int, default=64)
parser.add_argument("--model_width", type=int, default=128)
parser.add_argument("--seed", type=int, default=42)
parser.add_argument('--no-monitor', dest='monitor', action='store_false')
parser.add_argument("--genimages_size", type=int, default=5000)
parser.add_argument("--genimages_iter", type=int, default=0)
parser.add_argument("--shared_layers", type=int, default=4)
parser.add_argument("--exp_name", type=str, default='_inv')

args = parser.parse_args()

#EXPERIMENT_PATH = '/home/jmesuro/cgan-code/out/_inv_data_nameceleba64_bs128_gen_dim100_n_iter50000_share_w4_date2018-03-14 11:38:13'
EXPERIMENT_PATH =''
last_model_dir, all_models_dir, samples_dir, logs, errors_log, resume = \
    init_folders(args, os, datetime, time, EXPERIMENT_PATH=EXPERIMENT_PATH)

if resume:
    # Retomar una corrida interrumpida
    print '<<<RESUMING FROM PREVIOUS TRAINING>>>'
    n_iter = args.n_iter  # Se acepta una modificacion en la cantidad de iteraciones
    # Se ignora el resto de los argumentos de argparse y se levantan los de la corrida previa
    args = pickle.load(open("%s/args.p" % last_model_dir, "rb"))
    args.n_iter = n_iter

print args

np_rng = RandomState(args.seed)

# DATASET
data = datalist.get_dataset(args.dataname, args.datapath)
nchannels = data._nchannels

# SYMBOLIC INPUTS
X = T.tensor4()
Z = T.matrix()
rmix = T.vector()

# MODELS
gen_network, WsG = build_generator(gen_dim=args.gen_dim,
                                   img_size=(args.img_size, args.img_size),
                                   nchannels=nchannels,
                                   ngf=args.model_width)

dis_network, inv_network, WsD, wa = build_discriminator(img_size=(args.img_size, args.img_size),
                                                        nchannels=nchannels,
                                                        ndf=args.model_width,
                                                        wgan_output=True)

disX = lasagne.layers.get_output(dis_network, X)
genX = lasagne.layers.get_output(gen_network, Z)
disgenX = lasagne.layers.get_output(dis_network, genX)
invX = lasagne.layers.get_output(inv_network, genX)

test_invX = lasagne.layers.get_output(gen_network, invX, deterministic=True)
test_genX = lasagne.layers.get_output(gen_network, Z, deterministic=True)

# LOSS FUNCTIONS
disX_loss = wgan.compute_critic_loss(disX, label=True)
disgenX_loss = wgan.compute_critic_loss(disgenX, label=False)
genX_loss = wgan.compute_critic_loss(disgenX, label=True)
gen_loss = genX_loss
dis_loss = disX_loss + disgenX_loss

# GRAD PENALTY
rcol = rmix.dimshuffle(0, 'x', 'x', 'x')
mixX = (rcol * genX) + (1 - rcol) * X
dismixX = lasagne.layers.get_output(dis_network, mixX, deterministic=True)
dismixX_loss = wgan.compute_critic_loss(dismixX, label=True)
grad_penalty_loss = wgan.compute_grad_penalty(mixX, dismixX_loss)
dis_loss = dis_loss + args.grad_penalty * grad_penalty_loss

from lasagne.objectives import squared_error as se

inv_loss = se(invX, Z).mean()

# PARAMS
dis_params = lasagne.layers.get_all_params(dis_network, trainable=True)
gen_params = lasagne.layers.get_all_params(gen_network, trainable=True)
inv_params = lasagne.layers.get_all_params(inv_network, trainable=True)[-1]  # solo actualizamos la capa densa

# UPDATES
dis_updates = adam(dis_loss, dis_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
gen_updates = adam(gen_loss, gen_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)
inv_updates = adam(inv_loss, inv_params, learning_rate=args.lr, beta1=0.5, beta2=0.999)

# TRAINING FUNCTIONS
print 'COMPILING TRAINING FUNCTIONS'
t = time()
train_gen = theano.function([Z], gen_loss, updates=gen_updates, on_unused_input='ignore')
train_dis = theano.function([X, Z, rmix], dis_loss, updates=dis_updates, on_unused_input='ignore')
train_inv = theano.function([Z], inv_loss, updates=inv_updates, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)

# MONITOR
print 'COMPILING MONITOR FUNCTIONS'
t = time()
test_gen = theano.function([Z], test_genX, no_default_updates=True, on_unused_input='ignore')
test_inv = theano.function([Z], test_invX, on_unused_input='ignore')

print '%.2f seconds to compile theano functions' % (time() - t)

X_sample = data.get_unlab_batch(0, args.grid_size ** 2)
X_sample = data.image_crop(X_sample, args.img_size, random_state=np_rng)
color_grid_vis(X_sample.transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
               samples_dir + '/real_%s_sample.png' % (args.dataname))

Z_sample = floatX(np_rng.uniform(-1., 1., size=(args.grid_size ** 2, args.gen_dim)))

print "starting training"
if not resume:
    with open(errors_log, 'w') as f:
        f.write('# iter data_seen epoch dis_loss g_loss inv_loss')
        f.write('\n')
unlab_size = data.unlab_size

n_epochs = args.n_iter * args.batch_size / unlab_size

last_it = 0
t = time()
last_save = t - 3601.0  # Fuerza que en la primer iteracion guarde
for it in xrange(args.start_iter, args.n_iter):
    epoch = it * args.batch_size / unlab_size

    for k_it in range(args.dis_iter):
        Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
        r_batch = floatX(np_rng.uniform(0., 1., size=(args.batch_size,)))
        X_batch = data.get_unlab_batch(it * args.dis_iter + k_it, args.batch_size)
        X_batch = data.scale_data(data.image_crop(X_batch, args.img_size, random_state=np_rng))
        inv_loss_value = train_inv(Z_batch)
        dis_loss_value = train_dis(X_batch, Z_batch, r_batch)

    share_layers_dis_to_gen(args.shared_layers, WsG, WsD)
    for k_it in range(args.gen_iter):
        Z_batch = floatX(np_rng.uniform(-1., 1., size=(args.batch_size, args.gen_dim)))
        gen_loss_value = train_gen(Z_batch)

    share_layers_gen_to_dis(args.shared_layers, WsD, WsG)
    if (it % args.iter_save == 0) or (it % 10 == 0 and it < args.iter_save):
        samples = np.asarray(test_gen(Z_sample))
        samples_inv = np.asarray(test_inv(Z_sample))
        color_grid_vis(data.inv_scale_data(samples).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir + '/%d.png' % it)
        color_grid_vis(data.inv_scale_data(samples_inv).transpose(0, 2, 3, 1), (args.grid_size, args.grid_size),
                       samples_dir + '/%d_inv.png' % it)

        with open(errors_log, 'a') as f:
            np.savetxt(f, [[it + 1, (it + 1) * args.batch_size, epoch,
                            dis_loss_value, gen_loss_value, inv_loss_value]], fmt='%1.3e')

        t2 = time() - t
        t += t2
        horas = t2 / (1 + it - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d;    %4.2f horas. para 10000 iteraciones" % (it + 1, args.n_iter, epoch, horas)
        last_it = it + 1

    if time() - last_save > 3600.:  # Guardar modelos cada una hora para prevenir cortes inesperados
        last_save = time()
        args.start_iter = it + 1
        save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
        save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
        save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))
        pickle.dump(args, open("%s/args.p" % last_model_dir, "wb"))  # almacenar args aca tamb por si se corta la luz

args.start_iter = args.n_iter  # Corrida completada
save_network_values(gen_network, os.path.join(last_model_dir, 'gen_network.npz'))
save_network_values(dis_network, os.path.join(last_model_dir, 'dis_network.npz'))
save_network_values(inv_network, os.path.join(last_model_dir, 'inv_network.npz'))
pickle.dump(args, open("%s/args.p" % last_model_dir,
                       "wb"))  # Guardamos, en el mismo lugar que los parametros, los argumentos usados para luego poder cargarlos con load_gan

print "Fin train gan\n"
