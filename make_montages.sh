#!/bin/bash
montage -label '%f' ./monitor/disX/norm.dis*W*.jpg -tile 1x5 -geometry +2+2 ./monitor/disX/resume_norm_dis_W.jpg
montage -label '%f' ./monitor/genX/norm.gen*W*.jpg -tile 1x5 -geometry +2+2 ./monitor/genX/resume_norm_gen_W.jpg
for l in $(seq 1 4); do
        # echo   ./monitor/*/*dis*l${l}*Conv*.jpg
	montage -label '%f' ./monitor/*/*dis*l${l}*Conv*.jpg -tile 2x6 -geometry +2+2 ./monitor/genX/resume_dis_l${l}_W.jpg
	montage -label '%f' ./monitor/*/*gen*l${l}*Conv*.jpg -tile 2x6 -geometry +2+2 ./monitor/genX/resume_gen_l${l}_W.jpg
done
